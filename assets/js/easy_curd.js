//function init(id,target,action,form_id)
function Easy_curd(parm)
{

    this.refreshTable = function(status)
    {
        parm.data.status = status;
        datatableInit(parm,[],true,false);
        if(parm.refreshCallback)
        {
            parm.refreshCallback();
        }
    }

    this.refreshCounterTable = function(data,button)
    {
        parm.data.condition = data;
        if(button)
        {
            parm.removeButton = button.customButton;
        }
        else
        {
            parm.removeButton = "";
        }
        datatableInit(parm,[],true,false);
    }

    var extraSearch = false;
    this.refreshCounterTableSearch = function(data,button)
    {
        parm.data.condition = data;
        if(button)
        {
            parm.removeButton = button.customButton;
        }
        else
        {
            parm.removeButton = "";
        }
        extraSearch = true;
        datatableInit(parm,[],true,false);
    }



    function onRowClick(id)
    {
        $(id).on("click",'tbody tr td:not(:nth-child(2))', function () {
            var id = $(this).parent().find(".id").val();
            if(typeof(parm.onRowClickCallback) != 'undefined')
                parm.onRowClickCallback(id);
        });
    }

    function onRadioClick(id)
    {
        $(id).on("click",'tbody tr td .select', function () {
            var id = $(this).parent().find(".id").val();
            if(typeof(parm.customCallback) != 'undefined')
            {
                parm.customCallback(id,this);
            }
            else {
                if (typeof(parm.onRadioPreClickCallback) != 'undefined') {
                    parm.onRadioPreClickCallback(id);
                }
                $.post(parm.target, {'operation': 'get', id: id}).done(function (data) {
                    data = JSON.parse(data);
                    if (typeof(parm.onRadioPostClickCallback) != 'undefined') {
                        if (data.status == "ok") {
                            parm.onRadioPostClickCallback(data.status, data.data, data.message, data.title);
                        }
                        else {
                            parm.onRadioPostClickCallback(data.status);
                        }
                    }
                });
            }
        });

    }

    function form(id,target)
    {
        $.post(target,{'operation':'form'}).done(function (data) {
            data = JSON.parse(data);
            if(data.status == 'ok')
            {
                var form_group = $('<div></div>').addClass('row form-group');
                var col = $('<div></div>').addClass('col-lg-6');
                var label = $('<label></label>').text('Label');
                var select = $('<select></select>').attr('name','name').addClass('form-control');
                var input = $('<input/>').addClass('form-control');


                $.each(data.form, function (i,object) {
                    if(object.type == 'input')
                    {
                        input =  $('<input/>').addClass('form-control').attr('type','text').attr("name",object.name);
                        $(object.attributes).each(function (i,obj) {
                            $(input).attr(obj.key,obj.value);
                        });
                    }
                    else
                    {
                        input =  $('<select></select>').addClass('form-control').attr("name",object.name);
                        input.append($("<option></option>").attr('value','').text("Select"));
                        $.each(object.option, function (i, obj) {
                            input.append($("<option></option>").attr('value',obj.value).text(obj.label))
                        });
                        $(object.attributes).each(function (i,obj) {
                            $(input).attr(obj.key,obj.value);
                        });
                    }

                    label = $('<label></label>').text(object.label);
                    col = $('<div></div>').addClass('col-lg-6');
                    col.append(label).append(input);
                    if(i%2==0)
                    {
                        form_group = $('<div></div>').addClass('row form-group');
                    }
                    form_group.append(col);
                    $(id).append(form_group);
                });

                form_group = $('<div></div>').addClass('row form-group');
                var idInput = $('<input/>').attr('type','hidden').attr('name','id');
                var operation = $('<input/>').attr('type','hidden').addClass('operation').attr('name','operation');
                input = $('<button/>').addClass('btn btn-primary').attr('type','submit').text("Submit");
                col = $('<div></div>').addClass('col-lg-6');
                col.append(input);
                col.append(idInput);
                col.append(operation);
                form_group.append(col);
                $(id).append(form_group);

                submitForm(id,target)
            }
            else
            {
                toastr.warning(data.message,data.title);
            }

        });
    }

    function Add(form_id,add_btn)
    {
        $(add_btn).click(function () {
            $.each($(form_id).find('select,input'), function (i,obj) {
                $(obj).val("");
            })
            $(form_id).find('.operation').val("add");
            if(parm.add_form_callback)
            {
                if(parm.add_form_callback(form_id))
                {
                    if(parm.modal)
                    {
                        $(parm.modal).modal("show");
                    }
                    else
                    $("#myModal").modal("show");
                }
            }
            else
            {
                if(parm.modal)
                {
                    $(parm.modal).modal("show");
                }
                else
                $("#myModal").modal("show");
            }

        });
    }

    function Delete(id,target)
    {

        $(id).on('click','tr td .delete', function () {
            var id = $(this).parent().find('.id').val();
            if(parm.customDelete)
            {
                parm.customDelete(id);
                return false;
            }
            $.confirm({
                title: 'Delete!',
                confirmButtonClass: 'btn-info',
                cancelButtonClass: 'btn-danger',
                confirmButton: 'Yes',
                cancelButton: 'No',
                confirm: function(){

                    $.post(target,{id:id,operation:'delete'}).done(function(data,status){
                        data = JSON.parse(data);
                        if(data.status=="ok")
                        {
                            toastr.success(data.message,data.title);
                            datatableInit(parm,[],true,false);
                        }
                        else if(data.status=="fail")
                        {
                            toastr.error(data.message,data.title);
                        }
                    }).fail(function(){});
                }
            });
        });
    }

    function Update(id,target,form_id)
    {
        var operation = "none";
        $(id).on("click","tr td .edit",function(){
            var id = $(this).parent().find('.id').val();
            $("#edit_form").find('.id').val(id);
            if(typeof(parm.onClickEdit) != "undefined")
            {
                parm.onClickEdit(id);
                return true;
            }



            $.post(target,{id:id,'operation':'get'}).done(function (data) {
                data = JSON.parse(data);
                if(data.status == 'ok')
                {
                    if(typeof(parm.onClickEditPostCallback) != "undefined")
                    {
                        parm.onClickEditPostCallback(data.data);
                        return true;
                    }

                    if(typeof(parm.onClickEditFormCallback) != "undefined")
                    {
                        parm.onClickEditFormCallback(form_id);
                    }
                    $(form_id).find('.operation').val("update");
                    $.each(data.data, function (i,value) {
                        $(form_id).find('[name='+i+']').val(value);
                    });
                    if(parm.modal)
                    {
                        $(parm.modal).modal("show");
                    }
                    else
                    $("#myModal").modal("show");
                }
                else
                {
                    toastr.warning(data.message,data.title);
                }

            });
        });

    }

    function submitForm(id,target)
    {
        $(id).submit(function(){
            var postData = $(this).serializeArray();
            var action;
            $.post(target+'/action',postData)
                .done(function(data){
                    data = JSON.parse(data);
                    if(data.status == "ok")
                    {
                        toastr.success(data.message,data.title);
                        datatableInit(parm,[],true,false);
                        if(parm.modal)
                        {
                            $(parm.modal).modal("hide");
                        }
                        else
                        $("#myModal").modal("hide");
                    }
                    else
                    {
                        toastr.error(data.message,data.title);
                    }
                }).fail(function(){
                    toastr.error("Request Fail");
                    //alert("Request Fail");
                });
            return false;
        });
    }

    var data_table;
    var temp = "none";
    function datatableInit(parm,noSort,filter,init)
    {
        //parm.table_id,parm.data,[],true,parm.target
        parm.data.operation = "load";
        var noSearch = [];
        if(typeof(parm.noSearch) != "undefined")
        {
            noSearch = parm.noSearch;
        }

        if(typeof(parm.data) == "undefined")
        {
            parm.data = {action:
                                {
                                    "edit":false,
                                    "delete":false,
                                    'check':false,
                                    'radio':false
                                }
            };
        }
        else
        if(typeof(parm.action_column) != "undefined")
        {
            if(
                (typeof(parm.data.action.edit) != "undefined"&&parm.data.action.edit)
                ||
                (typeof(parm.data.action.delete) != "undefined"&&parm.data.action.delete)
                ||
                (typeof(parm.data.action.check) != "undefined"&&parm.data.action.check)
                ||
                (typeof(parm.data.action.radio) != "undefined"&&parm.data.action.radio)
                ||
                (typeof(parm.data.custom) != "undefined"&&parm.data.custom.length>0)
            ) {


                parm.data.action_column = parm.action_column;

                var actionCol = 0;
                var actionText = "Action";

                if (init)
                {
                    if (typeof(parm.data.action.check) != "undefined" && parm.data.action.check) {
                        actionText += " <input class='select_all' type='checkbox'/>";
                    }
                if (parm.data.action_column > 1) {
                    actionCol = parm.data.action_column - 1;

                    if ($(parm.table_id).find("thead tr th").length <= actionCol) {
                        noSort = [$(parm.table_id).find("thead tr th").length];
                        noSearch.push($(parm.table_id).find("thead tr th").length);
                        console.log(noSearch);
                        $(parm.table_id)
                            .find("thead tr")
                            .append($("<th></th>")
                                .addClass('all')
                                .html(actionText));
                    }
                    else {
                        $(parm.table_id)
                            .find("thead tr th")
                            .eq(actionCol)
                            .before($("<th></th>")
                                .addClass('all')
                                .html(actionText));
                    }


                } else {
                    $(parm.table_id)
                        .find("thead tr th")
                        .eq(actionCol)
                        .before($("<th></th>")
                            .addClass('all')
                            .text("Action"));

                }
                }

            }else {
                noSort = [];
            }

        }else {
            noSort = [];
        }

        var temp_check = true;

        $(parm.table_id).dataTable().fnDestroy();
        data_table = $(parm.table_id).DataTable({
            "ajax": {
                "url": parm.target,
                "type": "post",
                "data": parm.data
            },
            "processing": true,
            "serverSide": true,
            responsive: true,
            bsort: true,
            aoColumnDefs: [
                {
                    aTargets: noSort,
                    bSortable: false
                },
                {
                    "searchable": false, "targets": noSearch 
                }
            ],
            bFilter: filter,
            //"order": [[ 0, sort ]],
            "createdRow": function (row, data, index) {
                if(parm.removeButton&&parm.removeButton!="")
                {
                    $(row).find('td:nth-child('+parm.action_column+')').append(parm.removeButton);
                }
                //console.log($(row).find('td:nth-child(1)').html());
            },
            "autoWidth" : true,
            "preDrawCallback" : function() {},
            "fnInitComplete": function(oSettings, json) {

                if ($(parm.table_id).find("tbody tr:nth-child(1) td:nth-child(1)").find('.condition').length) {

                    temp = "temp";
                }
                else {
                    //alert($(parm.table_id).find("tbody tr:nth-child(1) td:nth-child(1)").html());
                    temp = "none";
                }

                if (extraSearch)
                {
                    var rows = $(parm.table_id).find("tbody tr").length;
                    var text = $(parm.table_id).find("tbody tr td:first-child").text();
                    if(text=="No matching records found")
                    {
                        $("[name=multiple_mfg_item_number]").focus();
                        scroll("[name=multiple_mfg_item_number]");
                    }else{
                        scroll("#count_counter_sheet");
                    }

                    extraSearch = false;
                }
                /*
                $("#inventory_data_table_length")
                    .append(
                    $('<label></label>')
                        .append('| Status ')
                        .append(
                        $("<select></select>")
                            .addClass("form-control")
                            .attr("id","inventory_status")
                            .append($("<option></option>").val("").text("All"))
                            .append($("<option></option>").val("Pending").text("Pending"))
                            .append($("<option></option>").val("Completed").text("Completed"))
                            .append($("<option></option>").val("In-Progress").text("In-Progress"))
                            .change(function () {
                                parm.data.status = this.value;
                                //console.log(parm.data);
                                //data_table.ajax.reload();
                                data_table
                                    .columns( 5 )
                                    .search( "Pending" )
                                    .draw();
                            })
                    )
                );
                */

            },
            "rowCallback" : function(nRow) {},
            "drawCallback" : function(oSettings) {
                $(parm.table_id).find('.edit,.delete,.custom_button').tooltip();
            }
        });
    }

    function scroll(target)
    {
        //var target = ".customer-data";
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    }

    this.removeRow = function(tag)
    {
        data_table
            .row( $(tag).parents('tr') )
            .remove()
            .draw();
    }

    this.temp = function(){
        return temp;
    }

    function onCustomClickCallback(callback,className)
    {
        $(parm.table_id).on("click","tr td ."+className, function () {
            var id = $(this).parent().find('.id').val();
            return callback(id,this);
        });
    }

    this.customClick = function(className,callback)
    {
        $(parm.table_id).on("click","tr td ."+className, function () {
            var id = $(this).parent().find('.id').val();
            return callback(id,this);
        });
    }

    if(typeof(parm.target) != "undefined"&&typeof(parm.table_id) != "undefined") {

        if(typeof(parm.InitDatatable) == "undefined")
        {
            var action_column = 1;
            if(typeof(parm.action_column) != "undefined")
            {
                action_column = parm.action_column;
            }
            datatableInit(parm, [action_column], true,true);
        }
        else
        {
            //parm.InitDatatable(parm.table_id,parm.target);
        }



        Update(parm.table_id, parm.target, parm.form_id);

        if( typeof(parm.form_id) != "undefined" )
        {
            if(parm.custom_form)
            {
                parm.custom_form(parm.form_id);
            }
            else{
                form(parm.form_id,parm.target);
            }


        }


        if( typeof(parm.form_id) != "undefined" && typeof(parm.add_btn) != "undefined" ) {
            Add(parm.form_id,parm.add_btn);
        }

        if( typeof(parm.data.action.delete) != "undefined")
        {

            Delete(parm.table_id,parm.target);

        }

        if( typeof(parm.data.action.radio) != "undefined")
        {
            onRadioClick(parm.table_id);
        }

        if( typeof(parm.onRowClickCallback) != "undefined") {
            onRowClick(parm.table_id);
        }

        if( typeof(parm.onCustomClickCallback) != "undefined") {
            if( typeof(parm.onCustomClickCallback.class) != "undefined"&&typeof(parm.onCustomClickCallback.method) != "undefined") {
                onCustomClickCallback(parm.onCustomClickCallback.method,parm.onCustomClickCallback.class);
            }
        }
    }




    //if(typeof(parm.onRowClickCallback) == "undefined" )
    //    alert("undefine Callback");


    if(typeof(parm.table_id) == "undefined")
    {
        console.error("Table ID is not define");
    }
    else if(typeof(parm.target) == "undefined")
    {
        console.error("Url for ajax call not define");
    }


}

