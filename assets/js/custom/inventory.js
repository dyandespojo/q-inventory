var base_url = $("base").attr('href');
var hospital_id = $(".hospital_id").val();
var hospital_number;
var inventory_serial_id;
var dept_number;
$("#add-inventory").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    //autoFocus: true
    onStepChanging: function (event, currentIndex, newIndex){
        var nextStep = '#wizard #wizard-p-' + newIndex;
        var heightNextStep = $(nextStep).css('minHeight');
        $(nextStep).parent().animate({height:heightNextStep},200);
        return true;
    }
});


$(document).ready(function () {


    if ($("#mapPrintView").length) {
        var mapPrintView = new loadMap({
            id: "",
            map_container: "#mapPrintView",
            node_class: "map-node",
            save_button: false,
            area: true,
            drag: false,
            location: false
        });
        var inventory_id = $("#inventory_id").val();
        var area_id = $("#area_id").val();
        mapPrintView.init(inventory_id, area_id);
    }


    var selected_inventory_id = 0;
    $('.pi_date').datepicker({format: "yyyy-mm-dd"});

    //Show Inventory according to status
    $("#inventory_status").change(function () {
        table.refreshTable(this.value);
    });


    //Initialize Inventory Table
    if ($("#inventory_data_table").length)
    {
        var table = new Easy_curd({
            "table_id": "#inventory_data_table", //Table ID
            "action_column": 2,
            "target": base_url + "Inventory/action", //Table URL for CURD
            "data": {
                hospital_id: hospital_id,
                action: {
                    "edit": true,
                    "delete": true,
                    'check': false,
                    'radio': false
                },
                custom: [
                    // '<button class="btn btn-xs btn-default view_item"><i class="fa fa-eye"></i></button>'
                ]
            }, //Action button such that edit and delete
            "add_btn": "#add",
            "form_id": "#edit_form", //Form ID
            //"InitDatatable": function (id,link) {},
            //"onRowClickCallback": function (id) {},
            "onCustomClickCallback": {
                "class": "view_item",
                "method": function (id, tag) {
                }
            },
            "customDelete": function (id) {

                $.confirm({
                    title: 'Delete!',
                    confirmButtonClass: 'btn-info',
                    cancelButtonClass: 'btn-danger',
                    confirmButton: 'Yes',
                    cancelButton: 'No',
                    confirm: function () {

                        $.post(base_url + "Inventory/deleteInventory", {id: id}).done(function (data, status) {
                            data = JSON.parse(data);
                            if (data.status == "ok") {
                                toastr.success(data.message, data.title);
                                table.refreshTable();
                            }
                            else if (data.status == "fail") {
                                toastr.error(data.message, data.title);
                            }
                        }).fail(function () {
                        });
                    }
                });
            },
            "onClickEdit": function (id) {
                $("#select_inventory_id").val(id);
                selected_inventory_id = id;
                $.post(base_url + "Inventory/getInventory", {id: id}).done(function (response) {
                    response = JSON.parse(response);
                    if (response.status == "ok") {
                        $("#setup_form").find('[name="inventory[pi_date]"]').val(response.data.pi_date);
                        $("#setup_form").find('[name="inventory[audit_percent]"]').val(response.data.audit_percent);
                        $(".section-2").fadeIn();
                        $(".section-1").slideUp();

                        $.post(base_url+"Inventory/getLocationSummaryPrePI",{inventory_id:selected_inventory_id,area_id:"none"})
                            .done(function (response) {
                                response = JSON.parse(response);
                                if(response.status=="ok")
                                {

                                    $("#pre_pi_location_name").empty();
                                    $("#pre_pi_location_name").append($("<option></option>").val("").text("Select"));
                                    /*$.each(response.data.location, function (i, obj) {
                                        $("#pre_pi_location_name")
                                            .append(
                                                $("<option></option>")
                                                    .val(obj.id)
                                                    .text(obj.name)
                                                    .attr('location_number',obj.location_number)
                                                    .attr('location_id',obj.id)
                                            );
                                    });*/


                                    $(".prepi_estimate_location").text(response.data.total);
                                    $(".prepi_completed_location").text(response.data.completed);
                                    $(".prepi_remaining_location").text(response.data.remain);
                                    $(".prepi_estimate_bins").text(response.data.total_bins);
                                    $(".prepi_completed_bins").text(response.data.complete_bins);
                                    $(".prepi_remaining_bins").text(response.data.remain_bins);
                                    $(".prepi_completed").text(response.data.completed_percent+"%");
                                    $(".prepi_bins_time").text(response.data.bins_time+" Minutes");

                                    //remove it then
                                     $("#add-inventory").steps("next");
                                      $("#add-inventory").steps("next");
                                       $("#add-inventory").steps("next");
                                        $("#add-inventory").steps("next");
                                         $("#add-inventory").steps("next");
                                    //$("#post_pl_count_time").val(parseFloat(response.data.bins_time)*60);
                                }
                            });

                    } else {
                        toastr.warning(response.message);
                    }
                });
            },
            "onClickEditPostCallback": function (data) {
            }
            //"onRadioPostClickCallback": function (status,data) {},
            //"onRadioPreClickCallback": function () {},
            //"customCallback": function (id) {}
        });
    }

    //Add button action under inventory table
    $("#start_visit").click(function () {
        $(".reset").val("");
        $("#select_inventory_id").val(0);
        selected_inventory_id = 0;
        $("#select_inventory_id").val(0);
        $(".section-2").fadeIn();
        $(".section-1").slideUp();
    });


    //Back button action on top of visit form
    $("#back_to_inventory").click(function () {
        table.refreshTable();
        $("#select_inventory_id").val(0);
        selected_inventory_id = 0;
        $("#add-inventory").steps("previous");
        $("#add-inventory").steps("previous");
        $("#add-inventory").steps("previous");
        $("#add-inventory").steps("previous");
        $("#add-inventory .steps ul").find('li:first-child').attr('class',"").addClass('first').addClass('current');
        $("#add-inventory .steps ul").find('li').not(':first-child').attr('class',"").addClass('disabled');
        $("#add-inventory .steps ul").find('li:last-child').addClass('last');

        $("#add-inventory .content").find('.current').removeClass('current');
        $("#add-inventory .content").attr('aria-hidden','true');

        $("#add-inventory .content").find('h3:first-child,section:first-child').addClass("current");


        $(".section-1").slideDown();
        $(".section-2").fadeOut();
    });

/*********************************Setup Form**********************************/

    //Setup form submission action
    $("#setup_form").submit(function(){
        var inventory_id = $("#select_inventory_id").val();
        var link = base_url+'Inventory/setup';
        var data = $(this).serializeArray();
        if(inventory_id > 0)
        {
            data[data.length] = {name:'inventory[id]',value:inventory_id};
        }

        $.post(link,data)
            .done(function (response) {
                response = JSON.parse(response);
                console.log(data);
                if(response.status == "ok")
                {
                    toastr.success(response.message,response.title);

                    //Set Inventory ID in Pre Physical Inventory Form
                    $("#pre_pl_inventory_id").val(response.id);
                    $("#inventory_file_form").find(".inventory_id").val(response.id);
                    $(".count_inventory_id").val(response.id);
                    $("#post_pi_inventory_id").val(response.id);
                    $("#select_inventory_id").val(response.id);
                    selected_inventory_id = response.id;
                    //Slide Visit to next(Import) Module
                    $("#add-inventory").steps("next");

                }
                else if(response.status == "warn")
                {
                    toastr.warning(response.message);
                }
                else
                {
                    toastr.error(response.message);
                }
        }).fail(function () {
                toastr.error("Request error");
            });
        return false;
    });

/******************************Import File***************************************/
// The event listener for the file upload
    document.getElementById('file').addEventListener('change', upload, false);

    // Method that checks that the browser supports the HTML5 File API
    function browserSupportFileUpload() {
        var isCompatible = false;
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            isCompatible = true;
        }
        return isCompatible;
    }

    // Method that reads and processes the selected file
    function upload(evt) {
        if (!browserSupportFileUpload()) {
            alert('The File APIs are not fully supported in this browser!');
        } else {
            var data = null;
            var file = evt.target.files[0];
            var reader = new FileReader();
            reader.readAsText(file);
            reader.onload = function(event) {
                var csvData = event.target.result;
                data = $.csv.toArrays(csvData);

                if (data && data.length > 0) {
                    var header = data[0];

                    var tableData = [];

                    $('#counter_sheet').find("thead tr").children().remove();
                    $(header).each(function (i,value) {
                        $('#counter_sheet').find("thead tr").append($('<th></th>').text(value));
                    });

                    $(data).each(function (i,obj) {
                        if(i!=0)
                        tableData.push(obj);
                    });

                    $('#counter_sheet').DataTable( {
                        data: tableData,
                        responsive: true
                    } );

                    $("#inventory_file_form button").prop("disabled",false);

                    toastr.success('Imported -' + data.length + '- rows successfully!');

                } else {
                    toastr.warning('No data to import!');
                }
            };
            reader.onerror = function() {
                alert('Unable to read ' + file.fileName);
            };
        }
    }

    $("#inventory_file_form").submit(function () {
        var form = this;
        var postData = new FormData(form);
        $("#file_inventory_id").val(selected_inventory_id);
        var parm  = $(this).serializeArray();
        $.confirm({
            title: 'Import Count Sheet',
            confirmButtonClass: 'btn-info',
            cancelButtonClass: 'btn-danger',
            confirmButton: 'Yes',
            cancelButton: 'No',
            confirm: function(){
                $.post(base_url+"Inventory/CheckItems",parm).done(function (data) {
                    var postData = new FormData(form);
                    var data = JSON.parse(data);
                    if(data.status=="warn")
                    {
                        $("#inventory_file_form").find('[name="remove"]').attr("checked",true);
                        postData = new FormData(form);
                        $("#inventory_file_form").find('[name="remove"]').attr("checked",false);

                        $.confirm({
                            title: 'Delete!',
                            content:data.message,
                            confirmButtonClass: 'btn-info',
                            cancelButtonClass: 'btn-danger',
                            confirmButton: 'Yes',
                            cancelButton: 'No',
                            confirm: function(){
                                $("#inventory_file_form button").prop("disabled",true);
                                $.ajax({
                                    url: base_url+"Inventory/uploadCSVfile", // Url to which the request is send
                                    type: "POST",             // Type of request to be send, called as method
                                    data: postData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                                    contentType: false,       // The content type used when sending data to the server.
                                    cache: false,             // To unable request pages to be cached
                                    processData:false,        // To send DOMDocument or non processed data file it is set to false
                                    success: function(data)   // A function to be called if request succeeds
                                    {
                                        data = JSON.parse(data);
                                        if(data.status == "ok")
                                        {
                                            $("#add-inventory").steps("next");
                                        }
                                        else
                                        {

                                        }
                                        $("#inventory_file_form button").prop("disabled",false);
                                    }
                                });
                            },
                            cancel: function(){
                                $("#add-inventory").steps("next");
                            }
                        });
                    }
                    else {
                        $("#inventory_file_form button").prop("disabled",true);
                        $.ajax({
                            url: base_url+"Inventory/uploadCSVfile", // Url to which the request is send
                            type: "POST",             // Type of request to be send, called as method
                            data: postData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData:false,        // To send DOMDocument or non processed data file it is set to false
                            success: function(data)   // A function to be called if request succeeds
                            {
                                data = JSON.parse(data);
                                if(data.status == "ok")
                                {
                                    $("#add-inventory").steps("next");
                                }
                                else
                                {

                                }
                                $("#inventory_file_form button").prop("disabled",false);
                            }
                        });
                    }
                    return false;

                });
            }
        });


        return false;
    });

    $("#post_pi").click(function () {
        $("#add-inventory").steps("next");
    });

    /*************************************Department*******************************************/
    $(".add_dept").on("click", function () {
    
        $("#deptModal").modal("show");
    
        $.post(base_url+"Inventory/getDepartments",{hospital_id:hospital_id}).done(function (data) {
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#dept_suggest").empty();
                $.each(data.data, function (i,obj) {
                    $("#dept_suggest").append($("<option>").val(obj.name));
                });
            }
        });
    
        $("#dept_form").find("input,textarea").not("[name = 'department[hospital_id]']").val("");
    });

    //Add Location
    $(".add_location").click(function () {
        var dept_id = $("#dept_id").val();
        var area_id = $("#area_id").val();
        if(dept_id=="")
        {
            toastr.warning("Please select Department");
            $("#dept_id").focus();
        }
        else if(area_id=="")
        {
            toastr.warning("Please select Area");
            $("#area_id").focus();
        }
        else
        {
            $.post(base_url + "Inventory/LocationNumber", {'department_id': dept_id}).done(function (data) {
                data = JSON.parse(data);
                if (data.status == "ok") {
                    $("#add_location_form").find('[name=location_number]').val(data.location_number);
                }

            });
            $("#addlocationModal").find("[name=name]").val("");
            $("#addlocationModal").modal("show");
        }

    });

    $("#add_location_form").submit(function () {

        var data = $(this).serializeArray();
        var area_id = $("#area_id").val();
        var dept_id = $("#dept_id").val();

        data.push({"name":"area_id","value":area_id});
        data.push({"name":"dept_id","value":dept_id});
        data.push({"name":"inventory_id","value":selected_inventory_id});

        $.post(base_url + "Inventory/addLocation", data).done(function (data) {
            data = JSON.parse(data);
            if (data.status == "ok")
            {
                $("#addlocationModal").modal("hide");

                inventory_map.addNode({
                    name:data.data.info.name,
                    location_number:data.data.info.location_number,
                    id:data.data.info.id
                });

                $("#pre_pi_location_name").empty();
                $("#pre_pi_location_name").append($("<option></option>").val("").text("Select"));
                $.each(data.data.location, function (i, obj) {
                    $("#pre_pi_location_name").append(
                        $("<option></option>")
                            .val(obj.id)
                            .text(obj.name)
                            .attr('location_number',obj.location_number)
                            .attr('location_id',obj.id)
                    );
                });
            }
        });
        return false;
    });


    //Load Department related General Areas
    $("#dept_id").change(function () {
        var dept_id = this.value;
        $("#pre_pi_location_name").empty();
        $("#pre_pi_location_name").append($("<option></option>").val("").text("Select"));
        if(dept_id!="") {
            inventory_map.clear();
            $.post(base_url + "Inventory/getAreas", {'department_id': dept_id}).done(function (data) {
                data = JSON.parse(data);
                if (data.status == "ok") {
                    pre_pi_dataview_table.refreshCounterTable({'inventory_id':inventory_id,'dept_id':dept_id});
                    $("#area_id").empty();
                    $("#area_id").append($("<option></option>").val("").text("Select"));
                    $.each(data.data, function (i, obj) {
                        $("#area_id").append($("<option></option>").val(obj.id).text(obj.name));
                    });
                    // $("#pre_pi_location_number").val(data.location_number);
                    //inventory_map.clear();
                }
            });
        }else {
            inventory_map.clear();
            $("#area_id").empty();
            $("#area_id").append($("<option></option>").val("").text("Select"));
        }

    });

    //General Area
    $("#area_id").change(function () {
        var department = $("#dept_id").find("option:selected").text();
        var area = $("#area_id").find("option:selected").text();
        inventory_map.clear();
        inventory_map.setInfo({area:area,department:department});

        $("#pre_pi_location_name").empty();
        $("#pre_pi_location_name").append($("<option></option>").val("").text("Select"));

        var area_id = this.value;
        var inventory_id = $("#select_inventory_id").val();
        if(this.value!="")
        $.post(base_url+"Inventory/getLocationSummaryPrePI",{inventory_id:inventory_id,area_id:area_id,module:"pre"})
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status=="ok")
                {
                    pre_pi_dataview_table.refreshCounterTable({'inventory_id':inventory_id,'area_id':area_id});

                    $("#pre_pi_location_name").empty();
                    $("#pre_pi_location_name").append($("<option></option>").val("").text("Select"));
                    $.each(response.data.location, function (i, obj) {
                        $("#pre_pi_location_name")
                            .append(
                                $("<option></option>")
                                    .val(obj.id)
                                    .text(obj.name)
                                    .attr('location_number',obj.location_number)
                                    .attr('location_id',obj.id)
                            );
                    });


                    $(".prepi_estimate_location").text(response.data.total);
                    $(".prepi_completed_location").text(response.data.completed);
                    $(".prepi_remaining_location").text(response.data.remain);
                    $(".prepi_estimate_bins").text(response.data.total_bins);
                    $(".prepi_completed_bins").text(response.data.complete_bins);
                    $(".prepi_remaining_bins").text(response.data.remain_bins);
                    $(".prepi_completed").text(response.data.completed_percent+"%");
                    $(".prepi_bins_time").text(response.data.bins_time+" Minutes");

                    //$("#post_pl_count_time").val(parseFloat(response.data.bins_time)*60);
                }
            });
    });
    
    $("#pre_pi_location_name").change(function () {
        var location_number = $(this).find("option:selected").attr('location_number');
        $("#pre_pi_location_number").val(location_number);
    });
    
    //Setup form submission action
    $("#dept_form").submit(function(){
        var data = $(this).serializeArray();
        $.post(base_url+'Inventory/addDepartments',data)
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status == "ok")
                {
                    toastr.success(response.message,response.title);

                    //Remove all option from Department
                    $("#dept_id").children().remove();

                    //Add first option with no value
                    $("#dept_id").append($("<option></option>").val("").text("Select"));

                    //Assign Department with new values
                    $(response.data.departments).each(function (i, obj) {
                        $("#dept_id").append($("<option></option>").val(obj.id).text(obj.name));
                    });

                    //Select newly added department
                    $("#dept_id").val(response.data.id);

                    $("#pre_pi_location_number").val(response.location_number);

                    //Hide Model
                    $("#deptModal").modal("hide");
                }
                else
                {
                    toastr.error("Some error occur");
                }
            }).fail(function () {
                toastr.error("Request error");
            });
        return false;
    });

    /*************************************Add Area************************************************/
    $(".add_area").on("click", function () {
        var dept_id = $("#dept_id").val();
        var hospital_id = $(".hospital_id").val();
        if(dept_id == "")
        {
            toastr.warning("Please select department");
            return false;
        }

        //Set Department ID in Area Form

        $("#area_form").find("input,textarea").val("");
        $("#area_form").find('[name="area[dept_id]"]').val(dept_id);

        $("#areaModal").modal("show");
        $.post(base_url+"Inventory/getAreas",{'department_id':dept_id}).done(function (data) {
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#area_suggest").empty();
                $.each(data.data, function (i,value) {
                    $("#area_suggest").append($("<option>").val(obj.name));
                });
            }
        });
    });

    //Setup form submission action
    $("#area_form").submit(function(){
        var data = $(this).serializeArray();
        $.post(base_url+'Inventory/addArea',data)
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status == "ok")
                {
                    toastr.success(response.message,response.title);

                    //Remove all option from Department
                    $("#area_id").children().remove();

                    //Add first option with no value
                    $("#area_id").append($("<option></option>").val("").text("Select"));

                    //Assign Department with new values
                    $(response.data.areas).each(function (i, obj) {
                        $("#area_id").append($("<option></option>").val(obj.id).text(obj.name));
                    });

                    //Select newly added department
                    $("#area_id").val(response.data.id);

                    //Hide Model
                    $("#areaModal").modal("hide");
                }
                else
                {
                    toastr.error("Some error occur");
                }
            }).fail(function () {
                toastr.error("Request error");
            });
        return false;
    });

    /********************************Pre Physical Inventory*****************************************/

    var inventory_map = new loadMap({
        id:"#area_id",
        dept_id:"#dept_id",
        map_container:"#map-container",
        node_class:"map-node",
        save_button:true,
        area:true,
        drag:true,
        location:false
    });


    /*
    var location_map = new loadMap({
        id:"#post_location_number",
        map_container:"#post-pi-map-container",
        node_class:"map-node",
        save_button:false,
        area:false,
        location:true
    });*/


    $("#pre_pl_form").submit(function () {
        var data = $(this).serializeArray();
        var inventory_id = $("#select_inventory_id").val();
        if(inventory_id == 0||inventory_id == "")
            inventory_id = selected_inventory_id;

        data.push({name:'location_inventory[inventory_id]',value:inventory_id});

        $.post(base_url+"Inventory/pre_pl",data)
            .done(function (data) {
            var response = JSON.parse(data);
            if(response.status == "ok")
            {
                toastr.success(response.message);

                $("#pre_pi_location_name").empty();
                $("#pre_pi_location_name").append($("<option></option>").val("").text("Select"));
                $.each(response.data.location, function (i, obj) {
                    $("#pre_pi_location_name")
                        .append(
                            $("<option></option>")
                                .val(obj.id)
                                .text(obj.name)
                                .attr('location_number',obj.location_number)
                                .attr('location_id',obj.id)
                        );
                });

                $("#pre_pl_form").find('[name^=location]')
                    .not('[name="location[area_id]"]')
                    .not('[name="location[inventory_id]"]')
                    .val("");

                $("#pre_pi_location_number").val(response.location_number);

                $.post(base_url+"Inventory/getLocationSummaryPrePI",{inventory_id:selected_inventory_id,area_id:"none"})
                    .done(function (response) {
                        response = JSON.parse(response);
                        if(response.status=="ok")
                        {

                            $("#pre_pi_location_name").empty();
                            $("#pre_pi_location_name").append($("<option></option>").val("").text("Select"));
                            $.each(response.data.location, function (i, obj) {
                                $("#pre_pi_location_name")
                                    .append(
                                        $("<option></option>")
                                            .val(obj.id)
                                            .text(obj.name)
                                            .attr('location_number',obj.location_number)
                                            .attr('location_id',obj.id)
                                    );
                            });


                            $(".prepi_estimate_location").text(response.data.total);
                            $(".prepi_completed_location").text(response.data.completed);
                            $(".prepi_remaining_location").text(response.data.remain);
                            $(".prepi_estimate_bins").text(response.data.total_bins);
                            $(".prepi_completed_bins").text(response.data.complete_bins);
                            $(".prepi_remaining_bins").text(response.data.remain_bins);
                            $(".prepi_completed").text(response.data.completed_percent+"%");
                            $(".prepi_bins_time").text(response.data.bins_time+" Minutes");

                            //$("#post_pl_count_time").val(parseFloat(response.data.bins_time)*60);
                        }
                    });
            }
            else
            {
                toastr.error("Unknown error occur");
            }
        });
        return false;
    });


    //Calculate Estimated Count Time
    $("#estimated_bins").focusout(function(){
        var result = 0;
        var hours = $("#estimated_bins_per_hours").val();
        if(hours != 0)
            result = this.value/hours;
        $("#estimate_count_hours").val(result);
        $("#estimated_count_min").val(result*60);
    });

    //Calculate Estimated Count Time
    $("#estimated_bins_per_hours").focusout(function(){
        var result = 0;
        var hours = this.value;
        if(hours != 0)
            result = $("#estimated_bins").val()/hours;
        $("#estimate_count_hours").val(result);
        $("#estimated_count_min").val(result*60);
    });

    //Export Button click Action
    //$('#exportPrePIModal').modal('show');
    $("#export_pre_pi").click(function () {
        $.post(base_url+"Inventory/getDepartments",{hospital_id:hospital_id}).done(function (data) {
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#pre_pl_export_dept").children().remove();
                $("#pre_pl_export_dept").append($("<option></option>").val("all").text("All"));
                $.each(data.data, function (i,obj) {
                    $("#pre_pl_export_dept").append($("<option></option>").val(obj.id).text(obj.name));
                });
            }
            $("#export_link").attr("href",base_url+"Inventory/exportPre_pi?hospital_id="+hospital_id);
            $("#print_link").attr("href",base_url+"Inventory/printPre_pi?hospital_id="+hospital_id);
            $("#pre_pl_export_dept").val("all");
            $("#pre_pl_export_area").val("all");
            $("#pre_pl_export_area").prop("disabled",true);
            $("#print_link,#export_link").hide(function () {
                $("#export_link").show();
            });
            $('#exportPrePIModal').modal('show');
        });

    });

    $("#print_pre_pi").click(function () {
        $.post(base_url+"Inventory/getDepartments",{hospital_id:hospital_id}).done(function (data) {
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#pre_pl_export_dept").children().remove();
                $("#pre_pl_export_dept").append($("<option></option>").val("all").text("All"));
                $.each(data.data, function (i,obj) {
                    $("#pre_pl_export_dept").append($("<option></option>").val(obj.id).text(obj.name));
                });
            }
            $("#export_link").attr("href",base_url+"Inventory/exportPre_pi?hospital_id="+hospital_id);
            $("#print_link").attr("href",base_url+"Inventory/printPre_pi?hospital_id="+hospital_id);
            $("#pre_pl_export_dept").val("all");
            $("#pre_pl_export_area").val("all");
            $("#pre_pl_export_area").prop("disabled",true);
            $("#print_link,#export_link").hide(function () {
                $("#print_link").show();
            });
            $('#exportPrePIModal').modal('show');
        });

    });

    //////////////////////////////////////////////////////POST PI Export////////////////////////////////////
    $("#print_post_pi").click(function(){
        var link = base_url+"Inventory/printPost_pi?hospital_id="+hospital_id;
        $("#post_print_link").attr("href",link);
        $("#exportPrintPIModal").modal('show');
    });

    $("#post_print_status").change(function(){
        var status = this.value;
        var link = base_url+"Inventory/printPost_pi?hospital_id="+hospital_id;
        $("#post_print_link").attr("href",link+"&status="+status);
    });

    $("#export_post_pi").click(function () {
        $.post(base_url+"Inventory/getDepartments",{hospital_id:hospital_id}).done(function (data) {

            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#post_export_dept").children().remove();
                $("#post_export_dept").append($("<option></option>").val("all").text("All"));
                $.each(data.data, function (i,obj) {
                    $("#post_export_dept").append($("<option></option>").val(obj.id).text(obj.name));
                });
            }

            $("#post_export_link").attr("href",base_url+"Inventory/exportPost_pi?hospital_id="+hospital_id);
            $("#post_print_link").attr("href",base_url+"Inventory/printPost_pi?hospital_id="+hospital_id);
            $("#post_export_dept").val("all");
            $("#post_export_area").val("all");
            $("#post_export_area").prop("disabled",true);
            $("#post_export_location").val("all");
            $("#post_export_location").prop("disabled",true);
            $("#print_link,#export_link").hide(function () {
                $("#export_link").show();
            });
            $('#exportPostPIModal').modal('show');
        });

    });

    $("#post_export_dept").change(function () {
        var dept = this.value;
        var link = base_url+'Inventory/exportPost_pi';
        var printlink = base_url+'Inventory/printPost_pi';
        parameter = "?hospital="+hospital_id+"&dept_id="+dept;
        if(dept == "all")
        {
            $("#post_export_area").val("all");
            $("#post_export_area").prop("disabled",true);
            $("#post_export_location").val("all");
            $("#post_export_location").prop("disabled",true);
        }
        else
        {
            $("#post_export_area").prop("disabled",false);
            //Assign Location According to Department in Export Form
            $.post(base_url+"Inventory/getAreas",{'department_id':dept}).done(function (data) {
                data = JSON.parse(data);
                if(data.status == "ok")
                {
                    $("#post_export_area").empty();
                    $("#post_export_area").append($("<option></option>").val("all").text("All"));
                    $.each(data.data, function (i,obj) {
                        $("#post_export_area").append($("<option></option>").val(obj.id).text(obj.name));
                    });
                }
            });
        }
        $("#post_export_link").attr("href",link+parameter);
        $("#post_print_link").attr("href",printlink+parameter);
    });

    $("#post_export_area").change(function () {
        var area = this.value;
        var link = base_url+'Inventory/exportPre_pi';
        var printlink = base_url+'Inventory/printPre_pi';
        parameter += "&area_id="+area;
        if(area == "all")
        {
            $("#post_export_location").val("all");
            $("#post_export_location").prop("disabled",true);
        }
        else
        {
            $("#post_export_location").prop("disabled",false);
            //Assign Location According to Department in Export Form
            $.post(base_url+"Inventory/getLocationPostPI",{'area_id':area,'inventory_id':selected_inventory_id})
                .done(function (data) {
                data = JSON.parse(data);
                if(data.status == "ok")
                {
                    $("#post_export_location").empty();
                    $("#post_export_location").append($("<option></option>").val("all").text("All"));
                    $.each(data.data, function (i,obj) {
                        $("#post_export_location").append($("<option></option>").val(obj.id).text(obj.name));
                    });
                }
            });
        }
        $("#post_export_link").attr("href",link+parameter);
        $("#post_print_link").attr("href",printlink+parameter);
    });

    $("#post_export_location").change(function () {
        var location = this.value;
        var link = base_url+'Inventory/exportPost_pi';
        var printlink = base_url+'Inventory/printPost_pi';
        parameter += "&location_id="+location;
        $("#post_export_link").attr("href",link+parameter);
        $("#post_print_link").attr("href",printlink+parameter);
    });

    //////////////////////////////////////////////////////POST PI Export////////////////////////////////////

    //Parameter for setting value in url
    var parameter = "";
    //Export Department ID
    $("#pre_pl_export_dept").change(function () {
        var dept = this.value;
        var link = base_url+'Inventory/exportPre_pi';
        var printlink = base_url+'Inventory/printPre_pi';
        parameter = "?hospital="+hospital_id+"&dept_id="+dept;
        if(dept == "all")
        {
            $("#pre_pl_export_area").val("all");
            $("#pre_pl_export_area").prop("disabled",true);
        }
        else
        {
            $("#pre_pl_export_area").prop("disabled",false);
            //Assign Location According to Department in Export Form
            $.post(base_url+"Inventory/getAreas",{'department_id':dept}).done(function (data) {
                data = JSON.parse(data);
                if(data.status == "ok")
                {
                    $("#pre_pl_export_area").empty();
                    $("#pre_pl_export_area").append($("<option></option>").val("all").text("All"));
                    $.each(data.data, function (i,obj) {
                        $("#pre_pl_export_area").append($("<option></option>").val(obj.id).text(obj.name));
                    });
                }
            });

        }
        $("#export_link").attr("href",link+parameter);
        $("#print_link").attr("href",printlink+parameter);
    });

    //Export Area ID
    $("#pre_pl_export_area").change(function () {
        var area = this.value;
        var link = base_url+'Inventory/exportPre_pi';
        var printlink = base_url+'Inventory/printPre_pi';
        parameter += "&area_id="+area;
        $("#export_link").attr("href",link+parameter);
        $("#print_link").attr("href",printlink+parameter);
    });

    //Build Sheet (Move to Count Module)
    $(".build_sheet").click(function () {
        var inventory_id = $("#select_inventory_id").val();
        //inventory_id = 1;
        $.post(base_url+"Inventory/getLocation",{inventory_id:inventory_id,status:"status <> 'Completed'"})
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status=="ok")
                {
                    $("#count_location_number").empty();
                    $("#count_location_number").append($("<option></option>").val("").text("Select"));
                    $.each(response.data, function (i,obj) {
                        $("#count_location_number")
                            .append($("<option></option>")
                            .attr('location_name',obj.name)
                            .attr('team',obj.team)
                                .val(obj.location_number)
                                .text(obj.location_number));
                    });
                    $("#add-inventory").steps("next");
                }
            });
    });

    $("#complete_item").click(function () {
        var location_number = $("#count_location_number").val();
        if(location_number=="")
        {
            toastr.warning("Please select location");
            $("#count_location_number").focus();
        }
        else
        {
            $.post(base_url+"Inventory/updateLocationStatus",{status:'Completed',location_number:location_number})
                .done(function (response) {
                    response = JSON.parse(response);
                    if(response.status == "ok")
                    {
                        toastr.success(response.message,response.title);
                        $("#count_location_number").find("option:selected").remove();
                        $("#team").val("");
                        $("#count_location_name").val("");
                    }
                }).fail(function () {
                    toastr.error("Request error");
                });
        }
    });


    $("#sheet_template").click(function(){});

    $("#clear_sheet").click(function(){
			$("#file").val("");
			$("#counter_submit").attr("disabled",true);
			$("#counter_sheet").dataTable().fnDestroy();
			$("#counter_sheet").find("tbody").empty();
	});



    var counter_table = new Easy_curd({
            "table_id":"#count_counter_sheet", //Table ID
            "action_column":2,
            "target":"Inventory/countSheet", //Table URL for CURD
            "noSearch":[],
            "data":{
                action:
                {
                    "edit":true,
                    "delete":true,
                    'check':false,
                    'radio':false
                },
                custom:[
                    '<span class="btn btn-xs btn-default select_item"><i class="fa fa-plus-circle"></i></span>'
                ]
            }, //Action button such that edit and delete
            "add_btn":"#add_item",
            "form_id":"#item_form", //Form ID
            "add_form_callback": function (form) {
                var location_number = $("#count_location_number").val();
                if(location_number=="")
                {
                    toastr.warning("Please select then location");
                    $("#count_location_number").focus();
                    return false;
                }
                $(form).find("[name=location_number]").val(location_number);
                return true;
            },
            //"InitDatatable": function (id,link) {},
            "refreshCallback": function () {
            },
            "onRowClickCallback": function (id) {},
            "onCustomClickCallback":{
                "class":"select_item",
                "method": function (id,tag) {
                    var mgt_num = $(tag).parents("tr").find("td:first-child").text();
                    var location = $("#count_location_number").val();
                    var name = $("#count_location_name").val();
                    
                    $("#update_item").find("[name='item[id]']").val(id);
                    $("#update_item").find("[name='item[location_number]']").val(location);
                    //scroll("#unit_of_measurement");
                    $("#mft_num").val(mgt_num);
                    toastr.success('Please type the detail of item');
                    $("#unit_of_measurement").focus();
                    /*$("#update_item").find('[name^=item]').val("");*/
                }
            }
            //"onRadioPostClickCallback": function (status,data) {},
            //"onRadioPreClickCallback": function () {},
            //"customCallback": function (id) {}
        });

    $("#update_item").submit(function () {
        $("#update_item").find('[name="item[location_number]"]').val();
        
        var mgt_num = $("#mft_num").val();
        var name = $("#count_location_name").val();
        var location_number = $("#count_location_number").val();
        //console.log(data);
        var data = $(this).serializeArray();
        data.push({name:'condition',value:counter_table.temp});
        data.push({name:'item[inventory_id]',value:selected_inventory_id});
        if( mgt_num.length == 0 )
        {
            toastr.warning("Please select item from table");
            return false;
        }else if(location_number == "")
        {
            scroll("#count_location_number");
            toastr.warning("Please select location");
            return false;
        }
        $.post(base_url+"Inventory/updateItem",data)
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status == "ok")
                {
                    toastr.success("Item added to "+name);
                    $("#update_item").find('[name^=item]').val("");
                    //$(tag).parents('tr').hide();
                }
            });
        return false;
    });

    //Change location number
    $("#count_location_number").change(function () {
        counter_table.refreshCounterTable();
    });



    //Un-assign Item from Location
    counter_table.customClick("remove_item",function(id,tag){
        var location = $("#count_location_number").val();
        var name = $("#count_location_name").val();
        if(location == "")
        {
            $("#count_location_number").focus();
            toastr.warning("Please select location");
        }
        else
        {
            $.post(base_url+"Inventory/updateItem",{id:id,location_number:""})
                .done(function (response) {
                    response = JSON.parse(response);
                    if(response.status == "ok")
                    {
                        toastr.success("Item remove from "+name);
                        $(tag).parents('tr').hide();
                    }
                });
        }
    });


    $("#count_location_number").change(function () {
        var location_name = $(this).find("option:selected").attr("location_name");
        var team = $(this).find("option:selected").attr("team");
        $("#count_location_name").val(location_name);
        $("#team").val(team);
    });

    //Save Team
    $(".save_team").click(function () {
        var team = $("#team").val();
        var location_number = $("#count_location_number").find("option:selected").val();

        if(team == "")
        {
            toastr.warning("Please mention team");
            return false;
        }
        $.post(base_url+"Inventory/updateLocation",{team:team,location_number:location_number})
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status == "ok")
                {
                    toastr.success(response.message,response.title);
                    $("#count_location_number").find("option:selected").attr('team',team);
                }
            }).fail(function () {
                toastr.error("Request error");
            });
    });

    /***********************Exact Search*************************/


    $(".extra_search_btn").click(function () {
        var mfg_item_number = $("#mfg_item_number").val();
        if(mfg_item_number=="")
        {
              toastr.warning("Please mention item number");
            $("#mfg_item_number").focus();
            return false;
        }
        else
        {
            if(!$("#count_counter_sheet_container").is(":visible"))
            {   
                $("#view_location_item_table_container").hide(); 
                $("#count_counter_sheet_container").show();
            }
            //scroll("#count_counter_sheet");
            counter_table.refreshCounterTableSearch({target:"temp",inventory_id:selected_inventory_id,mfg_item_number:mfg_item_number});
        }
    });


    $(".advanced_search_btn").click(function () {


        var item = $("[name=multiple_mfg_item_number]").val();
        var description = $("[name=multiple_mfg_item_description]").val();
        var vendor = $("[name=multiple_vendor_item_number]").val();
        var dataLoad = {operation:"load"};
        if(item.length>0)
        {
            dataLoad.multi_item_num = item;
        }

        if(description.length>0)
        {
            dataLoad.description = description;
        }

        if(vendor.length>0)
        {
            dataLoad.vendor = vendor;
        }

        if(item.length>0||description.length>0||vendor.length>0)
        {
            if(!$("#count_counter_sheet_container").is(":visible"))
            {
                $("#view_location_item_table_container").hide();
                $("#count_counter_sheet_container").show();
            }
            $("#mfg_item_number").val("");
            scroll("#count_counter_sheet");
            counter_table.refreshCounterTable(dataLoad);
        }
        else
        {
            toastr.warning("Please mention at least one search parameter");
        }

    });

    var custom_button = true;
    //View All item of Selected Location
    $("#view_location_item").click(function () {
        var location_number = $("#count_location_number").val();
        if(location_number=="")
        {
            $("#count_location_number").focus();
            scroll("#count_location_number");
            toastr.warning("Please select the location number");
            return false;
        }
        else
        {
            /*if(custom_button)
            {*/
                if(!$("#view_location_item_table_container").is(":visible"))
                {
                    $("#count_counter_sheet_container").hide();
                    $("#view_location_item_table_container").show();
                }
                custom_button = false;
                
                location_items_table.refreshCounterTable(
                    {location_number:location_number,inventory_id:selected_inventory_id}//,
                    //{customButton:'<span class="btn btn-xs btn-default remove_item"><i class="fa fa-minus-circle"></i></span>'}
                    );
            /*}
            else
            {
                counter_table.refreshCounterTable(
                    {location_number:location_number});
            }*/
            scroll("#view_location_item_table");
        }
    });

 var location_items_table = new Easy_curd({
        "table_id":"#view_location_item_table", //Table ID
        "action_column":2,
        "target":base_url+"Inventory/locationItemsTable", //Table URL for CURD
        "data":{
            action:
            {
                "edit":true,
                "delete":true,
                'check':false,
                'radio':false
            },
            condition:{'inventory_id':selected_inventory_id}
        }, //Action button such that edit and delete
        "modal":"#locationItemModal",
        "form_id":"#location_item_edit_form", //Form ID
        //"InitDatatable": function (id,link) {},
        //"onRowClickCallback": function (id) {},
        "onCustomClickCallback":{
            "class":"view_item",
            "method": function (id,tag) {}
        },
        //"custom_form":function (id) {},
        //'custom_submit':function(){},
        //'onClickEdit':function(id){},
       // customDelete:function(id){}
    });

    var location_map = new loadMap({
        id:"#post_pi_area_id",
        dept_id:"#post_pi_dept_id",
        map_container:"#post-pi-map-container",
        node_class:"map-node",
        save_button:false,
        area:true,
        drag:false,
        location:false
    });

    /*
    var inventory_map = new loadMap({
        id:"#area_id",
        dept_id:"#dept_id",
        map_container:"#map-container",
        node_class:"map-node",
        save_button:true,
        area:true,
        drag:true,
        location:false
    });

    */
    //Post Physical Inventory
    $("#post_pi_dept_id").change(function () {
        var dept_id = this.value;
        $("#post_location_number").empty();
        $("#post_location_number").append($("<option></option>").val("").text("Select"));
        if(dept_id!="") {
            location_map.clear();
            $.post(base_url + "Inventory/getPostAreas", {'dept_id': dept_id,'hospital_id':hospital_id}).done(function (data) {
                data = JSON.parse(data);
                if (data.status == "ok") {

                    location_table.refreshCounterTable({'status':status,'inventory_id':inventory_id,'dept_id':dept_id});
                    $("#post_pi_area_id").empty();
                    $("#post_pi_area_id").append($("<option></option>").val("").text("Select"));
                    $("#post_pi_area_id").append($("<option></option>").val("all").text("All"));
                    $.each(data.data, function (i, obj) {
                        $("#post_pi_area_id").append($("<option></option>").val(obj.id).text(obj.name));
                    });
                    // $("#pre_pi_location_number").val(data.location_number);
                }
            });

            $.post(base_url + "Inventory/getPostLocationSummary", {'dept_id': dept_id,inventory_id:selected_inventory_id}).done(function (response) {
                response = JSON.parse(response);
                if (response.status == "ok") {

                    $(".estimate_location").text(response.data.total);
                    $(".completed_location").text(response.data.completed);
                    $(".remaining_location").text(response.data.remain);
                    $(".estimate_bins").text(response.data.total_bins);
                    $(".completed_bins").text(response.data.complete_bins);
                    $(".remaining_bins").text(response.data.remain_bins);
                    $(".completed").text(response.data.completed_percent+"%");
                    $(".bins_time").text(response.data.bins_time+" Minutes");
                }
            });
        }else {
            $("#post_pi_area_id").empty();
            $("#post_pi_area_id").append($("<option></option>").val("").text("Select"));
        }
    });           

    $("#post_status").change(function () {
        var status = this.value;
        var inventory_id = $("#select_inventory_id").val();
        if(status == "")
        {
            clearPostPI();
            $("#post_pi_dept_id").empty();
            $("#post_pi_dept_id").append($("<option></option>").val("").text("Select"));

            $("#post_pi_area_id").empty();
            $("#post_pi_area_id").append($("<option></option>").val("").text("Select"));

            $("#post_location_number").empty();
            $("#post_location_number").append($("<option></option>").val("").text("Select"));
            //location_table.refreshCounterTable({'status':'None','inventory_id':0});
            return false;
        }


        var status;
        $.post(base_url+"Inventory/getLocationSummary",{hospital_id:hospital_id,inventory_id:inventory_id,status:"status = '"+status+"'"})
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status=="ok")
                {
                    location_map.clear();
                    $("#post_pi_dept_id").empty();
                    $("#post_pi_dept_id").append($("<option></option>").val("").text("Select"));
                    $("#post_pi_dept_id").append($("<option></option>").val("all").text("All"));
                    $.each(response.data.dept, function (i,obj) {
                        $("#post_pi_dept_id")
                            .append($("<option></option>")
                                .val(obj.id)
                                .text(obj.name));
                    });

                    $("#post_pi_area_id").empty();
                    $("#post_pi_area_id").append($("<option></option>").val("").text("Select"));

                    $("#post_location_number").empty();
                    $("#post_location_number").append($("<option></option>").val("").text("Select"));


                    $(".estimate_location").text(response.data.total);
                    $(".completed_location").text(response.data.completed);
                    $(".remaining_location").text(response.data.remain);
                    $(".estimate_bins").text(response.data.total_bins);
                    $(".completed_bins").text(response.data.complete_bins);
                    $(".remaining_bins").text(response.data.remain_bins);
                    $(".completed").text(response.data.completed_percent+"%");
                    $(".bins_time").text(response.data.bins_time+" Minutes");

                    //$("#post_pl_count_time").val(parseFloat(response.data.bins_time)*60);
                }
            });
            clearPostPI();
            status = this.value;
        location_table.refreshCounterTable({'status':this.value,'inventory_id':inventory_id});
    });

    function clearPostPI()
    {
        $("#post_location_number").val("");
        $("#post_pi_dept").val("");
        $("#post_pi_area").val("");
        $("#post_pi_location").val("");
        $("#post_pi_consume").val("");
        $("#post_pi_consigned").val("");
        $("#post_pi_estimated_bins").val("");
        $("#estimate_bins_per_hour").val("");
        $("#post_pl_team").val("");
        $("#post_pl_count_time").val("");
        $("#total_time").val("");
    }



    //General Area
    $("#post_pi_area_id").change(function () {
        var department = $("#post_pi_dept_id").find("option:selected").text();
        var area = $("#post_pi_area_id").find("option:selected").text();
        location_map.clear();
        location_map.setInfo({area:area,department:department});

        $("#post_location_number").empty();
        $("#post_location_number").append($("<option></option>").val("").text("Select"));

        var area_id = this.value;
        var inventory_id = $("#select_inventory_id").val();
        var status = $('#post_status').val();
        if(area!="") {

            location_map.clear();
            $.post(base_url + "Inventory/getLocationSummaryPrePI", {
                    inventory_id: inventory_id,
                    area_id: area_id,
                    status: status,
                    hospital_id:hospital_id
                })
                .done(function (response) {
                    response = JSON.parse(response);
                    if (response.status == "ok") {
                        location_table.refreshCounterTable({'status':status,'inventory_id':inventory_id,'area_id':area_id});

                        $("#post_location_number").empty();
                        $("#post_location_number").append($("<option></option>").val("").text("Select"));
                        $.each(response.data.location, function (i, obj) {
                            $("#post_location_number")
                                .append(
                                    $("<option></option>")
                                        .val(obj.location_number)
                                        .text(obj.location_number)
                                        .attr('location_number', obj.location_number)
                                        .attr('location_id', obj.id)
                                );
                        });
                        clearPostPI();
/*
                        $(".prepi_estimate_location").text(response.data.total);
                        $(".prepi_completed_location").text(response.data.completed);
                        $(".prepi_remaining_location").text(response.data.remain);
                        $(".prepi_estimate_bins").text(response.data.total_bins);
                        $(".prepi_completed_bins").text(response.data.complete_bins);
                        $(".prepi_remaining_bins").text(response.data.remain_bins);
                        $(".prepi_completed").text(response.data.completed_percent + "%");
                        $(".prepi_bins_time").text(response.data.bins_time + " Minutes");*/

                        //$("#post_pl_count_time").val(parseFloat(response.data.bins_time)*60);
                    }else
                    {
                        clearPostPI();
                    }
                });

                $.post(base_url + "Inventory/getPostLocationSummary", {'area_id': area_id,inventory_id:selected_inventory_id})
                .done(function (response) {
                response = JSON.parse(response);
                if (response.status == "ok") {

                    $(".estimate_location").text(response.data.total);
                    $(".completed_location").text(response.data.completed);
                    $(".remaining_location").text(response.data.remain);
                    $(".estimate_bins").text(response.data.total_bins);
                    $(".completed_bins").text(response.data.complete_bins);
                    $(".remaining_bins").text(response.data.remain_bins);
                    $(".completed").text(response.data.completed_percent+"%");
                    $(".bins_time").text(response.data.bins_time+" Minutes");
                }
            });
        }else
        {
            clearPostPI();
        }
    });

    //Post PI Location Number
    $("#post_location_number").change(function () {
        var location_number = this.value;
        var location_id = $(this).find('option:Selected').attr('location_id');
        $.post(base_url+'Inventory/getLocationDetail',{location_number:location_number})
            .done(function (response) {
            response = JSON.parse(response);
                if(response.status == "ok")
                {
                    $(".info_box").find(".department_name").text(response.data.dept);
                    $(".info_box").find(".area_name").text(response.data.area);
                    $("#post_pi_dept").val(response.data.dept);
                    $("#post_pi_area").val(response.data.area);
                    $("#post_pi_location").val(response.data.location);
                    $("#post_pi_consume").val(response.data.consume);
                    $("#post_pi_consigned").val(response.data.consigned);
                    $("#post_pi_estimated_bins").val(response.data.estimated_bins);
                    $("#estimate_bins_per_hour").val(response.data.estimated_bins_per_hours);
                    $("#post_pl_team").val(response.data.team);
                    $("#post_pl_count_time").val(parseInt(response.data.estimated_bins_per_hours)*60);
                    $("#total_time").val(response.data.min);
                }else
                {
                    $("#post_pi_dept").val("");
                    $("#post_pi_area").val("");
                    $("#post_pi_location").val("");
                    $("#post_pi_consume").val("");
                    $("#post_pi_consigned").val("");
                    $("#post_pi_estimated_bins").val("");
                    $("#estimate_bins_per_hour").val("");
                    $("#post_pl_team").val("");
                    $("#post_pl_count_time").val("");
                    $("#total_time").val("");
                }
        });

            $.post(base_url + "Inventory/getPostLocationSummary", {'location_id': location_id,inventory_id:selected_inventory_id})
            .done(function (response) {
                response = JSON.parse(response);
                if (response.status == "ok") {
                    //location_map.clear();

                    $(".estimate_location").text(response.data.total);
                    $(".completed_location").text(response.data.completed);
                    $(".remaining_location").text(response.data.remain);
                    $(".estimate_bins").text(response.data.total_bins);
                    $(".completed_bins").text(response.data.complete_bins);
                    $(".remaining_bins").text(response.data.remain_bins);
                    $(".completed").text(response.data.completed_percent+"%");
                    $(".bins_time").text(response.data.bins_time+" Minutes");
                }
            });
    });


    //Initialize Location Table
    var location_table = new Easy_curd({
        "table_id":"#post_pi_location_table", //Table ID
        "action_column":2,
        "target":base_url+"Inventory/location", //Table URL for CURD
        "data":{
            action:
            {
                "edit":true,
                "delete":true,
                'check':false,
                'radio':false
            },
            condition:{'inventory_id':selected_inventory_id}
        }, //Action button such that edit and delete
        "modal":"#locationModel",
        "form_id":"#location_form", //Form ID
        //"InitDatatable": function (id,link) {},
        //"onRowClickCallback": function (id) {},
        "onCustomClickCallback":{
            "class":"view_item",
            "method": function (id,tag) {}
        },
        "custom_form":function (id) {
            $(id).submit(function () {
                var data = $(this).serializeArray();
                data.push({'name':'inventory[inventory_id]','value':selected_inventory_id});
                $.post(base_url+"Inventory/updatelocationEdit",data)
                    .done(function (data) {
                        data = JSON.parse(data);
                        if(data.status == "ok" )
                        {
                            location_table.refreshTable();
                            $("#locationModel").modal('hide');
                        }
                        else {
                            toastr.error(data.message);
                        }

                    });
                return false;
            });
        },
        'custom_submit':function(){
        },
        'onClickEdit':function(id){

            $.post(base_url+"Inventory/getLocationEdit",{'id':id,inventory_id:selected_inventory_id})
                .done(function(data){
                    data = JSON.parse(data);
                    if(data.status == "ok") {
                        var location = data.data[0];
                        $("#location_form").find(".id").val(id);
                        $("#location_form").find('[name="location[name]"]').val(location.name);
                        $("#location_form").find('[name="inventory[consigned]"]').val(location.consigned);
                        $("#location_form").find('[name="inventory[consume]"]').val(location.consume);
                        $("#location_form").find('[name="inventory[estimated_bins]"]').val(location.estimated_bins);
                        $("#location_form").find('[name="inventory[estimated_bins_per_hours]"]').val(location.estimated_bins_per_hours);
                        $("#locationModel").modal('show');
                    }
                    else{
                        toastr.error("Error")
                    }
                });
            // $("#location_form").find("id").val(id);
        },
        customDelete:function(id){
            $.post(base_url+"Inventory/deleteLocationEdit",{location_id:id,inventory_id:selected_inventory_id})
                .done(function(data){
                    data = JSON.parse(data);
                    if(data.status == "ok") {
                        location_table.refreshTable();
                        toastr.success(data.message);
                    }
                    else{
                        toastr.error(data.message);
                    }
                });
    }
    });

    var pre_pi_dataview_table = new Easy_curd({
        "table_id":"#pre_pi_dataview_table", //Table ID
        "action_column":2,
        "target":base_url+"Inventory/location", //Table URL for CURD
        "data":{
            action:
            {
                "edit":true,
                "delete":true,
                'check':false,
                'radio':false
            },
            condition:{'hospital_id':hospital_id}
        }, //Action button such that edit and delete
        "modal":"#locationModel",
        "form_id":"#location_form", //Form ID
        //"InitDatatable": function (id,link) {},
        //"onRowClickCallback": function (id) {},
        "onCustomClickCallback":{
            "class":"view_item",
            "method": function (id,tag) {}
        },
        "custom_form":function (id) {
            $(id).submit(function () {
                var data = $(this).serializeArray();
                data.push({'name':'inventory[inventory_id]','value':selected_inventory_id});
                $.post(base_url+"Inventory/updatelocationEdit",data)
                    .done(function (data) {
                        data = JSON.parse(data);
                        if(data.status == "ok" )
                        {
                            pre_pi_dataview_table.refreshTable();
                            $("#locationModel").modal('hide');
                        }
                        else {
                            toastr.error(data.message);
                        }

                    });
                return false;
            });
        },
        'custom_submit':function(){
        },
        'onClickEdit':function(id){

            $.post(base_url+"Inventory/getLocationEdit",{'id':id,inventory_id:selected_inventory_id})
                .done(function(data){
                    data = JSON.parse(data);
                    if(data.status == "ok") {
                        var location = data.data[0];
                        $("#location_form").find(".id").val(id);
                        $("#location_form").find('[name="location[name]"]').val(location.name);
                        $("#location_form").find('[name="inventory[consigned]"]').val(location.consigned);
                        $("#location_form").find('[name="inventory[consume]"]').val(location.consume);
                        $("#location_form").find('[name="inventory[estimated_bins]"]').val(location.estimated_bins);
                        $("#location_form").find('[name="inventory[estimated_bins_per_hours]"]').val(location.estimated_bins_per_hours);
                        $("#locationModel").modal('show');
                    }
                    else{
                        toastr.error("Error")
                    }
                });
            // $("#location_form").find("id").val(id);
        },
        customDelete:function(id){
            $.post(base_url+"Inventory/deleteLocationEdit",{location_id:id,inventory_id:selected_inventory_id})
                .done(function(data){
                    data = JSON.parse(data);
                    if(data.status == "ok") {
                        location_table.refreshTable();
                        toastr.success(data.message);
                    }
                    else{
                        toastr.error(data.message);
                    }
                });
        }
    });


    function scroll(target)
    {
        //var target = ".customer-data";
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    }


});



