var hospital_value="0";
var  department_value="0";
var  top_value="5";
$(document).ready(function(){
    hospital_value=$("#hospital_lists").val();
 $("#hospital_lists").change(function(){
    hospital_value=$("#hospital_lists").val();
 department_list();
 });
     $("#depatments").change(function(){
    department_value=$("#depatments").val();
 department_data();
 });
    department_list();
    
});
function department_list()
{
    //alert(hospital_value);
    $.post("deshboard_data",
    {
        item_id: hospital_value,
        data_req : "department"
    },
    function(data, status){
       $("#depatments").html(data);
     department_value=$("#depatments").val();
    department_data();
    });
}
function department_data()
{
$.ajax({
    type:'POST',
    url:"deshboard_data",
    data:{
        item_id: department_value,
        data_req : "department_data_full"
    },
    success:function(data){
         	

       
        data=JSON.parse(data);
        $(".total_inv_value").html(data['total_value']);
        $(".total_main").html(data['total_main']);
        $(".total_pou").html(data['total_pou']);
        $(".total_owned").html(data['total_owned']);
        $(".total_match").html(data['total_match']);
        $(".total_unmatch").html(data['total_unmatch']);
        $(".total_consigned").html(data['total_consigned']);
        $(".on_hand").html(data['on_hand']);
        $(".on_hand_main").html(data['on_hand_main']);
        $(".on_hand_pou").html(data['on_hand_pou']);
        $(".on_hand_owned").html(data['on_hand_owned']);
        $(".on_hand_consigned").html(data['on_hand_consigned']);
        $(".on_hand_match").html(data['on_hand_match']);
        $(".on_hand_unmatch").html(data['on_hand_unmatch']);
        
        $(".expired").html(data['expired']);
        $(".expired_main").html(data['expired_main']);
        $(".expired_pou").html(data['expired_pou']);
        $(".expired_owned").html(data['expired_owned']);
        $(".expired_consigned").html(data['expired_consigned']);
        $(".expired_match").html(data['total_match']);
        $(".expired_unmatch").html(data['total_unmatch']);
        
        
            //console.log(data.supplier_detail[0].manufacture_name);
    $(function () {
    // Create the chart
    $('#container-main').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}$<br/>'
        },

        series: [{
            name: 'Inventory',
            colorByPoint: true,
            data: [{
                name: 'Total Inventory Value',
                y: parseInt(data['total_value']),
            }, {
                name: 'On Hand Value',
                y: parseInt(data['on_hand']),
            }, {
                name: 'Expired Value',
                y: parseInt(data['expired']),
            }, {
                name: 'Main',
                y: parseInt(data['total_main']),
            }, {
                name: 'POU',
                y: parseInt(data['total_pou']),
            },  {
                name: 'Owned',
                y: parseInt(data['total_owned']),
            }, {
                name: 'Consigned',
                y: parseInt(data['total_consigned']),
            }
                  ]
        }],
    });
});
    $(function () {
    // Create the chart
    $('#container-on-hand').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}$</b> of total<br/>'
        },

        series: [{
            name: 'Inventory',
            colorByPoint: true,
            data: [{
                name: 'Total Inventory Value',
                y: parseInt(data['total_value']),
            }, {
                name: 'On Hand Value',
                y: parseInt(data['on_hand']),
            }, {
                name: 'Main',
                y: parseInt(data['on_hand_main']),
            }, {
                name: 'POU',
                y: parseInt(data['on_hand_pou']),
            }, {
                name: 'Owned',
                y: parseInt(data['on_hand_owned']),
            }, {
                name: 'Consigned',
                y: parseInt(data['on_hand_consigned']),
            }
                  ]
        }],
    });
});
    $(function () {
    // Create the chart
    $('#container-expired').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}$</b> of total<br/>'
        },

     series: [{
            name: 'Inventory',
            colorByPoint: true,
            data: [{
                name: 'Total Inventory Value',
                y: parseInt(data['total_value']),
            },  {
                name: 'Expired Value',
                y: parseInt(data['expired']),
            }, {
                name: 'Main',
                y: parseInt(data['expired_main']),
            }, {
                name: 'POU',
                y: parseInt(data['expired_pou']),
            }, {
                name: 'Owned',
                y: parseInt(data['expired_owned']),
            }, {
                name: 'Consigned',
                y: parseInt(data['expired_consigned']),
            }
                  ]
        }],
    });
});
  
     var oh_ex_total=parseFloat(data['on_hand'])+parseFloat(data['expired']);
     var oh_value=((parseFloat(data['on_hand'])*100)/oh_ex_total);
     var ex_value=((parseFloat(data['expired'])*100)/oh_ex_total);
    $(function () {

    $(document).ready(function () {

        // Build the chart
        $('#container-oh-ex').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Value',
                colorByPoint: true,
                data: [{
                    name: 'On Hand',
                    y: oh_value
                }, {
                    name: 'Expired',
                    y: ex_value,
                } ]
            }]
        });
    });
});    
    
   var pou_main_total=parseFloat(data['total_main'])+parseFloat(data['total_pou']);
     var main_value=((parseFloat(data['total_main'])*100)/oh_ex_total);
     var pou_value=((parseFloat(data['total_pou'])*100)/oh_ex_total);
    
$(function () {

    $(document).ready(function () {

        // Build the chart
        $('#container-ma-po').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Value',
                colorByPoint: true,
                data: [{
                    name: 'Main',
                    color:'#90ed7d',
                    y: main_value
                }, {
                    name: 'POU',
                    color: '#f0ad4e',
                    y: pou_value
                } ]
            }]
        });
    });
});    
         
        var owned_consigned_total=parseFloat(data['total_owned'])+parseFloat(data['total_consigned']);
     var owned_value=((parseFloat(data['total_owned'])*100)/oh_ex_total);
     var consigned_value=((parseFloat(data['total_consigned'])*100)/oh_ex_total);
    
        $(function () {

    $(document).ready(function () {

        // Build the chart
        $('#container-ow-co').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Value',
                colorByPoint: true,
                data: [{
                    name: 'Owned',
                    color:'#337ab7',
                    y: owned_value,
                }, {
                    name: 'Consigned',
                    color:'#90ed7d',
                    y: consigned_value,
                } ]
            }]
        });
    });
}); 
        
            var match_unmatch_total=parseFloat(data['total_owned'])+parseFloat(data['total_consigned']);
     var match_value=((parseFloat(data['total_match'])*100)/oh_ex_total);
     var unmatch_value=((parseFloat(data['total_unmatch'])*100)/oh_ex_total);
    
        $(function () {

$(document).ready(function () {

        // Build the chart
        $('#container-ma-un').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Value',
                colorByPoint: true,
                data: [{
                    name: 'Match',
                    color: '#f0ad4e',
                    y: match_value
                }, {
                    name: 'Un Match',
                    color:'#434348',
                    y: unmatch_value,
                } ]
            }]
        });
    });
            
        
$(function () {
    // Create the chart
    $('#main_vs_pou').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}$</b> of total<br/>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [ {
                name: 'Main',
                 color: '#f0ad4e',
                y: parseInt(data['on_hand_main']),
            }, {
                name: 'POU',
                y: parseInt(data['on_hand_pou']),
            }
                  ]
        }],
    });
});

    
$(function () {
    // Create the chart
    $('#match_vs_unmatch').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [ {
                name: 'Match',
                y: parseInt(data['on_hand_match']),
            }, {
                name: 'Unmatch',
                y: parseInt(data['on_hand_umatch']),
            }
                  ]
        }],
    });
});
    
$(function () {
    // Create the chart
    $('#owned_vs_consigned').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}$</b> of total<br/>'
        },

        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [ {
                name: 'Owned',
                y: parseInt(data['on_hand_owned']),
            }, {
                name: 'Consigned',
                 color: '#f0ad4e',
                y: parseInt(data['on_hand_consigned']),
            }
                  ]
        }],
    });
});
            
    
$(function () {
    // Create the chart
    $('#main_vs_pou').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Inventory',
            colorByPoint: true,
            data: [ {
                name: 'Main',
                 color: '#f0ad4e',
                y:parseInt(data['expired_main']),
            }, {
                name: 'POU',
                y: parseInt(data['expired_pou']),
            }
                  ]
        }],
    });
});

    
$(function () {
    // Create the chart
    $('#match_vs_unmatch').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Inventory',
            colorByPoint: true,
            data: [ {
                name: 'Match',
                y: parseInt(data['expired_match']) ,
            }, {
                name: 'Unmatch',
                y: parseInt(data['expired_unmatch']),
            }
                  ]
        }],
    });
});
    
$(function () {
    // Create the chart
    $('#owned_vs_consigned').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: ''
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        series: [{
            name: 'Inventory',
            colorByPoint: true,
            data: [ {
                name: 'Owned',
                y: parseInt(data['expired_owned']),
            }, {
                name: 'Consigned',
                 color: '#f0ad4e',
                y: parseInt(data['expired_consigned']),
            }
                  ]
        }],
    });
});



});    

  
/*   for( var key in data['supplier_detail'] ) {
        if( data['supplier_detail'].hasOwnProperty(key) ) {
            console.log( key);
        }
    }
*/
        
 var chart;
$(document).ready(function() {
    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container-supplier',
            type: 'column',
            events: {
                load: requestData
            }
        },
        title: {
            text: ''
        },
        xAxis: {
              type: 'category',
        },
        yAxis: {
           title: {
                text: ''
            }
        },
        series: [{
            name: 'suppliers',
            data: []
        }]
    });        
});
        
function requestData() {
//    data.supplier_detail[0].manufacture_name
    $.ajax({
        url: "deshboard_supplier_data",
        data:{
        item_id: department_value,
        data_req : "department_data_full"
        },
        type: "POST",
        success: function(data) {
            var categories = [];
            var seriesData = [];
             data=JSON.parse(data);
            $.each(data, function() {
                categories.push(this.name);
                seriesData.push(parseInt(this.y));
            });
            chart.xAxis[0].setCategories(categories);
            chart.series[0].setData(seriesData);
        }
    });
}

/*
$("#depatments").change(function(){
    department_value=$("#depatments").val();
 department_data();
 });
 */
        
        
 var charttop;
$(document).ready(function() {
    charttop = new Highcharts.Chart({
        chart: {
            renderTo: 'container-supplier-top',
            type: 'column',
            events: {
                load: requestDatatop
            }
        },
        title: {
            text: ''
        },
        xAxis: {
              type: 'category',
        },
        yAxis: {
           title: {
                text: ''
            }
        },
        series: [{
            name: 'suppliers',
            data: []
        }]
    });        
});
 //top-filter
    function requestDatatop() {
//    data.supplier_detail[0].manufacture_name
    $.ajax({
        url: "deshboard_top_supplier_data",
        data:{
        item_id: department_value,
        number : top_value
        },
        type: "POST",
        success: function(data) {
            console.log(data);
            var categories = [];
            var seriesData = [];
             data=JSON.parse(data);
            $.each(data, function() {
                categories.push(this.name);
                seriesData.push(parseInt(this.y));
            });
            charttop.xAxis[0].setCategories(categories);
            charttop.series[0].setData(seriesData);
        }
    });
}
           $("#top-filter").change(function(){
    top_value=$("#top-filter").val();
  requestDatatop();
 });
      
    },
    fail:function( jqXHR, textStatus, errorThrown ) {
        console.log( 'Could not get posts, server response: ' + textStatus + ': ' + errorThrown );
    }
});
}
