
$(document).ready(function () {

    Easy_curd({
        "table_id":"#hospital_data_table", //Table ID
        "action_column":2,
        "target":"counter_hospitals/action", //Table URL for CURD
        "noSearch":[4,5],
        "data":{
            action:
            {
                "edit":true,
                "delete":true,
                'check':false,
                'radio':false
            },
            custom:[
                '<a href="CounterInventory/inventoryList" class="btn btn-xs btn-default inventory"><i class="fa fa-clipboard"></i></a>'
            ]
        }, //Action button such that edit and delete
        "add_btn":"#add",
        "form_id":"#edit_form", //Form ID
        //"InitDatatable": function (id,link) {},
        "onRowClickCallback": function (id) {

        },
        "onCustomClickCallback":{
            "class":"inventory",
            "method": function (id,tag) {
                var href = $(tag).prop('href');
                $(tag).prop('href',href+"/"+id)
            }
        }
        //"onRadioPostClickCallback": function (status,data) {},
        //"onRadioPreClickCallback": function () {},
        //"customCallback": function (id) {}
    });

});



