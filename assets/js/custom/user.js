$(document).ready(function () {
    Easy_curd({
        "table_id":"#user_data_table", //Table ID
        "action_column":2,
        "target":"userController/action", //Table URL for CURD
        "data":{
            action:
            {"edit":true,"delete":true,'check':false,'radio':false},
            /*custom:[
                '<a href="hospitals" class="btn btn-xs btn-default"><i class="fa fa-clipboard"></i></a>'
            ]*/
        }, //Action button such that edit and delete
        onClickEditFormCallback:function(form){
            $(form).find('[name=id]').attr('name','users_id');
        },
        add_form_callback:function(form){
            $(form).find('[name=id]').attr('name','users_id');
            return true;
        },
        "add_btn":"#add",
        "form_id":"#edit_form", //Form ID
        //"InitDatatable": function (id,link) {},
        //"onRowClickCallback": function (id) {},
        //"onRadioPostClickCallback": function (status,data) {},
        //"onRadioPreClickCallback": function () {},
        //"customCallback": function (id) {}
    });
});