var base_url = $("base").attr('href')+"CounterInventory";
var hospital_id = $(".hospital_id").val();
var hospital_number;
var inventory_serial_id;
var dept_number;
var startTime;
var endTime;
$("#add-inventory").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    //autoFocus: true
    onStepChanging: function (event, currentIndex, newIndex){
        var nextStep = '#wizard #wizard-p-' + newIndex;
        var heightNextStep = $(nextStep).css('minHeight');
        $(nextStep).parent().animate({height:heightNextStep},200);
        return true;
    }
});

function startCountTime()
{
    startTime = new Date();
}

function getTime()
{
    endTime = new Date();
    var difference = endTime.getTime()-startTime.getTime();
    var minutesDifference = Math.floor(difference/1000/60);
    difference -= minutesDifference*1000*60
    var secondsDifference = Math.floor(difference/1000);
    return {"minutes":minutesDifference,"seconds":secondsDifference};
}



$(document).ready(function () {

    var selected_inventory_id = 0;
    $('.pi_date').datepicker({format: "yyyy-mm-dd"});

    //Show Inventory according to status
    $("#inventory_status").change(function () {
        table.refreshTable(this.value);
    });



    //Initialize Inventory Table
    var table = new Easy_curd({
        "table_id":"#inventory_data_table", //Table ID
        "action_column":2,
        "target":base_url+"/action", //Table URL for CURD
        "data":{
            hospital_id:hospital_id,
            action:
            {
                "edit":true,
                "delete":true,
                'check':false,
                'radio':false
            },
            custom:[
                '<button class="btn btn-xs btn-default count_item"><i class="fa fa-eye"></i></button>'
            ]
        }, //Action button such that edit and delete
        "add_btn":"#add",
        "form_id":"#edit_form", //Form ID
        //"InitDatatable": function (id,link) {},
        //"onRowClickCallback": function (id) {},
        "onCustomClickCallback":{
            "class":"count_item",
            "method": function (id,tag) {
                $("#select_inventory_id").val(id);
                selected_inventory_id = id;
                $.post(base_url+"/getLocation",{inventory_id:id,status:'status = "Open"'}).done(function (response) {
                    response = JSON.parse(response);
                    if(response.status == "ok")
                    {
                        $("#count_location_number").children().remove();
                        $("#count_location_number").append($("<option></option>").val("").text("Select"));
                        $(response.data).each(function (i,obj) {
                            $("#count_location_number")
                                .append(
                                $("<option></option>")
                                    .attr('location_name',obj.name)
                                    .attr('team',obj.team)
                                    .val(obj.location_number)
                                    .text(obj.location_number));
                        });


                        $(".section-2").fadeIn();
                        $(".section-1").slideUp();
                    }else
                    {
                        toastr.warning(response.message);
                    }
                });
            }
        }
    });



/*********************************Setup Form**********************************/

    //Setup form submission action
    $("#setup_form").submit(function(){
        var inventory_id = $("#select_inventory_id").val();
        var link = base_url+'/setup';
        var data = $(this).serializeArray();
        if(inventory_id > 0)
        {
            data[data.length] = {name:'inventory[id]',value:inventory_id};
        }

        $.post(link,data)
            .done(function (response) {
                response = JSON.parse(response);
                console.log(data);
                if(response.status == "ok")
                {
                    toastr.success(response.message,response.title);

                    //Set Inventory ID in Pre Physical Inventory Form
                    $("#pre_pl_inventory_id").val(response.id);
                    $("#inventory_file_form").find(".inventory_id").val(response.id);
                    $(".count_inventory_id").val(response.id);
                    $("#post_pi_inventory_id").val(response.id);
                    $("#select_inventory_id").val(response.id);
                    selected_inventory_id = response.id;
                    //Slide Visit to next(Import) Module
                    $("#add-inventory").steps("next");
                }
                else if(response.status == "warn")
                {
                    toastr.warning(response.message);
                }
                else
                {
                    toastr.error(response.message);
                }
        }).fail(function () {
                toastr.error("Request error");
            });
        return false;
    });


    /*************************************Add Area************************************************/


    /********************************Pre Physical Inventory*****************************************/

    $("#complete_item").click(function () {
        var location_number = $("#count_location_number").val();
        if(location_number=="")
        {
            toastr.warning("Please select location");
            $("#count_location_number").focus();
        }
        else
        {
            $.post(base_url+"/updateLocationStatus",{status:'Completed',location_number:location_number})
                .done(function (response) {
                    response = JSON.parse(response);
                    if(response.status == "ok")
                    {
                        toastr.success(response.message,response.title);
                        $("#count_location_number").find("option:selected").remove();
                        $("#team").val("");
                        $("#count_location_name").val("");
                        $("#count_location_number").attr("disabled",false);
                        status = true;
                    }
                }).fail(function () {
                    toastr.error("Request error");
                });
        }
    });

    var counter_table = new Easy_curd({
            "table_id":"#count_counter_sheet", //Table ID
            "action_column":2,
            "target":base_url+"/countSheet", //Table URL for CURD
            "noSearch":[],
            "data":{
                action:
                {
                    "edit":false,
                    "delete":false,
                    'check':false,
                    'radio':false
                },
                custom:[
                    '<span class="btn btn-xs btn-default select_item"><i class="fa fa-plus-circle"></i></span>'
                ]
            }, //Action button such that edit and delete
            "add_btn":"#add_item",
            "form_id":"#item_form", //Form ID

            "add_form_callback": function (form) {
                var location_number = $("#count_location_number").val();
                if(location_number=="")
                {
                    toastr.warning("Please select then location");
                    $("#count_location_number").focus();
                    return false;
                }
                $(form).find("[name=location_number]").val(location_number);
                return true;
            },
            //"InitDatatable": function (id,link) {},
            "refreshCallback": function () {
            },
            "onRowClickCallback": function (id) {},
            "onCustomClickCallback":{
                "class":"select_item",
                "method": function (id,tag) {
                    var mgt_num = $(tag).parents("tr").find("td:first-child").text();
                    var location = $("#count_location_number").val();
                    var name = $("#count_location_name").val();
                    $("#update_item").find("[name='item[id]']").val(id);
                    $("#update_item").find("[name='item[location_number]']").val(location);
                    //scroll("#unit_of_measurement");
                    $("#mft_num").val(mgt_num);
                    toastr.success('Please type the detail of item');
                    $("#unit_of_measurement").focus();
                    /*$("#update_item").find('[name^=item]').val("");*/

                    /*if(location == "")
                    {
                        $("#count_location_number").focus();
                        toastr.warning("Please select location");
                    }
                    else
                    {
                        var time = getTime();
                        $.post(base_url+"/updateItem",{id:id,location_number:location,status:'audit',auto_count_time:time})
                            .done(function (response) {
                               response = JSON.parse(response);
                                if(response.status == "ok")
                                {
                                    toastr.success("Item added to "+name);
                                    $(tag).parents('tr').hide();
                                    startCountTime();
                                }
                            });
                    }*/
                }
            }
            //"onRadioPostClickCallback": function (status,data) {},
            //"onRadioPreClickCallback": function () {},
            //"customCallback": function (id) {}
        });
    

    $("#update_item").submit(function () {

        var data = $(this).serializeArray();
        var mgt_num = $("#mft_num").val();
        var name = $("#count_location_name").val();
        var location_number = $("#count_location_number").val();
        data.push({name:"item[inventory_id]",value:selected_inventory_id});

        if( mgt_num.length == 0 )
        {
            toastr.warning("Please select item from table");
            return false;
        }else if(location_number == "")
        {
            scroll("#count_location_number");
            toastr.warning("Please select location");
            return false;
        }
        $.post(base_url+"/updateItem",data)
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status == "ok")
                {
                    toastr.success("Item added to "+name);
                    $("#update_item").find('[name^=item]').val("");
                    $(tag).parents('tr').hide();
                }
            });
        return false;
    });

    //Change location number
    $("#count_location_number").change(function () {
        /*startCountTime();*/
        var location_number = this.value;
        if(this.value!="")
        $.post(base_url+'/workStatus',
            {
                inventory_id:selected_inventory_id,
                location_number:location_number,
                status:"progress"
            }
        ).done(function (response) {});

        $("#update_item").find('[name^=item]').val("");
        counter_table.refreshCounterTable();
        $(this).attr("disabled",true);
    });

    var status = true;
    function checkStatus()
    {
        $.post(base_url+'/getStatus')
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status=="ok")
                {
                    if(response.countStatus == "progress")
                    {
                        status = false;
                        var id = response.data.inventory_id;
                        selected_inventory_id = id;
                        $.post(base_url+"/getLocation",{inventory_id:id,status:'status <> "Completed"'}).done(function (innerResponse) {
                            innerResponse = JSON.parse(innerResponse);
                            if(innerResponse.status == "ok")
                            {
                                $("#count_location_number").children().remove();
                                $("#count_location_number").append($("<option></option>").val("").text("Select"));
                                $(innerResponse.data).each(function (i,obj) {
                                    $("#count_location_number")
                                        .append(
                                        $("<option></option>")
                                            .attr('location_name',obj.name)
                                            .attr('team',obj.team)
                                            .val(obj.location_number)
                                            .text(obj.location_number));
                                });


                                $("#count_location_number").val(response.data.location_number);
                                $("#team").val($("#count_location_number").find("option:selected").attr('team'));
                                $("#count_location_name").val($("#count_location_number").find("option:selected").attr('location_name'));
                                $("#count_location_number").attr("disabled",true);
                                $(".section-2").fadeIn();
                                $(".section-1").slideUp();
                            }else
                            {
                                toastr.warning(innerResponse.message);
                            }
                        });
                    }
                }
            });
    }

    checkStatus();

    //Un-assign Item from Location
    counter_table.customClick("remove_item",function(id,tag){
        var location = $("#count_location_number").val();
        var name = $("#count_location_name").val();
        if(location == "")
        {
            $("#count_location_number").focus();
            toastr.warning("Please select location");
        }
        else
        {
            $.post(base_url+"/updateItem",{id:id,location_number:""})
                .done(function (response) {
                    response = JSON.parse(response);
                    if(response.status == "ok")
                    {
                        toastr.success("Item remove from "+name);
                        $(tag).parents('tr').hide();
                    }
                });
        }
    });


    $("#count_location_number").change(function () {
        var location_name = $(this).find("option:selected").attr("location_name");
        var team = $(this).find("option:selected").attr("team");
        $("#count_location_name").val(location_name);
        $("#team").val(team);
    });

    //Save Team
    $(".save_team").click(function () {
        var team = $("#team").val();
        var location_number = $("#count_location_number").find("option:selected").val();

        if(team == "")
        {
            toastr.warning("Please mention team");
            return false;
        }
        $.post(base_url+"/updateLocation",{team:team,location_number:location_number})
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status == "ok")
                {
                    toastr.success(response.message,response.title);
                    $("#count_location_number").find("option:selected").attr('team',team);
                }
            }).fail(function () {
                toastr.error("Request error");
            });
    });

    /***********************Exact Search*************************/


    $(".extra_search_btn").click(function () {
        var mfg_item_number = $("#mfg_item_number").val();
        if(mfg_item_number=="")
        {
            toastr.warning("Please mention item number");
            $("#mfg_item_number").focus();
            return false;
        }
        else
        {
            scroll("#count_counter_sheet");
            counter_table.refreshCounterTable({mfg_item_number:mfg_item_number});
        }
    });

    $(".advanced_search_btn").click(function () {


        var item = $("[name=multiple_mfg_item_number]").val();
        var description = $("[name=multiple_mfg_item_description]").val();
        var vendor = $("[name=multiple_vendor_item_number]").val();
        var dataLoad = {operation:"load"};
        if(item.length>0)
        {
            dataLoad.multi_item_num = item;
        }

        if(description.length>0)
        {
            dataLoad.description = description;
        }

        if(vendor.length>0)
        {
            dataLoad.vendor = vendor;
        }

        if(item.length>0||description.length>0||vendor.length>0)
        {
            scroll("#count_counter_sheet");
            counter_table.refreshCounterTable(dataLoad);
        }
        else
        {
            toastr.warning("Please mention at least one search parameter");
        }

    });

    var custom_button = true;
    //View All item of Selected Location
    $("#view_location_item").click(function () {
        var location_number = $("#count_location_number").val();
        if(location_number=="")
        {
            $("#count_location_number").focus();
            scroll("#count_location_number");
            toastr.warning("Please select the location number");
            return false;
        }
        else
        {
            //if(custom_button)
            //{
                custom_button = false;
                counter_table.refreshCounterTable(
                    {location_number:location_number},
                    {customButton:'<span class="btn btn-xs btn-default remove_item"><i class="fa fa-minus-circle"></i></span>'});
            //}
            /*else
            {
                counter_table.refreshCounterTable(
                    {location_number:location_number});
            }*/

            scroll("#count_counter_sheet");

        }
    });

    //Post Physical Inventory
    $("#post_status").change(function () {
        var status = this.value;
        var inventory_id = $("#post_pi_inventory_id").val();
        $.post(base_url+"/getLocationSummary",{inventory_id:inventory_id,status:"status = '"+status+"'"})
            .done(function (response) {
                response = JSON.parse(response);
                if(response.status=="ok")
                {
                    $("#post_location_number").empty();
                    $("#post_location_number").append($("<option></option>").val("").text("Select"));
                    $.each(response.data.location, function (i,obj) {
                        $("#post_location_number")
                            .append($("<option></option>")
                                .attr('location_name',obj.name)
                                .attr('team',obj.team)
                                .val(obj.location_number)
                                .text(obj.location_number));
                    });

                    $(".estimate_location").text(response.data.total);
                    $(".completed_location").text(response.data.completed);
                    $(".remaining_location").text(response.data.remain);
                    $(".estimate_bins").text(response.data.total_bins);
                    $(".completed_bins").text(response.data.complete_bins);
                    $(".remaining_bins").text(response.data.remain_bins);
                    $(".completed").text(response.data.completed_percent+"%");
                    $(".bins_time").text(response.data.bins_time+" Minutes");

                    //$("#post_pl_count_time").val(parseFloat(response.data.bins_time)*60);
                }
            });
        $("#post_pi_dept").val("");
        $("#post_pi_area").val("");
        $("#post_pi_location").val("");
        $("#post_pi_consume").val("");
        $("#post_pi_consigned").val("");
        $("#post_pi_estimated_bins").val("");
        $("#estimate_bins_per_hour").val("");
        $("#post_pl_team").val("");
        location_table.refreshCounterTable({'status':this.value,'inventory_id':inventory_id});
    });

    //Post PI Location Number
    $("#post_location_number").click(function () {
        var location_number = this.value;
        $.post(base_url+'/getLocationDetail',{location_number:location_number})
            .done(function (response) {
            response = JSON.parse(response);
                if(response.status == "ok")
                {
                    $("#post_pi_dept").val(response.data.dept);
                    $("#post_pi_area").val(response.data.area);
                    $("#post_pi_location").val(response.data.location);
                    $("#post_pi_consume").val(response.data.consume);
                    $("#post_pi_consigned").val(response.data.consigned);
                    $("#post_pi_estimated_bins").val(response.data.estimated_bins);
                    $("#estimate_bins_per_hour").val(response.data.estimated_bins_per_hours);
                    $("#post_pl_team").val(response.data.team);
                }
        });
    });

    function scroll(target)
    {
        //var target = ".customer-data";
        var $target = $(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 900, 'swing', function () {
            window.location.hash = target;
        });
    }

    //Back button action on top of visit form
    $("#back_to_inventory").click(function () {

        if(status)
        {
            table.refreshTable();
            $("#select_inventory_id").val(0);
            selected_inventory_id = 0;
            $("#add-inventory").steps("previous");
            $("#add-inventory").steps("previous");
            $("#add-inventory").steps("previous");
            $("#add-inventory").steps("previous");
            $("#add-inventory .steps ul").find('li:first-child').attr('class',"").addClass('first').addClass('current');
            $("#add-inventory .steps ul").find('li').not(':first-child').attr('class',"").addClass('disabled');
            $("#add-inventory .steps ul").find('li:last-child').addClass('last');

            $("#add-inventory .content").find('.current').removeClass('current');
            $("#add-inventory .content").attr('aria-hidden','true');

            $("#add-inventory .content").find('h3:first-child,section:first-child').addClass("current");

            $(".section-1").slideDown();
            $(".section-2").fadeOut();
        }else
        {
            toastr.warning("Please complete the location item counting");
        }

    });


});



