function loadMap(parm) {
    var base_url = $("base").attr('href');
    var id = "";
    var node_class = "";
    var dept_id = "";
    var map = "";
    var last_top = 5;
    var last_left = 5;
    var operation = "none";
    if(parm.id)
    {
        id = parm.id;
    }

    if(parm.map_container)
    {
        map = parm.map_container;
    }

    if(parm.node_class)
    {
        node_class = parm.node_class;
    }

    if(parm.dept_id)
    {
        dept_id = parm.dept_id;
    }

    // <button class="btn btn-primary save_postion">Save Position</button>
    if(parm.save_button)
	{    
	$(map).parent().append(
        $('<a></a>')
            .addClass('btn btn-primary')
            .text("Save Location Position")
            .click(function(){
                var data = [];
                $(map).find("."+node_class).each(function (i,obj) {
                    var location_id = $(obj).find('.id').text();
                    var top = $(obj).css('top');
                    var left = $(obj).css('left');
                    data.push({
                        "id":location_id,
                        position_top:top,
                        position_left:left
                    });
                });

                //console.log(data);
				var link = "";
				if(operation == "area")
				{
					link = base_url+'Inventory/updateLocationPosition';
				}
				else if(operation == "dept")
				{
					link = base_url+"Inventory/updateGAPosition";
				}else
				{
					link = "none";
				}
			
			if(link!="none")
                $.post(link,{data:data}).done(function (response) {
                    response = JSON.parse(response);
                    if(response.status == "ok")
                    {
                        toastr.success(response.message);
                    }else
                    {
                        toastr.warning(response.message);
                    }
                });
                return false;
            })
    )/*.append(
        $('<a></a>')
            .addClass('btn btn-primary')
            .text("Print Map")
            .click(function(){
                // var html = $(map).html();
                var inventory_id = $("#select_inventory_id").val();
                var area_id = $("#area_id").val();
                var printView = window.open(base_url+"Inventory/mapPrintView?inventory_id="+inventory_id+"&area_id="+area_id, "Print", "height=450,width=580");
                // printView.document.write(html);
                return false;
            })
    )*/;
	
	$(map).parent().append(
        $('<a></a>')
            .addClass('btn btn-primary')
            .text("Print Map")
			.attr("href",base_url+"inventory/mapPrintView")
			.attr("target","_blank")
            .click(function(){
  /*              Popup($(map).html());
				return false;
*/
				var selected_dept = $("#dept_id").val();
				var selected_area = $("#area_id").val();
				var link = base_url+"inventory/mapPrintView?";
				var temp = "";
				if(selected_dept!="")
				{
					temp = link+"dept="+selected_dept;
					$(this).attr("href",temp);
				}
				
				if(selected_area!="")
				{
					temp = temp+"&area="+selected_area;
					$(this).attr("href",temp);
				}
				link = $(this).attr("href");
				Popup(link);
				return false;
				
                if(operation=="none"||selected_dept=="")
				{
					toastr.warning("Please select department or gneral area.");
					return false;
				}
				})
    );
	}

	
	function Popup(data) 
    {
        var mywindow = window.open(data, 'Print Map', 'height=400,width=600');
        //mywindow.document.write('<html><head><title>my div</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
		//mywindow.document.write('<link rel="stylesheet" href="'+base_url+'assets/css/custom/print.css" type="text/css" />');
		//var css = '<style>body{background-color:#0000;}</style>';
		//mywindow.document.write(css);
		//mywindow.document.write('</head><body style="background-color: #00ffe4;">');
		//$(data).find("#map-container").css({"background-color":"black"});
        //mywindow.document.write(data);
		//mywindow.document.write('<script>alert(document.innerHTML);</script>');
        //mywindow.document.write('</body></html>');*/
        //mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        //mywindow.close();
		

        return true;
    }

    if(parm.area)
    $(id).change(function () {

        if(this.value=="")
        {
			operation = "none";
			return false;
		} 
        else if(this.value=="all")
        {
            
            $(".info_box").find(".department_name").text(name);
            var name = $(dept_id).find(":selected").text();
            var select_dept_id = $("#post_pi_dept_id").find("option:selected").val();
            var inventory_id = $("#select_inventory_id").val();
            var area_name = $(this).find('option:selected').text();
            var dept_name = $("#post_pi_dept_id").find('option:selected').text();
            $.post(base_url+"Inventory/getGAPostPI",{inventory_id:inventory_id,dept_id:select_dept_id})
                .done(function (response) {
                    $(".info_box").find(".department_name").text(dept_name);
                    $(".info_box").find(".area_name").text(area_name);

                    response = JSON.parse(response);
                    var left = 5;
                    var top = 5;
                    $.each(response.data,function(i,val){
                        var node = $('<table></table>').css({left:val.position_left+'px',top:val.position_top+'px',"min-width":100+"px"})
                            .addClass(node_class)
                            .append(
                            $('<tr></tr>').append(
                                $('<td></td>').text(val.name).append(
                                    $('<span class="id"></span>').text(val.id).hide()
                                )
                            )
                        ).draggable({
                                containment: "parent",
                                disabled: !parm.drag
                            });
                        $(map).append(node);
                    });
                });   
            return false;
        }
		else{
			operation = "area";
    		var area_id = this.value;
            var area_name = $(this).find('option:selected').text();
            var dept_name = $(dept_id).find('option:selected').text();
            var inventory_id = $("#select_inventory_id").val();
            $.post(base_url+"Inventory/getLocationPostPI",{inventory_id:inventory_id,area_id:area_id})
                .done(function (response) {
                    $(".info_box").find(".department_name").text(dept_name);
                    $(".info_box").find(".area_name").text(area_name);
                    response = JSON.parse(response);
                    var left = 5;
                    var top = 5;
                    $.each(response.data,function(i,val){
                        var node = $('<table></table>').css({left:val.position_left+'px',top:val.position_top+'px'})
                            .addClass(node_class)
                            .append(
                                $('<tr></tr>').append(
                                    $('<td></td>').text(val.name).append(
                                        $('<span class="id"></span>').text(val.id).hide()
                                    )
                                )
                            ).append(
                                $('<tr></tr>').append(
                                    $('<td></td>').text(val.location_number)
                                )
                            ).draggable({
                                containment: "parent",
                                disabled: !parm.drag
                            });
                        $(map).append(node);
                    });
                });
        }
        
    });


    if(parm.dept_id)
        $(dept_id).change(function () {
            if(this.value==""||this.value=="all")
			{
				operation = "none";
				return false;
			}            
            else
                operation = "dept";
            var name = $(this).find(":selected").text();
            var select_dept_id = this.value;
            var inventory_id = $("#select_inventory_id").val();
            $.post(base_url+"Inventory/getGAPostPI",{inventory_id:inventory_id,dept_id:select_dept_id})
                .done(function (response) {
                    $(".info_box").find(".department_name").text(name);
                    response = JSON.parse(response);
                    var left = 5;
                    var top = 5;
                    $.each(response.data,function(i,val){
                        var node = $('<table></table>').css({left:val.position_left+'px',top:val.position_top+'px',"min-width":100+"px"})
                            .addClass(node_class)
                            .append(
                            $('<tr></tr>').append(
                                $('<td></td>').text(val.name).append(
                                    $('<span class="id"></span>').text(val.id).hide()
                                )
                            )
                        ).draggable({
                                containment: "parent",
                                disabled: !parm.drag
                            });
                        $(map).append(node);
                    });
                });
        });



    if(parm.location)
        $(id).change(function () {
            if(this.value=="")
            {
                $(map).find('.'+node_class).remove();
                $(".info_box").find(".department_name").text("None");
                $(".info_box").find(".area_name").text("None");
                return false;
            }

            var location_number = this.value;
            var inventory_id = $("#select_inventory_id").val();
            $.post(base_url+"Inventory/getMapBylocation",{inventory_id:inventory_id,location_number:location_number})
                .done(function (response) {
                    $(map).find('.'+node_class).remove();
                    // $(".info_box").find(".department_name").text($("#post_pi_dept").val());
                    // $(".info_box").find(".area_name").text($("#post_pi_area").val());
                    response = JSON.parse(response);
                    var left = 5;
                    var top = 5;
                    $.each(response.data,function(i,val){


                        if(location_number==val.location_number)
                        {
                            var node = $('<table></table>').css({left:val.position_left+'px',top:val.position_top+'px'})
                                .addClass(node_class)
                                .addClass("node-active")
                                .append(
                                    $('<tr></tr>').append(
                                        $('<td></td>').text(val.name).append(
                                            $('<span class="id"></span>').text(val.id).hide()
                                        )
                                    )
                                ).append(
                                    $('<tr></tr>').append(
                                        $('<td></td>').text(val.location_number)
                                    )
                                ).draggable({
                                    containment: "parent",
                                    disabled: true
                                });
                        }
                        else
                        {
                            var node = $('<table></table>').css({left:val.position_left+'px',top:val.position_top+'px'})
                                .addClass(node_class)
                                .append(
                                    $('<tr></tr>').append(
                                        $('<td></td>').text(val.name).append(
                                            $('<span class="id"></span>').text(val.id).hide()
                                        )
                                    )
                                ).append(
                                    $('<tr></tr>').append(
                                        $('<td></td>').text(val.location_number)
                                    )
                                ).draggable({
                                    containment: "parent",
                                    disabled: true
                                });
                        }



                        $(map).append(node);
                    });
                });
        });


    this.addNode = function (data) {
        var left = 5;
        var right = 5;
        var node = $('<table></table>').css({left:left+'px',top:top+'px'})
            .addClass(node_class)
            .append(
                $('<tr></tr>').append(
                    $('<td></td>').text(data.name).append(
                        $('<span class="id"></span>').text(data.id).hide()
                    )
                )
            ).append(
                $('<tr></tr>').append(
                    $('<td></td>').text(data.location_number)
                )
            ).draggable({
                containment: "parent"
            });
        $(map).append(node);
    }

    this.init = function (inventory_id,area_id) {

        $.post(base_url+"Inventory/getLocationPostPI",{inventory_id:inventory_id,area_id:area_id})
            .done(function (response) {
                response = JSON.parse(response);
                var left = 5;
                var top = 5;
                $.each(response.data,function(i,val){
                    var node = $('<table></table>').css({left:val.position_left+'px',top:val.position_top+'px'})
                        .addClass(node_class)
                        .append(
                            $('<tr></tr>').append(
                                $('<td></td>').text(val.name).append(
                                    $('<span class="id"></span>').text(val.id).hide()
                                )
                            )
                        ).append(
                            $('<tr></tr>').append(
                                $('<td></td>').text(val.location_number)
                            )
                        ).draggable({
                            containment: "parent",
                            disabled: !parm.drag
                        });
                    $(map).append(node);
                });
            });
    }

    this.clear = function()
    {
        $(map).find('.'+node_class).remove();
        $(".info_box").find(".department_name").text("None");
        $(".info_box").find(".area_name").text("None");
    }

    this.setInfo = function (data) {
        //console.log(data);
        if(data.department)
            $(".info_box").find(".department_name").text(data.department);
        if(data.area)
            $(".info_box").find(".area_name").text(data.area);
    }

    /*
    function Popup(data)
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title>my div</title>');
        /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
/*        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }*/
}

$(document).ready(function () {

    // var inventory_map = new loadMap({
    //         id:"#area_id",
    //         map_container:"#map-container",
    //         node_class:"map-node"
    // });

    // inventory_map.addNode({name:"Name",location_number:"Number"});

});