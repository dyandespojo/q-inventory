
$(document).ready(function () {
    var data_table;
    var operation;
    init("#data_table",{"table":"user"},[]);
    $("#data_table").on("click",".edit",
        function(){
            operation = "edit";
            var id = $(this).parent().find('.id').val();
            $("#edit_form").find("[name=smPortalUniqueUserID]").val(id);
            $.post("internal_user/getData",{id:id})
                .done(function (data) {
                    data = JSON.parse(data);
                    if(data.status)
                    {
                        $.each(data.data,function (i,value) {
                            $("#edit_form").find("[name="+i+"]").val(value);
                            //alert(i+"=>"+obj);
                        });
                        $("#myModal").modal("show");
                        //alert(data.title+" => "+data.message);
                    }
                    else
                    {
                        toastr.error(data.message,data.title);
                        //alert(data.title+" => "+data.message);
                    }
                }).fail(function () {
                    alert("Fail Request");
                });
        });


    $("#add").on("click",
        function(){
            operation = "add";
            $.each($("#edit_form").find("input"),function (i,value) {
                $(value).val("");
            });
            $("#myModal").modal("show");
        });


    $("#data_table").on("click",".delete",
        function(){
            var id = $(this).parent().find('.id').val();
            var row = $(this).parents("tr");
            $.confirm({
                title: 'Delete!',
                confirmButtonClass: 'btn-info',
                cancelButtonClass: 'btn-danger',
                confirmButton: 'Yes',
                cancelButton: 'No',
                confirm: function(){
                    $.post("internal_user/delete",{id:id}).done(function(data,status){
                        data = JSON.parse(data);
                        if(data.status=="ok")
                        {
                            toastr.success(data.message,data.title);
                            init("#data_table",{"table":"user"},[]);
                            /*
                            data_table
                                .row( row )
                                .remove()
                                .draw();*/
                        }
                        else if(data.status=="fail")
                        {
                            toastr.error(data.message,data.title);
                        }
                    }).fail(function(){});
                }
            });
        });

    $("#edit_form").submit(function(){
        var postData = $(this).serializeArray();
        var action;
        if(operation == "edit")
        {
            action = $(this).attr('action');
        }
        else
        {
            action = "internal_user/add";
        }
        $.post(action,postData)
            .done(function(data){
                data = JSON.parse(data);
                if(data.status == "ok")
                {
                    toastr.success(data.message,data.title);
                    init("#data_table",{"table":"user"},[]);
                    $("#myModal").modal("hide");
                }
                else
                {
                    toastr.error(data.message,data.title);
                }
            }).fail(function(){
                toastr.error("Request Fail");
                //alert("Request Fail");
            });
        return false;
    });

    function init(table,data,noSort)
    {
        $(table).dataTable().fnDestroy();
        data_table = $(table).DataTable({
            "ajax": {
                "url": "internal_user/load",
                "type": "post",
                "data": data
            },
            "processing": true,
            "serverSide": true,
            responsive: true,
            bsort: true,
            aoColumnDefs: [
                {
                    aTargets: noSort,
                    bSortable: false
                }
            ],
            //"order": [[ 0, sort ]],
            "createdRow": function (row, data, index) {
            },
            "autoWidth" : true,
            "preDrawCallback" : function() {},
            "fnInitComplete": function(oSettings, json) {},
            "rowCallback" : function(nRow) {},
            "drawCallback" : function(oSettings) {}
        });


    }

});



