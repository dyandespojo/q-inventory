var base_url = $("base").attr('href');
var hospital_id = $(".hospital_id").val();
var hospital_number;
var inventory_serial_id;
var dept_number;
$("#add-inventory").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    //autoFocus: true
    onStepChanging: function (event, currentIndex, newIndex){
        var nextStep = '#wizard #wizard-p-' + newIndex;
        var heightNextStep = $(nextStep).css('minHeight');
        $(nextStep).parent().animate({height:heightNextStep},200);
        return true;
    }
});


$(document).ready(function () {

    $("#area_id").change(function()
    {
        var id = $(this).val();
        var hospital_id = $("#pre_pl_form").find("[name='hospital[id]']").val();
        $.post(base_url+"Inventory/getLocation",{id:id,hospital_id:hospital_id}).done(function (data) {
            console.log(data);
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#location_id").empty();
                $("#location_id").append($("<option></option>").val("").text("Select"));
                $.each(data.data.locations, function (i,value) {
                     $("#location_id").append($("<option></option>").val(value.id).text(value.name));
                });
            }
            else if(data.status == "warn")
            {
                toastr.warning(data.message,data.title);
            }
            else
            {
                toastr.error(data.message,data.title);
            }
        });
    });


    $("#estimated_bins").focusout(function(){
        var result = 0;
        var hours = $("#estimated_bins_per_hours").val();
        if(hours != 0)
            result = this.value/hours;
        $("#estimate_count_hours").val(result);
        $("#estimated_count_min").val(result*60);
    });

    $("#estimated_bins_per_hours").focusout(function(){
        var result = 0;
        var hours = this.value;
        if(hours != 0)
            result = $("#estimated_bins").val()/hours;
        $("#estimate_count_hours").val(result);
        $("#estimated_count_min").val(result*60);
    });

    $("#dept_id").on("change",function()
    {
        var id = $(this).val();
        var name = $(this).find("option:selected").text();
        name = name.substr(0,3).toUpperCase();
        dept_number = name;
        $.post(base_url+"Inventory/getAreas",{id:id}).done(function (data) {
            //console.log(data);
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                //alert(data.data.loc_id);
                inventory_serial_id = parseInt(data.data.loc_id)+1;
                $("#pre_pl_form").find("[name='location[location_number]']").val(hospital_number+'-'+dept_number+'-'+inventory_serial_id);

                $("#area_id").empty();
                $("#location_id").empty();
                $("#area_id,#location_id").append($("<option></option>").val("").text("Select"));
                $.each(data.data.areas, function (i,value) {
                    $("#area_id").append($("<option></option>").val(value.id).text(value.name));
                });
            }
            else if(data.status == "warn")
            {
                toastr.warning(data.message,data.title);
            }
            else
            {
                toastr.error(data.message,data.title);
            }
        });
    });


    $('.pi_date')
        .datepicker({format: "yyyy-mm-dd"});

    $("#file").change(function(e) {
        var data = [];
        var ext = $("input#file").val().split(".").pop().toLowerCase();
        if($.inArray(ext, ["csv"]) == -1) {
            toastr.error("Please select file with .csv extention","Invalid File");
            return false;
        }
        if (e.target.files != undefined) {
            var reader = new FileReader();
            reader.onload = function(e) {
                var csvval = e.target.result.split("\n");
                var result = e.target.result;
                var columns = [];

                $("#counter_sheet thead tr").empty();
                $(csvval[0].split(",")).each(function (i,val) {
                    columns.push({title:val});
                });

                var data = [];
                var row;
                for(var i = 1;i < csvval.length;i++)
                {
                    row = csvval[0].split(",");
                    data.push(row);
                }
                $('#counter_sheet').DataTable( {
                    data: data,
                    columns: columns,
                    responsive: true
                } );
                $("#inventory_file_form button").prop("disabled",false);
            };
            reader.readAsText(e.target.files.item(0));
        }
        return false;
    });


    $("#inventory_status").change(function () {
        table.refreshTable(this.value);
    });

    var locations;
    var table = new Easy_curd({
        "table_id":"#inventory_data_table", //Table ID
        "action_column":2,
        "target":base_url+"Inventory/action", //Table URL for CURD
        "data":{
            hospital_id:hospital_id,
            action:
            {
                "edit":true,
                "delete":true,
                'check':false,
                'radio':false
            },
            custom:[
                '<button class="btn btn-xs btn-default view_item"><i class="fa fa-eye"></i></button>'
            ]
        }, //Action button such that edit and delete
        "add_btn":"#add",
        "form_id":"#edit_form", //Form ID
        //"InitDatatable": function (id,link) {},
        //"onRowClickCallback": function (id) {},
        "onCustomClickCallback":{
            "class":"view_item",
            "method": function (id,tag) {}
        },

        "onClickEdit": function (id) {

            $.post(base_url+"Inventory/getDetail",{id:id}).done(function (data) {
                data = JSON.parse(data);
                //console.log(data);
                /*
                if(data.status == "ok"||data.condition)
                {

                    $.each(data.data.inventory[0], function (i,value) {
                        $("#inventory_form").find('[name="inventory['+i+']"]').val(value);
                        if(i == "id")
                        {
                            $("#inventory_file_form").find(".id").val(value);
                            $("#pre_pl_form").find(".id").val(value);
                        }
                    });

                    if(data.condition)
                    {
                        $(".section-2").fadeIn();
                        $(".section-1").slideUp();
                        return;
                    }

                    $("#location_id").empty();
                    $("#location_id").append($("<option></option>").val("").text("Select"));
                    locations = data.data.locations;
                    $.each(data.data.locations, function (i,value) {
                        $("#location_id").append($("<option></option>").val(value.id).text(value.name));
                    });

                    $("#area_id").empty();
                    $("#area_id").append($("<option></option>").val("").text("Select"));
                    $.each(data.data.areas, function (i,value) {
                        $("#area_id").append($("<option></option>").val(value.id).text(value.name));
                    });


                    $.each(data.data.hospital,function(i,val){
                        if(i!="name")
                            $("#inventory_form").find("[name='hospital["+i+"]']").val(val);
                    });


                    $("#map-image").children().remove();
                    $.each(data.data.map,function(i,val){
                        $("#map-image").append(
                            $("<td></td>").attr("colspan",1)
                                .append(
                                $("<span></span>")
                                    .append(
                                    $("<img>")
                                        .attr("src",base_url+"map/"+val.src)
                                        .attr("width","100px")
                                )
                                    .append(
                                    $("<button></button>")
                                        .addClass("btn delete_map")
                                        .append(
                                        $("<i></i>")
                                            .addClass("glyphicon glyphicon-remove")
                                    ).click(function () {
                                            var id = $(this).parent().find(".id").val();
                                            var span = $(this).parents("span");
                                            $.confirm({
                                                title: 'Delete!',
                                                confirmButtonClass: 'btn-info',
                                                cancelButtonClass: 'btn-danger',
                                                confirmButton: 'Yes',
                                                cancelButton: 'No',
                                                confirm: function(){

                                                    $.post(base_url+"Inventory/DeleteMap",{id:id,operation:'delete'}).done(function(data,status){
                                                        data = JSON.parse(data);
                                                        if(data.status=="ok")
                                                        {
                                                            $(span).remove();
                                                            toastr.success(data.message,data.title);
                                                        }
                                                        else if(data.status=="fail")
                                                        {
                                                            toastr.error(data.message,data.title);
                                                        }
                                                    }).fail(function(){});
                                                }
                                            });
                                        })
                                )
                                    .append(
                                    $("<input>")
                                        .addClass("id")
                                        .prop("type","hidden")
                                        .val(val.id)
                                )
                            )
                        );
                    });

                    $.each(data.data.inventory[0], function (i,value) {
                        $("#pre_pl_form").find('[name="inventory['+i+']"]').val(value);
                    });
                    var result = 0;
                    var hours = $("#estimated_bins_per_hours").val();
                    if(hours != 0)
                        result = $("#estimated_bins").val()/hours;
                    $("#estimate_count_hours").val(result);
                    $("#estimated_count_min").val(result*60);

                    //$("#pre_pl_form").find('[name="inventory[location_id]"]').val(data.data.location_id);
                    $("#pre_pl_form").find('[name="location[location_number]"]').val(data.data.location_number);
                    $("#pre_pl_form").find('[name="area[id]"]').val(data.data.area_id);
                    $("#pre_pl_form").find('[name="dept[id]"]').val(data.data.dept_id);
                    if(data.data.hospital_id == 0)
                        data.data.hospital_id = "";
                    $("#inventory_form").find('[name="hospital[id]"]').val(data.data.hospital_id);
                    $("#pre_pl_form").find('[name="location[hospital_id]"]').val(data.data.hospital_id);

                    $(".section-2").fadeIn();
                    $(".section-1").slideUp();
                }
                else if(data.status == "warn")
                {
                    toastr.warning(data.message,data.title);
                }
                else
                {
                    toastr.error(data.message,data.title);
                }
                */
            });
        },
        "onClickEditPostCallback":function(data){
        }
        //"onRadioPostClickCallback": function (status,data) {},
        //"onRadioPreClickCallback": function () {},
        //"customCallback": function (id) {}
    });


    $("#start_visit").click(function () {
        //var hospital_id = $("#inventory_form").find("#hospital_id").val();
        if($(".hospital_id").val()=="")
        {
            $("#inventory_form").find("[name^=hospital],[name^=inventory]").val("");
        }
        else
        {
            hospital_number = $("#hospital_id").find('option:selected').text().substr(0,3).toUpperCase();
        }
        //$("#inventory_form").find("[name='hospital[id]']").val(hospital_id);
        /*
        $("#inventory_form").find("input,select").each(function (i,obj) {
            $(obj).val("");
        });*/
        $("#map-image").children().remove();
        var date = new Date();
        var dateString = date.getMonth()+"/"+date.getDate()+"/"+date.getFullYear();
        $("#inventory_form").find("[name='hospital[pi_date]']").val(dateString);

/*        var h_id = $(".hospital_id").val();
        $("#inventory_form").find("[name=hospital_id]").val(h_id);*/

        //initStep();
        $(".section-2").fadeIn();
        $(".section-1").slideUp();

    });


    $("#back_to_inventory").click(function () {
        table.refreshTable();
        if($.fn.DataTable.isDataTable( '#counter_sheet' ))
        $('#counter_sheet').dataTable().fnDestroy();
        $('#counter_sheet thead').empty();
        $('#counter_sheet tbody').empty();
        $("#inventory_file_form button").prop("disabled",true);
        $(".section-1").slideDown();
        $(".section-2").fadeOut();
        //$("#add-inventory").steps('destroy');
    });



    $("#inventory_form").submit(function () {
        var data = $(this).serializeArray();
        $.post(base_url+'Inventory/save',data).done(function (data) {
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                //inventory_serial_id = data.id;
                if(data.id!=true)
                {
                    $("#inventory_form").find("[name='inventory[id]']").val(data.id);
                    $("#inventory_form").find("[name='inventory[id]']").val(data.id);
                }
                $("#inventory_file_form").find(".id").val(data.id);
                $("#pre_pl_form").find("[name='inventory[id]']").val(data.id);
                $("#item_form").find("[name='inventory[id]']").val(data.id);
                $("#add-inventory").steps("next");
            }
            else
            {

            }
        }).fail(function () {
            alert("Request error");
        });
        //$("#add-inventory").steps("next");
        return false;
    });


    $("#inventory_file_form").submit(function () {
        $("#inventory_file_form button").prop("disabled",true);
        $.ajax({
            url: base_url+"/Inventory/uploadCSVfile", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                data = JSON.parse(data);
                if(data.status == "ok")
                {
                    $("#add-inventory").steps("next");
                }
                else
                {

                }
                $("#inventory_file_form button").prop("disabled",false);
            }
        });
        return false;
    });

    $("#pre_pl_form").submit(function () {

        $.ajax({
            url: base_url+"/Inventory/prePI", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data)   // A function to be called if request succeeds
            {
                data = JSON.parse(data);
                if(data.status == "ok")
                {
                    inventory_serial_id += 1;
                    $("#pre_pl_form").find("[name='location[location_number]']").val(hospital_number+'-'+dept_number+'-'+inventory_serial_id);
                    $("#pre_pl_form").find('[name^=location]').not('[name="location[area_id]"]').val("");
                    toastr.success("Location added successfully");
                    //$("#add-inventory").steps("next");
                }
                else
                {}
            }
        });
        return false;
    });


    $(".build_sheet").click(function () {
        var hospital_id = $(".hospital_id").val();
        $.post(base_url+"Inventory/getLocation",{"hospital_id":hospital_id},
            function (data) {
                data = JSON.parse(data);
                if(data.status == "ok")
                {
                    $("#count_location_number").children().remove();
                    $("#count_location_number").append($('<option></option>').val("").text("Select"));
                    $.each(data.data.locations, function (i,obj) {
                        $("#count_location_number").append($('<option></option>').val(obj.id).text(obj.location_number).attr("name",obj.name));
                    });
                    $("#add-inventory").steps("next");
                }
        });
    });


    $("#count_location_number").change(function () {
        $("#count_location_name").val($(this).find("option:selected").attr("name"));
    });



    $(".add_dept").on("click", function () {

        $("#deptModal").modal("show");

        $.post(base_url+"Inventory/getDepartments").done(function (data) {
            //console.log(data);
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#dept_suggest").empty();
                $.each(data.data.dept, function (i,value) {
                    $("#dept_suggest").append($("<option>").val(obj.name));
                });
            }
            else if(data.status == "warn")
            {
                toastr.warning(data.message,data.title);
            }
            else
            {
                toastr.error(data.message,data.title);
            }
        });


        $("#dept_form").find("input,textarea").each(function (i,obj) {
            $(obj).val("");
        });
    });

    $("#dept_form").submit(function () {
        var data = $(this).serializeArray();
        $.post(base_url+'Inventory/addDept',data).done(function (data) {
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#dept_id").empty();
                $("#dept_id").append($("<option></option>").val("").text("Select"));

                $("#area_id").empty();
                $("#area_id").append($("<option></option>").val("").text("Select"));

                $("#location_id").empty();
                $("#location_id").append($("<option></option>").val("").text("Select"));

                $("#dept_suggest").empty();

                $("#area_form").find('[name=dept_id]').empty();
                $("#area_form").find('[name=dept_id]').append($("<option></option>").val("").text("Select"));

                $("#location_form").find('[name=dept_id]').empty();
                $("#location_form").find('[name=dept_id]').append($("<option></option>").val("").text("Select"));
                $(data.dept).each(function(i,obj){
                    $("#dept_id").append($("<option></option>").val(obj.id).text(obj.name));
                    $("#area_form").find('[name=dept_id]').append($("<option></option>").val(obj.id).text(obj.name));
                    $("#location_form").find('[name=dept_id]').append($("<option></option>").val(obj.id).text(obj.name));
                    $("#dept_suggest").append($("<option>").val(obj.name));
                });
                $("#deptModal").modal("hide");
            }
            else
            {

            }
        }).fail(function () {
            alert("Request error");
        });
        return false;
    });

    $("#location_form").submit(function () {
        var data = $(this).serializeArray();
        $.post(base_url+'Inventory/addLocation',data).done(function (data) {
            data = JSON.parse(data);
            console.log(data);
            if(data.status == "ok")
            {
                $("#locationModal").modal("hide");
                $("#location_id").children().remove();
                $("#location_id").append($("<option></option>").val("").text("Select"));
                $.each(data.locations, function (i,value) {
                    $("#location_id").append($("<option></option>").val(value.id).text(value.name));
                });
                $("#location_id").val(data.id);
            }
        }).fail(function () {
            alert("Request error");
        });
        return false;
    });


    $(".add_area").on("click", function () {

        var dept = $("#dept_id").val();
        if(dept=="")
        {
            toastr.error("Please select Department");
            return false;
        }
        $("#areaModal").modal("show");
        $("#area_form").find("input,textarea").each(function (i,obj) {
            $(obj).val("");
        });

        var dept_id = $("#dept_id").val();
        $("#area_form").find('[name=dept_id]').val(dept_id);
        $.post(base_url+"Inventory/getAreas",{id:dept_id}).done(function (data) {
            //console.log(data);
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#area_suggest").empty();
                $.each(data.data.areas, function (i,value) {
                    $("#area_suggest").append($("<option>").val(obj.name));
                });

            }
            else if(data.status == "warn")
            {
                toastr.warning(data.message,data.title);
            }
            else
            {
                toastr.error(data.message,data.title);
            }
        });
    });

    $("#area_form").submit(function () {
        var data = $(this).serializeArray();
        $.post(base_url+'Inventory/addArea',data).done(function (data) {
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#area_id").empty();
                $("#area_id").append($("<option></option>").val("").text("Select"));

                $("#location_id").empty();
                $("#location_id").append($("<option></option>").val("").text("Select"));


                $(data.area).each(function(i,obj){
                    $("#area_id").append($("<option></option>").val(obj.id).text(obj.name));
                });
                $("#areaModal").modal("hide");

            }
            else
            {

            }
        }).fail(function () {
            alert("Request error");
        });

        return false;
    });

    $(".add_location").on("click", function () {

        var dept = $("#dept_id").val();
        var area = $("#area_id").val();
        if(dept=="")
        {
            toastr.error("Please select Department");
            return false;
        }

        if(area == "")
        {
            toastr.error("Please select Area");
            return false;
        }

        $("#locationModal").modal("show");

        $("#location_form").find("input,textarea").each(function (i,obj) {
            $(obj).val("");
        });
        $(".hospital_id").val($("#hospital_id").val());


        var dept_id = $("#dept_id").val();
        $("#location_form").find('[name=dept_id]').val(dept_id);
        $.post(base_url+"Inventory/getAreas",{id:dept_id}).done(function (data) {
            //console.log(data);
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#location_form").find("[name=area_id]").empty();
                $("#location_form").find("[name=area_id]").append($("<option></option>").text("Select"));
                $("#location_suggest").empty();
                $.each(data.data.areas, function (i,value) {
                    $("#location_form").find("[name=area_id]").append($("<option></option>").val(value.id).text(value.name));
                });
                $("#location_form").find("[name=area_id]").val($("#area_id").val());
            }
            else if(data.status == "warn")
            {
                toastr.warning(data.message,data.title);
            }
            else
            {
                toastr.error(data.message,data.title);
            }
        });
    });



    $("#location_form").find('[name=dept_id]').on("change", function () {
        var id = $(this).val();
        //alert(id);
        $.post(base_url+"Inventory/getAreas",{id:id}).done(function (data) {
            //console.log(data);
            data = JSON.parse(data);
            if(data.status == "ok")
            {
                $("#location_form").find('[name=area_id]').empty();
                $("#location_form").find('[name=area_id]').append($("<option></option>").val("").text("Select"));
                $.each(data.data.areas, function (i,value) {
                    $("#location_form").find('[name=area_id]').append($("<option></option>").val(value.id).text(value.name));
                });
            }
            else if(data.status == "warn")
            {
                toastr.warning(data.message,data.title);
            }
            else
            {
                toastr.error(data.message,data.title);
            }
        });
    });


    $("#location_id").on("change", function () {
        var id = $(this).val();
        if(id>0)
        $.post(base_url+"Inventory/getLocation",{id:id})
            .done(function (data) {
            data = JSON.parse(data);
                if(data.status == "ok")
                {
                    var location_number = data.data.locations[0].location_number;
                    $("#inventory_form").find("[name='location[location_number]']").val(location_number);
                }
                else
                {

                }
        }).fail(function () {
                alert("Request Fail");
            });
    });
    
    $("#hospital_id").on("change", function () {

        var id = this.value;
        var name = $(this).find("option:selected").text();
        name = name.substr(0,3).toUpperCase();
        hospital_number = name;
        $("#pre_pl_form").find('[name="location[hospital_id]"]').val(id);

        if(id.length==0)
        {
            $("#inventory_form").find("[name^='hospital']").val("");
        }
        else
        $.post(base_url+"Inventory/getHospital",{id:id})
            .done(function (data) {
                data = JSON.parse(data);
                if(data.status == "ok")
                {
                    var hospital = data.data[0];
                    $.each(hospital,function(i,val){
                        $("#inventory_form").find('[name="hospital['+i+']"]').val(val);
                    });
                }
                else
                {}

            }).fail(function () {
                alert("Request Fail");
            });
    });

    $("#item_form").submit(function () {
        var data = $(this).serializeArray();
        $.post(base_url+"Inventory/addItem",data)
            .done(function (data) {
                data = JSON.parse(data);
                if(data.status == "ok")
                {

                }
                else
                {

                }
            }).fail(function () {
                alert("Request Fail");
            });
        return false;
    });

    function initCountSheet(data)
    {
        $("#count_counter_sheet").dataTable().fnDestroy();
        var data_table = $("#count_counter_sheet").DataTable({
            "ajax": {
                "url": base_url+"Inventory/CountAction",
                "type": "post",
                "data": data
            },
            "processing": true,
            "serverSide": true,
            responsive: true,
            bsort: true,
            //"order": [[ 0, sort ]],
            "createdRow": function (row, data, index) {
            },
            "autoWidth" : true,
            "preDrawCallback" : function() {},
            "fnInitComplete": function(oSettings, json) {
                if(json.recordsFiltered==0)
                {
                    toastr.warning("No item found")
                }
            },
            "rowCallback" : function(nRow) {},
            "drawCallback" : function(oSettings) {}
        });
    }


    $(".extra_search_btn").click(function () {

        var item_number = $(this).parents(".extra_search").find('[name=mfg_item_number]').val();
        if(item_number.length>0)
        {
            $.post(base_url+"Inventory/getItem",{'operation':'get','item_num':item_number})
                .done(function(data){
                    data = JSON.parse(data);
                    //alert(data.data.manufacture_item_number);
                    if(data.status == "ok")
                    {
                        $.confirm({
                            title: 'Item found!',
                            content:'Do you want to add Item?',
                            confirmButtonClass: 'btn-info',
                            cancelButtonClass: 'btn-info',
                            confirmButton: 'Yes',
                            cancelButton: 'No',
                            confirm: function(){
                                $(this).parents(".extra_search").find('[name=mfg_item_number]').val("");
                            }
                        });

                        initCountSheet({operation:'load',item_num:item_number});

                    }
                    else
                    {
                        toastr.warning(data.message,data.title);
                        $('[name=multiple_mfg_item_number]').focus();
                    }
                })
                .fail(function () {

                });
        }
    });



    $(".advanced_search_btn").click(function () {


        var item = $("[name=multiple_mfg_item_number]").val();
        var description = $("[name=multiple_mfg_item_description]").val();
        var vendor = $("[name=multiple_vendor_item_number]").val();
        var dataLoad = {operation:"load"};
        if(item.length>0)
        {
            dataLoad.multi_item_num = item;
        }

        if(description.length>0)
        {
            dataLoad.description = description;
        }

        if(vendor.length>0)
        {
            dataLoad.vendor = vendor;
        }

        if(item.length>0||description.length>0||vendor.length>0)
        {
            $.post(base_url+"Inventory/getItem",dataLoad)
                .done(function(data){
                    data = JSON.parse(data);

                    if(data.status == "ok")
                    {
                        //var name = data.data.manufacture_item_number;
                        initCountSheet(dataLoad);

                        /*$.confirm({
                         title: 'Item found!',
                         content:'Do you want to add Item?',
                         confirmButtonClass: 'btn-info',
                         cancelButtonClass: 'btn-info',
                         confirmButton: 'Yes',
                         cancelButton: 'No',
                         confirm: function(){
                         $(this).parents(".extra_search").find('[name=mfg_item_number]').val("");
                         }
                         });*/
                    }
                    else
                    {
                        toastr.warning(data.message,data.title);
                    }
                })
                .fail(function () {

                });
        }
        else
        {
            toastr.warning("Please mention at least one search parameter");
        }

    });


});



