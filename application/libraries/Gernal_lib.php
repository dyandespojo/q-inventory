<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class Gernal_lib {

	function __construct()
    {

	}
    function get_table_head($my_array)
    {
        $converted='';
        
       foreach($my_array[0] as $key => $value)
        {
           $converted=$converted.'<td>'.$key.'</td>';      
        }
        $converted=$converted.'<td></td>';
        return $converted;
    }
	function  get_table_data($my_array,$id="-1",$read="#",$update="#",$delete="#")
    {
            $converted='';
        $i=0;
       foreach($my_array as $value)
        {
           $converted=$converted.'<tr>';
           foreach($value as $value1)
           {
           $converted=$converted."<td>".$value1."</td>";      
           }         
           $converted=$converted.'<td><a href="'.$read.'" id="'.$value[$id].'" class="action-icon"><i class="fa fa-eye"></i></a><a href="'.$update.'" id="'.$value[$id].'" class="action-icon"><i class="fa fa-pencil-square-o"></i></a><a href="'.$delete.'" id="'.$value[$id].'" class="action-icon delete-action"><i class="fa fa-trash-o"></i></td></tr>';
$i++;
        }
        return $converted;
    }
    
    function  get_dropdown_data($my_array,$id,$name,$dropdown_name)
    {
            $converted="<select name=".$dropdown_name.">";
       foreach($my_array as $value)
        {
           $converted=$converted."<option value=".$value[$id].">".$value[$name]."</option>";

        }
        $converted=$converted."</select>";
        return $converted;
    }
    
  
}