<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class easy_curd{

    private $tables;
    private $CI;
    private $load_data;
    private $form;
    private $return = "none";

    public function __construct($config)
    {
        //parent::__construct();
        $this->CI =& get_instance();
        $this->tables = $config['table'];
        $this->load_data = $config['load_data'];

        if(isset($config['return']))
            $this->return = $config['return'];

        $this->CI->load->model('Easy_curd_model');
        $this->CI->Easy_curd_model->set($this->tables);
        $this->form = $config['columns'];
    }

    public function index($parm)
    {
        $data = $_POST;
		//echo  json_encode($data);
		//exit;
		if(isset($parm['return'])&&$parm['return']=="temp")
		{
				$this->tables = $parm['table'];
				$this->load_data['table'] = $parm['table'];
				$this->CI->Easy_curd_model->set($parm['table']);
		}
		
        $response = ['status'=>'ok','title'=>'Success','message'=>'Successfully initialize'];
        if(isset($data['operation']))
        {
            $operation = $data['operation'];
            switch($operation)
            {
                case "add":
                    $response = $this->add($data);
                    break;
                case "delete":
                    $response = $this->delete($data);
                    break;
                case "update":
                    $response = $this->update($data);
                    break;
                case "get":
                    $response = $this->get($data);
                    break;
                case "load":
                    $response = $this->load($data);
                    break;
                case "form":
                    $response['form'] = $this->CI->Easy_curd_model->getFieldDetail($this->form);
                    break;
                default:
                    $response = ['status'=>'fail','title'=>'Operation','message'=>'Operation not valid'];
                    break;
            }
        }
        else
        {
            $response = ['status'=>'fail','title'=>'Operation','message'=>'Operation not define'];
        }

		

        if($this->return=="none")
        {
            echo json_encode($response);
        }
        else{
            return $response;
        }

    }


	private function get($data)
    {
        $response = ['status'=>'ok','title'=>'Get Data','message'=>'Data get successfully'];
        if(isset($data['id']))
        {
            $id = $data['id'];
            $result = $this->CI->Easy_curd_model->getById($id);
            if($result)
            {
                $response['data'] = $result;
            }
            else
            {
                $response['status'] = 'fail';
                $response['message'] = 'Data not found';
            }
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select row';
        }
        return $response;
    }


    private function add($data)
    {
        $response = ['status'=>'ok','title'=>'Insert Data','message'=>'Data inserted successfully'];
        unset($data['id']);
        unset($data['operation']);
        $result = $this->CI->Easy_curd_model->insert($data);

        if(!$result)
        {
            $response['status'] = 'fail';
            $response['message'] = 'Data not added';
        }
        return $response;
    }

    private function update($data)
    {
        $response = ['status'=>'ok','title'=>'Get Data','message'=>'Update successfully'];
        if(isset($data['id']))
        {
            $id = $data['id'];
            unset($data['operation']);
            $result = $this->CI->Easy_curd_model->update($id,$data);

            if(!$result)
            {
                $response['status'] = 'fail';
                $response['message'] = 'Data not update';
            }
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select row';
        }
        return $response;
    }

    private function delete($data)
    {
        $response = ['status'=>'ok','title'=>'Delete','message'=>'Data deleted successfully'];
        if(isset($data['id']))
        {
            $id = $data['id'];
            $result = $this->CI->Easy_curd_model->delete($id);
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select row';
        }

        return $response;
    }



    public function load($data)
    {
        if(isset($data['action']))
        {
            $action = $data['action'];
        }
        else
        {
            $action = "none";
        }

        if(isset($data['custom']))
        {
            $custom_buttons = $data['custom'];
        }
        else
        {
            $custom_buttons = "none";
        }

        if(isset($data['action_column']))
        {
            $action_column = $data['action_column'] - 1;
        }
        else
        {
            $action_column = 1;
        }
        $i = 0;
        $prefix = 0;

        if($this->load_data['type'] == 'plainQuery') {
            $columns = [];
            $i = 0;
            foreach ($this->load_data['col'] as $item) {
                array_push($columns, ['db' => $item, 'dt' => $i++]);
            }
            array_push($columns, ['db' => $this->load_data['primaryKey'], 'dt' => $i++]);
        }
        else if($this->load_data['type'] == 'joinQuery') {
            $columns = [];
            $i = 0;
            foreach ($this->load_data['col'] as $item) {
                $split = explode('.',$item);
                //echo $item." ";
                array_push($columns, ['db' => $item, 'dt' => $i++,'name'=>$split[1]]);
            }
            $split = explode('.',$this->load_data['primaryKey']);
            array_push($columns, ['db' => $this->load_data['primaryKey'], 'dt' => $i++,'name'=>$split[1]]);
        }
        else if($this->load_data['type'] == 'subQuery') {
            $columns = [];
            $i = 0;
            foreach ($this->load_data['col'] as $item) {
                if(isset($item['as']))
                {
                    array_push($columns, ['db' => $item['db'],'as'=>$item['as'], 'dt' => $i++,'name'=>$item['name']]);
                }
                else
                {
                    array_push($columns, ['db' => $item['db'], 'dt' => $i++,'name'=>$item['name']]);
                }
            }
            $split = explode('.',$this->load_data['primaryKey']);
            array_push($columns, ['db' => $this->load_data['primaryKey'], 'dt' => $i++,'name'=>$split[1]]);
        }

        $sql_details = array(
            'user' => $this->CI->db->username,
            'pass' => $this->CI->db->password,
            'db'   => $this->CI->db->database,
            'host' => $this->CI->db->hostname
        );

        $this->CI->load->library("ssp");

        $response  = $this->CI->ssp->simple( $data, $sql_details, $this->load_data['table'], $this->load_data['primaryKey'], $columns,$this->load_data['where'] );


        if($action != "none")
        {
            foreach ($response['data'] as $key => $value) {
                $index = 1;
                $check = false;
                $id = $response['data'][$key][count($value)-1];
                //echo $id.' ';
                unset($response['data'][$key][count($value)-1]);
                $input = '<input class="id" type="hidden" name="id" value="'.$id.'"/>';

                if(isset($action["edit"]) && $action["edit"] == "true")
                {
                    $check = true;
                    $input .= '<button class="btn btn-xs btn-default edit" 
                                    rel="tooltip" 
                                    data-placement="bottom" 
                                    data-original-title="Edit">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    </button>&nbsp;';
                }

                if(isset($action["delete"]) && $action["delete"] == "true")
                {
                    $check = true;
                    $input .= '<button class="btn btn-xs btn-default delete" rel="tooltip" 
                                                        data-placement="bottom" 
                                                        data-original-title="Delete"><i class="glyphicon glyphicon-remove"></i></button>&nbsp;';
                }

                if(isset($action['radio']) && $action['radio'] == "true")
                {
                    $check = true;
                    $input .= '<input type="radio" class="select" name="select' . $prefix . '" value="' . $id . '">';
                }

                if(isset($action['check']) && $action['check'] == "true")
                {
                    $check = true;
                    $input .= '<input type="checkbox" class="select" name="select" value="'.$id.'">';
                }

                if($custom_buttons != "none"&&count($custom_buttons)>0)
                {
                    $check = true;
                    foreach($custom_buttons as $btn)
                    {
                        $input .= $btn;
                    }

                }

                if($check) {

                    //echo (count($response['data'][$key])-1) .'<'. $action_column;
                    //exit;
                    /*if((count($response['data'][$key])-1) > $action_column)
                    {*/
                        $res = $this->array_insert($response['data'][$key], $input, $action_column);
                        $response['data'][$key] = $res;
                    /*}
                    else
                    {
                        $res = $this->array_insert($response['data'][$key], $input, $action_column);
                        $response['data'][$key] = $res;
                    }
                    /*else if((count($response['data'][$key])-1) == $action_column)
                    {
                        $res = $this->array_insert($response['data'][$key], $input, $action_column);
                        $response['data'][$key] = $res;
                    }*/
                }
                else
                {
                    $row  = $response['data'][$key][0];
                    $response['data'][$key][0] = $row.$input;
                }
            }
        }
        return $response;
    }


    function array_insert($array, $insert, $position) {
        settype($array, "array");
        settype($insert, "array");
        settype($position, "int");

        //if pos is start, just merge them
        if($position==0) {
            $array = array_merge($insert, $array);
        } else {


            //if pos is end just merge them
            if($position > (count($array)-1)) {
                $array = array_merge($array, $insert);
            } else {
                //split into head and tail, then merge head+inserted bit+tail
                $head = array_slice($array, 0, $position);
                $tail = array_slice($array, $position);
                $array = array_merge($head, $insert, $tail);
            }
        }
        return $array;
    }

}