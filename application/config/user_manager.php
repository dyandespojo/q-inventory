<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

////////////////////////////////////////
//////USER MANAGER FOR CODEIGNITER//////
/////////////version 1.0.0//////////////
////////////////////////////////////////
///written by - Anuradha Jayathilaka
///email - me@anu3d.info
///web - www.anu3d.info
////////////////////////////////////////
//This code is free to use in any project.
//please leave this information if you're using this. thanks :)
////////////////////////////////////////


/*
settings for new users
*/
$config['um_accountactive']=false;
$config['um_accountblocked']=false;
$config['um_profileprivacy']='public';
$config['um_appearonline']=1;

/*
users who didnt show any activity more than this time will be considered as logged out 
value is in minutes. works only when the hook is enabled.900=15mins
*/
$config['um_login_timeout']=900;

//default country that will be selected on the registration form.
$config['um_default_country']='LK';

//email settings
$config['um_email_from']='qinventry@ideaspromoter.com';
$config['um_email_from_name']='Q_inventry';
$config['um_email_activate_subject']='Activate your account';
$config['um_email_reset_subject']='Did you requested a password reset?';

/*
image processing
*/
$config['um_img_dp_path']='./dps/';
$config['um_img_pics_path']='./pics/';
$config['um_img_allowed_types']='gif|jpg';
$config['um_img_max_size']=1000;
$config['um_img_max_width']=1000;
$config['um_img_max_height']=1000;

$config['um_img_dp_thumb_w']=75;
$config['um_img_dp_thumb_h']=75;


/*
this array defines the rules for the registration form
you may add your own. but it should match the database and the model
*/
$config['um_register_rules']=array(
               array(
                     'field'   => 'username', 
                     'label'   => 'Username', 
                     'rules'   => 'trim|required|is_unique[users.username]|max_length[20]'
                  ),
               array(
                     'field'   => 'password', 
                     'label'   => 'Password', 
                     'rules'   => 'trim|required|min_length[6]'
                  ),
			 array(
                     'field'   => 'password2', 
                     'label'   => 'Password2', 
                     'rules'   => 'trim|required|min_length[6]|matches[password]'
                  ),
               array(
                     'field'   => 'email', 
                     'label'   => 'Email', 
                     'rules'   => 'trim|required|is_unique[users.email]'//valid_email
                  ),   
               array(
                     'field'   => 'firstname', 
                     'label'   => 'Firstname', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'secondname', 
                     'label'   => 'Secondname', 
                     'rules'   => 'trim'
                  ),
               array(
                     'field'   => 'lastname', 
                     'label'   => 'Lastname', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'dateofbirth', 
                     'label'   => 'Date of birth', 
                     'rules'   => 'trim|required'
                  )
            );

/*
this array defines the rules for the password reset form 2
*/
$config['um_pwd_reset_rules']=array(
               array(
                     'field'   => 'password', 
                     'label'   => 'Password', 
                     'rules'   => 'trim|required|min_length[6]|max_length[10]'
                  ),
			 array(
                     'field'   => 'password2', 
                     'label'   => 'Password2', 
                     'rules'   => 'trim|required|min_length[6]|max_length[10]|matches[password]'
                  ),
               array(
                     'field'   => 'email', 
                     'label'   => 'Email', 
                     'rules'   => 'trim|required'//valid_email|xss_clean
                  ),   
			   array(
                     'field'   => 'token', 
                     'label'   => 'token', 
                     'rules'   => 'trim|required'
                  )
            );

/*
this array defines the rules for the profile editor
*/
$config['um_profile_rules']=array(
               array(
                     'field'   => 'password', 
                     'label'   => 'Password', 
                     'rules'   => 'trim|min_length[6]'
                  ), 
			 array(
                     'field'   => 'password2', 
                     'label'   => 'Password2', 
                     'rules'   => 'trim|min_length[6]|matches[password]'
                  ),
               array(
                     'field'   => 'email', 
                     'label'   => 'Email', 
                     'rules'   => 'trim|required|callback_check_unique_pass'//valid_email|xss_clean
                  ),   
               array(
                     'field'   => 'firstname', 
                     'label'   => 'Firstname', 
                     'rules'   => 'trim|required'
                  ),
               array(
                     'field'   => 'secondname', 
                     'label'   => 'Secondname', 
                     'rules'   => 'trim'
                  ),
               array(
                     'field'   => 'lastname', 
                     'label'   => 'Lastname', 
                     'rules'   => 'trim|required'
                  ),
			   array(
                     'field'   => 'country', 
                     'label'   => 'Country', 
                     'rules'   => 'trim|required'
                  ),
			   array(
					 'field'   => 'interests', 
                     'label'   => 'Interests', 
                     'rules'   => 'trim'
                  ),
			   array(
					 'field'   => 'address', 
                     'label'   => 'Address', 
                     'rules'   => 'trim'
                  ),
               array(
                     'field'   => 'dateofbirth', 
                     'label'   => 'Date of birth', 
                     'rules'   => 'trim|required'
                  )
            );

/* login */
$config['um_login_rules']=array(
                 array(
                     'field'   => 'username', 
                     'label'   => 'Username', 
                     'rules'   => 'trim|required'
                  ),
             
                 array(
                     'field'   => 'password', 
                     'label'   => 'Password', 
                     'rules'   => 'trim|required|min_length[6]'
                  ),
			 );
