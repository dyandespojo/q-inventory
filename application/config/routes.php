<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'user_manager_controller/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['user'] = 'userController';
$route['datafiles'] = 'uploadfilesControler';
$route['uploadthefile'] = 'uploadfilesControler/do_upload';
$route['uploadqifile'] = 'qifilesControler/do_upload';
$route['qifiles'] = 'qifilesControler';
$route['deshboard'] = 'deshboard';
$route['deshboard_onhand'] = 'deshboard/onhand';
$route['deshboard_expired'] = 'deshboard/expired';
$route['deshboard_supplier'] = 'deshboard/supplier';
$route['deshboard_data'] = 'deshboard/data';
$route['deshboard_supplier_data'] = 'deshboard/supplier_data';
$route['deshboard_top_supplier_data'] = 'deshboard/supplier_data_top';

$route['register'] = 'user_manager_controller/register'; 
$route['activate'] = 'user_manager_controller/activate'; 
$route['login'] = 'user_manager_controller/login';
$route['logout'] = 'user_manager_controller/logout';
$route['profile'] = 'user_manager_controller/show_profile';
$route['editprofile'] = 'user_manager_controller/edit_profile'; 
$route['resetpass'] = 'user_manager_controller/reset_pass';
$route['reset'] = 'user_manager_controller/reset';


$route['userlist'] = 'admin_controller/user';
$route['userblock'] = 'admin_controller/userblock';


$route['admin_addhospital'] = 'admin_controller/addHospital';
$route['admin_hospitals'] = 'admin_controller/hospital';
$route['admin_heads'] = 'admin_controller/hospital_table_head';
$route['admin_data'] = 'admin_controller/hospital_table_data';
$route['del_hospitals'] = 'admin_controller/del_hospital';
$route['admin_inventry'] = 'admin_controller/inventry';

$route['admin'] = 'admin_controller';
$route['client'] = 'client_controller';
$route['counter'] = 'Counter_hospitals';


