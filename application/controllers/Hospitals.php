<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hospitals extends CI_Controller
{

    private $data;
    private $js = 0;
    private $css = 0;

    public function __construct()
    {
        parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }

        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        $user = $this->data['user'];
        if($user['userlvl']==3)
        {
            redirect("/counter");
        }
        if($user['userlvl']==2)
        {
         if($this->Um_users_model->get_user_licensed_status($username)!='yes')
         {
            redirect("client");
         } 
        }
        $this->data['css'][$this->css++] = "assets/css/custom/internal_user.css";
        $this->data['js'][$this->js++] =  base_url("assets/js/easy_curd.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/custom/hospital.js");
    }

    public function index()
    {
        $this->load->helper('form');
        $this->load->view('hospitals/index', $this->data);
    }

    public function action()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Name','field' => 'name'],
            ['label'=>'Email','field' => 'email'],
            ['label'=>'Phone','field' => 'phone'],
            ['label'=>'Address','field' => 'address'],
            ['label'=>'City','field' => 'city'],
            ['label'=>'State','field' => 'state'],
            ['label'=>'Zip','field' => 'zip'],
            ['label'=>'Contact Name','field' => 'contact_name'],

        ];


       $load_data = [
            'type' => 'subQuery',
            'primaryKey' => 'h.id',
            'col' => [
                ['db'=>'h.id','name'=>'id'],
                ['db'=>'h.name','name'=>'name'],
                ['db'=>'h.email','name'=>'email'],
                ['db'=>'h.phone','name'=>'phone'],
                ['db'=>'(select username from users where userlvl = 2 and h.client_id = users_id)','as'=>'client_id','name'=>'client_id'],
                ['db'=>'(select username from users where userlvl = 3 and h.counter_id = users_id)','as'=>'counter_id','name'=>'counter_id']
            ],
            'table' => 'hospital as h',
            'where' =>''
        ];

        if( $this->data['user']['userlvl']==2 )
        {
            $load_data['where'] = "h.client_id = ".$this->data['user']['users_id'];
        }
        else if( $this->data['user']['userlvl']==3 )
        {
            if(isset($_POST['action']))
            {
                $_POST['action']['edit'] = "false";
                $_POST['action']['delete'] = "false";
            }
            $load_data['where'] = "h.counter_id = ".$this->data['user']['users_id'];
        }

 

        $this->load->library('easy_curd',['table'=>'hospital','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }
}