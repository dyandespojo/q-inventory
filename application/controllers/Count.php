<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Count extends CI_Controller
{

    private $data;
    private $js = 0;
    private $css = 0;

    public function __construct()
    {
        parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }

        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        $this->data['css'][$this->css++] = base_url("assets/css/jquery.steps.css");
        $this->data['css'][$this->css++] = base_url("assets/plugins/datepicker/css/bootstrap-datepicker.css");

        $this->data['js'][$this->js++] =  base_url("assets/plugins/datepicker/js/bootstrap-datepicker.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/jquery.steps.min.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/easy_curd.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/custom/inventoryCount.js");
    }

    public function index()
    {
        $this->load->helper('form');
        $this->load->view('hospitals/index', $this->data);
    }

    public function inventoryList($id = 0)
    {
        $this->load->helper('form');
        $this->data['hospital_id'] = $id;
        $this->load->model("Hospital_model");
        $this->load->model("Department_model");
        $this->load->model("Area_model");
        $this->load->model("Location_model");
        $this->data['departments'] = $this->Department_model->getAll();
        $this->data['hospital'] = $this->Hospital_model->get_all_hospital();
        $this->data['selected_hospital'] = "";
        if($id!=0)
        {
            $data = $this->Hospital_model->getByID($id);
            $this->data['selected_hospital'] = $data[0];
        }
        $this->data['area'] = $this->Area_model->getAll();
        $this->data['location'] = $this->Location_model->getAll();
        $this->load->view('admin/inventory/count', $this->data);
    }

    public function getDetail()
    {
        $data = $_POST;
        //$data['id'] = 1;
        $response = ['status'=>'ok','title'=>'Get Data','message'=>'Detail retrieved'];
        if(isset($data['id']))
        {
            $id = $data['id'];
            $this->load->model(["Inventory_model","Area_model","Department_model","Hospital_model","Location_model","Map_model"]);
            $inventory = $this->Inventory_model->getByID($id);
            $response['data']['inventory'] = $inventory;
            if(count($inventory)>0)
            {
                $location = $this->Location_model->getByID($inventory[0]['location_id']);
                if(count($location)>0)
                {
                    $response['data']['map'] = $this->Map_model->getByLocationID($location[0]['id']);
                    $response['data']['location_id'] = $location[0]['id'];
                    $response['data']['location_number'] = $location[0]['location_number'];

                    if($location[0]['hospital_id']!=0)
                    {
                        $hospital = $this->Hospital_model->getByID($location[0]['hospital_id']);
                        $response['data']['hospital_id'] = $hospital[0]['id'];
                        $response['data']['hospital'] = $hospital[0];
                    }
                    else
                    {
                        $response['data']['hospital_id'] = 0;
                        $response['data']['hospital'] = [];
                    }

                    $area = $this->Area_model->getByID($location[0]['area_id']);
                    //echo json_encode($area);
                    //exit;
                    if(count($area)>0) {
                        $response['data']['area_id'] = $area[0]['id'];
                        $dept = $this->Department_model->getByID($area[0]['dept_id']);
                        if(count($dept)>0) {
                            $response['data']['dept_id'] = $dept[0]['id'];
                            $response['data']['areas'] = $this->Area_model->getByDetp($response['data']['dept_id']);
                            $response['data']['locations'] = $this->Location_model->getByCondition(['area_id'=>$response['data']['area_id'],'hospital_id'=>$response['data']['hospital_id']]);
                        }else
                        {
                            $response['status'] = "warn";
                            $response['message'] = "No inventory related department found";
                        }
                    }else
                    {
                        $response['status'] = "warn";
                        $response['message'] = "No inventory related area found";
                    }
                }
                else
                {
                    $response['status'] = "ok";
                    $response['message'] = "No inventory related location found";
                    $response['condition'] = "no location";
                }
            }else
            {
                $response['status'] = "warn";
                $response['message'] = "No inventory found";
            }

        }
        else
        {
            $response['status'] = "fail";
            $response['message'] = "Please select inventory";
        }

        echo json_encode($response);
    }




    function getHospital()
    {
        $data = $_POST;
        //$data['id'] = 1;
        $response = ['status'=>'ok','title'=>'Get Data','message'=>'Detail retrieved'];
        if(isset($data['id']))
        {
            $id = $data['id'];
            $this->load->model(["Hospital_model"]);
            $fetch = $this->Hospital_model->getByID($id);
            $response['data'] = $fetch;
        }
        else
        {
            $response['status'] = "fail";
            $response['message'] = "Please select inventory";
        }
        echo json_encode($response);
    }

    function getAreas()
    {
        $data = $_POST;
        //$data['id'] = 1;
        $response = ['status'=>'ok','title'=>'Get Data','message'=>'Detail retrieved'];
        if(isset($data['id']))
        {
            $id = $data['id'];
            $this->load->model(["Area_model","Location_model"]);
            $area = $this->Area_model->getByDetp($id);
            $last = $this->Location_model->getLastId();
            if(count($last)==0)
            {
                $last = 0;
            }
            else
            {
                $last = $last[0]['id'];
            }
            $response['data']['loc_id'] = $last;
            $response['data']['areas'] = $area;
        }
        else
        {
            $response['status'] = "fail";
            $response['message'] = "Please select inventory";
        }
        echo json_encode($response);
    }

    function getItem()
    {
        $data = $_POST;
        //$data['id'] = 1;
        $response = ['status'=>'ok','title'=>'Search Item','message'=>'Item found'];

        if(isset($data['multi_item_num'])||isset($data['description'])||isset($data['vendor']))
        {
            $this->load->model(["Items_model"]);
            $condition = [];
            if(isset($data['multi_item_num']))
            {
                $data['multi_item_num'] = preg_replace('/[^A-Za-z0-9]/', '', $data['multi_item_num']);
                $condition['manufacture_item_number'] = $data['multi_item_num'];
            }
            if(isset($data['description']))
            {
                $condition['manufacture_item_description'] = $data['description'];
            }
            if(isset($data['vendor']))
            {
                $condition['vendor_item_no'] = $data['vendor'];
            }

            $items = $this->Items_model->getByLike($condition);

            if(count($items)==0)
            {
                $response['status'] = "warn";
                $response['message'] = "Item not found";
            }
            else
            {
                $response['data'] = $items;
            }
        }
        else if(isset($data['item_num']))
        {
            $this->load->model(["Items_model"]);
            $data['item_num'] = preg_replace('/[^A-Za-z0-9]/', '', $data['item_num']);
            $items = $this->Items_model->getByCondition(['manufacture_item_number'=>$data['item_num']]);
            if(count($items)==0)
            {
                $response['status'] = "warn";
                $response['message'] = "Item not found";
            }
            else
            {
                $response['data'] = $items[0];
            }
        }
        else
        {
            $response['status'] = "fail";
            $response['message'] = "Please select inventory";
        }
        echo json_encode($response);
    }

    function getDepartments()
    {
        $data = $_POST;
        //$data['id'] = 1;
        $response = ['status'=>'ok','title'=>'Get Data','message'=>'Detail retrieved'];

            $id = $data['id'];
            $this->load->model(["Department_model"]);
            $dept = $this->Department_model->getAll();
            $response['data']['dept'] = $dept;
        if(!$response['data']['dept'])
        {
            $response['status'] = "fail";
            $response['message'] = "No data found";
        }
        echo json_encode($response);
    }

    function getLocation()
    {
        $data = $_POST;
        //$data['id'] = 1;
        $response = ['status'=>'ok','title'=>'Get Data','message'=>'Detail retrieved'];
        $this->load->model(["Location_model"]);
        if(isset($data['id']))
        {
            $id = $data['id'];
            if(isset($data['hospital_id']))
            {
                $locations = $this->Location_model->getByCondition(['area_id'=>$id,'hospital_id'=>$data['hospital_id']]);
            }
            else
            {
                $locations = $this->Location_model->getByArea($id);
            }

            $response['data']['locations'] = $locations;
        }
        else if(isset($data['hospital_id']))
        {
            $locations = $this->Location_model->getByCondition(['hospital_id'=>$data['hospital_id']]);
            $response['data']['locations'] = $locations;
        }
        else
        {
            $response['status'] = "fail";
            $response['message'] = "Please select inventory";
        }

        echo json_encode($response);
    }

    public function save()
    {
        $data = $_POST;
        $this->load->model(["Inventory_model","Hospital_model"]);
        $response = ['status'=>'ok','title'=>'Add Inventory','message'=>'Inventory added successfully'];
        if(isset($data['inventory']['name'])&&isset($data['hospital']))
        {
            $id = $data['inventory']['id'];
            if($id == "")
            {
                unset($data['inventory']['id']);
                $result = $this->Inventory_model->insert($data['inventory']);
                //$result = 4;
            }
            else
            {
                $result = $this->Inventory_model->update($id,$data['inventory']);
                $result = "update";
                $response['id'] = $id;
            }

            if(isset($data['hospital']))
            {
                $this->Hospital_model->update($data['hospital']['id'],$data['hospital']);
            }

            if(!$result)
            {
                $response['status'] = 'fail';
                $response['message'] = 'Data not update';
            }else if($result != "update")
            {
                $response['id'] = $result;
            }
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please mention detail of inventory';
        }
        echo json_encode($response);
    }


    public function addDept()
    {
        $data = $_POST;
        $this->load->model("Department_model");
        $response = ['status'=>'ok','title'=>'Add Department','message'=>'Department added successfully'];
        if(!isset($data['id']))
        {
            $result = $this->Department_model->insert($data);
            $response['title'] = "Edit Department";
            $response['message'] = "Department edited";
            $response['dept'] = $this->Department_model->getAll();
        }
        else if(isset($data['id']))
        {
            $result = $this->Department_model->update($data['id'],$data);
            $response['title'] = "Edit Department";
            $response['message'] = "Department edited";
            $response['dept'] = $this->Department_model->getAll();
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please';
        }
        echo json_encode($response);
    }


    public function addLocation()
    {
        $data = $_POST;
        $this->load->model("Location_model");
        $response = ['status'=>'ok','title'=>'Add Location','message'=>'Location added successfully'];
        $dept_id = $data['dept_id'];
        unset($data['dept_id']);
        if(!isset($data['id']))
        {
            $result = $this->Location_model->insert($data);
            $response['title'] = "Add Location";
            $response['message'] = "Location added";
            $response['id'] = $result;
            $response['locations'] = $this->Location_model->getByCondition(['area_id'=>$data['area_id'],'hospital_id'=>$data['hospital_id']]);
        }
        else if(isset($data['id']))
        {
            $result = $this->Location_model->update($data['id'],$data);
            $response['title'] = "Update Location";
            $response['message'] = "Location updated";
            $response['locations'] = $this->Location_model->getByArea($dept_id);
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please';
        }
        echo json_encode($response);
    }

    public function addArea()
    {
        $data = $_POST;
        $this->load->model("Area_model");
        $response = ['status'=>'ok','title'=>'Add Area','message'=>'Area added successfully'];
        if(!isset($data['id']))
        {
            $result = $this->Area_model->insert($data);
            $response['title'] = "Edit Area";
            $response['message'] = "Area edited";
            $response['area'] = $this->Area_model->getByDetp($data['dept_id']);
        }
        else if(isset($data['id']))
        {
            $result = $this->Area_model->update($data['id'],$data);
            $response['title'] = "Edit Area";
            $response['message'] = "Area edited";
            $response['area'] = $this->Area_model->getByDetp($data['dept_id']);
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please';
        }
        echo json_encode($response);
    }


    public function DeleteMap()
    {
        $data = $_POST;
        $this->load->model("Map_model");
        $response = ['status'=>'ok','title'=>'Delete Map','message'=>'Map deleted successfully'];
        if(isset($data['id']))
        {
            $result = $this->Map_model->getByID($data['id']);
            $src = $result[0]['src'];
            unlink('./map/'.$src);
            $this->Map_model->delete($data['id']);
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select map';
        }
        echo json_encode($response);
    }

    public function prePI()
    {
        $data = $_POST;
        $area = [];
        $dept = [];
        $inventory = [];
        $location = [];
        if(isset($data['area']))
        {
            $area = $data['area'];
        }

        if(isset($data['dept']))
        {
            $dept = $data['dept'];
        }

        if(isset($data['hospital']))
        {
            $hospital = $data['hospital'];
        }

        if(isset($data['inventory']))
        {
            $inventory = $data['inventory'];
        }

        if(isset($data['location']))
        {
            $location = $data['location'];
        }


        $response = ['status'=>'ok','title'=>'Insert Items','message'=>'Items added successfully'];
        $this->load->model(['Location_model','Inventory_model','Map_model']);
        //$this->Inventory_model->update($inventory['id'],$inventory);
        $this->Location_model->insert($location);
        $this->load->helper(array('form', 'url'));
        $config['upload_path'] = './map/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload("map"))
        {
            $data = array('error' => $this->upload->display_errors());
/*            $response['status'] = "fail";
            $response['message'] = "Please select valid file";*/
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $name = $data['upload_data']['file_name'];
            if(isset($inventory['location_id']))
            {
                $id = $location['id'];
                /*
                $location_data = $this->Location_model->getByID($inventory['location_id']);
                $image = $location_data[0]['map'];
                if(strlen($image)>0)
                unlink('./map/'.$image);
                */
                $this->Map_model->insert(["location_id"=>$inventory['location_id'],"src"=>$name]);
            }
            else
            {
                $response['status'] = "fail";
                $response['message'] = "Please select inventory";
            }
        }
        echo json_encode($response);
    }


    public function addItem()
    {
        $data = $_POST;
        $this->load->model("Items_model");
        $response = ['status'=>'ok','title'=>'Add Item','message'=>'Item added successfully'];
        if(!isset($data['id']))
        {
            $response['data'] = $data;
            //$result = $this->Items_model->insertGetID($data);
            //$response['item'] = $this->Items_model->getByID($result);
        }
        else if(isset($data['id']))
        {
            //$result = $this->Items_model->update($data['id'],$data);
            $response['title'] = "Edit Item";
            $response['message'] = "Item updated Successfully";
            //$response['item'] = $this->Items_model->getByID($result);
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please';
        }
        echo json_encode($response);
    }



    public function uploadCSVfile()
    {
        $response = ['status'=>'ok','title'=>'Insert Items','message'=>'Items added successfully'];
        $this->load->helper(array('form', 'url'));
        $config['upload_path'] = './files/';
        $config['allowed_types'] = 'gif|jpg|png|csv';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload("file"))
        {
            $data = array('error' => $this->upload->display_errors());
            $response['status'] = "fail";
            $response['message'] = "Please select valid file";
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $name = $data['upload_data']['file_name'];
            $this->load->library('csvimport');
            $result = $this->csvimport->get_array('./files/'.$name);
            if(isset($_POST['id']))
            {
                $this->load->library('csvimport');
                $this->load->model(['Items_model','Inventory_items_model']);
                if(isset($_POST['remove']))
                {
                    $this->Items_model->deleteItem($_POST['id']);
                }

                $inventory_id = $_POST['id'];
                $result = $this->csvimport->get_array('./files/'.$name);
                foreach($result as $item)
                {

                    $items = [] ;
                    $items = [
                        'countsheet_control_number'=>$item['COUNTSHEET CONTROL NUMBER'],
                        'location_number'=>$item['LOCATION NUMBER'],
                        'bin'=>$item['BIN (SHELF)'],
                        'manufacture_item_number'=>$item['MANUFACTURER ITEM NUMBER'],
                        'manufacture_item_number_nodash'=>$item['MANUFACTURER ITEM NODASH'],
                        'manufacture_name'=>$item['MANUFACTURER NAME'],
                        'manufacture_item_description'=>$item['MANUFACTURE ITEM DESCRIPTION'],
                        'on_hand_qty'=>$item['ON HAND QTY'],
                        'on_hand_unit_of_measure'=>$item['ON HAND UNIT OF MEASURE (CS,BX,EA)'],
                        'expired_on_hand_qty'=>$item['EXPIRED ON HAND QTY'],
                        'hospital_item_number'=>$item['HOSPITAL ITEM NUMBER'],
                        'effective_uom_to_luom_factor'=>$item['EFFECTIVE UOM TO LUOM FACTOR'],
                        'on_hand_uom_to_luom_factor'=>$item['ON HAND UOM TO LUOM FACTOR'],
                        'stock_yestno'=>$item['STOCK_YESTNO'],
                        'effective_unit_of_measure_price'=>$item['EFFECTIVE UNIT OF MEASURE PRICE'],
                        'consignment_flag'=>$item['CONSIGNMENT FLAG'],
                        'dioh_flag'=>$item['90 DIOH FLAG'],
                        'low_unit_of_mesure_price'=>$item['LOUM PRICE'],
                        'item_source'=>$item['ITEM SOURCE'],
                        'price_source'=>$item['PRICING SOURCE'],
                        'packing_soucre'=>$item['PACKAGING SOURCE'],//
                        'vendor_item_no'=>$item['VENDOR ITEM NO'],
                        'price_match'=>$item['PRICE MATCH'],
                        'pi_align'=>$item['PI ALIGN'],
                        'original_hospital_mgf_id_no_dash'=>$item['ORIGINALHOSPITALMFGIDNODASH'],
                        'original_hospital_vendor_item_no_dash'=>$item['ORIGINALHOSPITALVENDORITEMNODASH'],

                    ];

                    $result = $this->Items_model->getByMft($items['manufacture_item_number']);
                    if(count($result)>0)
                    {
                        $this->Items_model->updateMft($items);
                        $item_id = $result[0]['id'];
                    }
                    else
                    {
                        $item_id = $this->Items_model->insertGetID($items);
                    }

                    //$item_id = 1;

                    /*
                    $inventory_items = [];
                    $inventory_items = [
                        'packing'=>$item['PACKAGING'],
                        'purchase_unit_of_measure'=>$item['PURCHASE UNIT OF MEASURE'],
                        'effective_uom_to_luom_factor'=>$item['EFFECTIVE UOM TO LUOM FACTOR'],
                        'item_id' => $item_id,
                        'inventory_id'=>$inventory_id
                    ];

                    $this->Inventory_items_model->insert($inventory_items);*/
                }

                unlink('./files/'.$name);
            }
            else
            {
                $response['status'] = "fail";
                $response['message'] = "Please select inventory";
            }

        }
        echo json_encode($response);
    }


    public function CountAction()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $load_data = [
            'type' => 'plainQuery',
            'primaryKey' => 'id',
            'col' => [
                'manufacture_item_number',
                'manufacture_item_number_nodash',
                'manufacture_name',
                'manufacture_item_description',
                'low_unit_of_mesure',
                'low_unit_of_mesure_price',
                'item_source',
                'price_source',
                'packing_soucre',
                'vendor_name',
                'vendor_item_no'
            ],
            'table' => 'items',
            'where' =>''
        ];



        if(isset($data['item_num'])&&$data['item_num']!="")
        {
            $data['item_num'] = preg_replace('/[^A-Za-z0-9]/', '', $data['item_num']);
            $load_data['where'] .= "manufacture_item_number like '".$data['item_num']."%' ";
        }
        else if(isset($data['multi_item_num'])&&$data['multi_item_num']!="")
        {
            $data['multi_item_num'] = preg_replace('/[^A-Za-z0-9]/', '', $data['multi_item_num']);
            $load_data['where'] .= "manufacture_item_number like '".$data['multi_item_num']."%' ";
        }

        if(isset($data['description'])&&$data['description']!="")
        {
            if($load_data['where']!="")
                $load_data['where'] .= " AND ";
            $load_data['where'] .= "manufacture_item_description like '%".$data['description']."%' ";
        }

        if(isset($data['vendor'])&&$data['vendor']!="")
        {
            if($load_data['where']!="")
                $load_data['where'] .= " AND ";

            $load_data['where'] .= "vendor_item_no like '".$data['vendor']."%' ";
        }



        $this->load->library('easy_curd',['table'=>'items','load_data'=>$load_data,'columns'=>[]]);
        $this->easy_curd->index($data);
    }

    function clean($string) {
        $string = str_replace('', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }


    public function action()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Name','field' => 'name'],
            ['label'=>'Hospital',
                'field' => 'location_id',
                'relation'=>
                    [
                        'table'=>'location',
                        'key'=>'id',
                        'label'=>'name'
                    ]
            ]
        ];


        /*
        SELECT
        h.id,
        h.name,
        h.email,
        h.phone,
        (select username from users where userlvl = 2 and h.client_id = users_id) as client_id,
        (select username from users where userlvl = 3 and h.counter_id = users_id) as counter_id
        FROM `hospital` h
         *//*
        $load_data = [
            'type' => 'subQuery',
            'primaryKey' => 'h.id',
            'col' => [
                ['db'=>'h.id','name'=>'id'],
                ['db'=>'h.name','name'=>'name'],
                ['db'=>'h.email','name'=>'email'],
                ['db'=>'h.phone','name'=>'phone'],
                ['db'=>'(select username from users where userlvl = 2 and h.client_id = users_id) as client_id','name'=>'client_id'],
                ['db'=>'(select username from users where userlvl = 3 and h.counter_id = users_id) as counter_id','name'=>'counter_id']
            ],
            'table' => 'hospital as h',
            'where' =>''
        ];*/

        $load_data = [
            'type' => 'joinQuery',
            'primaryKey' => 'i.id',
            'col' => [
                'i.id',
                'i.pi_date',
                'h.name',
                'i.status'
            ],
            'table' => 'inventory i left join hospital h on i.hospital_id = h.id',
            'where' =>''
        ];

        if(isset($_POST['hospital_id'])&&$_POST['hospital_id']!=0)
        {
            $load_data['where'] = "i.hospital_id = ".$_POST['hospital_id'];
        }

        if( $this->data['user']['userlvl']==2 )
        {

        }
        else if( $this->data['user']['userlvl']==3 )
        {
            if(isset($_POST['action']))
            {
                $_POST['action']['edit'] = "false";
                $_POST['action']['delete'] = "false";
            }
        }

        if(isset($_POST['status']))
        {
            $status = $_POST['status'];
            if($status != "")
            {
                $load_data['where'] .= " i.status = '$status'";
            }
        }

        $this->load->library('easy_curd',['table'=>'inventory','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }
}