<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Counter_controller extends CI_Controller
{
    private $data;
    private $js = 0;
    private $css = 0;

    public function __construct()
    {
        parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }

        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        
        $this->data['css'][$this->css++] = base_url("assets/css/jquery.steps.css");
        $this->data['css'][$this->css++] = base_url("assets/plugins/datepicker/css/bootstrap-datepicker.css");

        $this->data['js'][$this->js++] =  base_url("assets/plugins/datepicker/js/bootstrap-datepicker.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/jquery.steps.min.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/jquery.csv-0.71.min.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/easy_curd.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/custom/inventory.js");
    }

    public function index()
    {
        $this->load->helper('form');
        $this->load->view('hospitals/index', $this->data);
    }


    public function inventoryList($id = 0)
    {
        if($id == 0)
            redirect("/hospitals");
        $this->load->helper('form');
        $this->data['hospital_id'] = $id;
        $this->load->model("Hospital_model");
        $this->load->model("Department_model");
        $this->load->model("Area_model");
        $this->load->model("Location_model");
        $this->data['departments'] = $this->Department_model->getByHospital($id);
        $this->data['hospital'] = $this->Hospital_model->get_all_hospital();
        $this->data['selected_hospital'] = "";
        $data = $this->Hospital_model->getByID($id);
        $this->data['selected_hospital'] = $data[0];
        $this->data['area'] = $this->Area_model->getAll();
        $this->data['location'] = $this->Location_model->getAll();
        $this->load->view('admin/inventory/index', $this->data);
    }


    public function setup($operation = "new")
    {
        $data = $_POST;

        $inventory = $data['inventory'];
        $hospital = $data['hospital'];
        $response = ['status'=>'ok','title'=>'Setup','message'=>'Record added successfully'];
        $hospital_response = $this->update('hospital',$hospital);
        if(isset($inventory['id']))
        {
            $inventory_response = $this->update('inventory',$inventory);
            $response['id'] = $inventory['id'];
            if($inventory_response['result'])
            {
                $response['message'] = "Inventory updated successfully";
            }
            else
            {
                $response['status'] = 'warn';
                $response['message'] = "Inventory not updated";
            }

        }
        else
        {
            $inventory_response = $this->insert('inventory',$inventory);
            $response['id'] = $inventory_response['data']['id'];
        }

        echo json_encode($response);
    }

    /***************************Delete Inventory*****************************************/
    function deleteInventory()
    {
        $data = $_POST;
        $id = $data['id'];
        $this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Delete Inventory','message'=>'Inventory remove successfully'];

        //<---------Note--------->
        //Delete maps related to location
        //Maps delete functionality need to be implemented

        //Delete location related to inventory
        $this->Curd->deleteByCondition('location',"inventory_id = $id");
        //Delete All Items
        $this->Curd->deleteByCondition('items',"inventory_id = $id");
        //Delete all location
        $this->Curd->deleteByCondition('inventory',"id = $id");

        echo json_encode($response);
    }
    /****************************Upload CSV File***********************************/

    public function uploadCSVfile()
    {
        $response = ['status'=>'ok','title'=>'Insert Items','message'=>'Items added successfully'];
        $this->load->helper(array('form', 'url'));
        $config['upload_path'] = './files/';
        $config['allowed_types'] = 'gif|jpg|png|csv';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload("file"))
        {
            $data = array('error' => $this->upload->display_errors());
            $response['status'] = "fail";
            $response['message'] = "Please select valid file";
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $name = $data['upload_data']['file_name'];
            $this->load->library('csvimport');
            $result = $this->csvimport->get_array('./files/'.$name);
            if(isset($_POST['inventory_id']))
            {
                $this->load->library('csvimport');
                $this->load->model(['Items_model','Inventory_items_model']);
                if(isset($_POST['remove']))
                {
                    $this->Items_model->deleteItem($_POST['inventory_id']);
                }

                $inventory_id = $_POST['inventory_id'];
                $result = $this->csvimport->get_array('./files/'.$name);

                foreach($result as $item)
                {

                    $items = [];
                    $items = [
                        'countsheet_control_number'=>$item['COUNTSHEET CONTROL NUMBER'],
                        'location_number'=>$item['LOCATION NUMBER'],
                        'bin'=>$item['BIN (SHELF)'],
                        'manufacture_item_number'=>$item['MANUFACTURER ITEM NUMBER'],
                        'manufacture_item_number_nodash'=>$item['MANUFACTURER ITEM NODASH'],
                        'manufacture_name'=>$item['MANUFACTURER NAME'],
                        'manufacture_item_description'=>$item['MANUFACTURE ITEM DESCRIPTION'],
                        'on_hand_qty'=>$item['ON HAND QTY'],
                        'on_hand_unit_of_measure'=>$item['ON HAND UNIT OF MEASURE (CS,BX,EA)'],
                        'expired_on_hand_qty'=>$item['EXPIRED ON HAND QTY'],
                        'hospital_item_number'=>$item['HOSPITAL ITEM NUMBER'],
                        'effective_uom_to_luom_factor'=>$item['EFFECTIVE UOM TO LUOM FACTOR'],
                        'on_hand_uom_to_luom_factor'=>$item['ON HAND UOM TO LUOM FACTOR'],
                        'stock_yestno'=>$item['STOCK_YESTNO'],
                        'effective_unit_of_measure_price'=>$item['EFFECTIVE UNIT OF MEASURE PRICE'],
                        'consignment_flag'=>$item['CONSIGNMENT FLAG'],
                        'dioh_flag'=>$item['90 DIOH FLAG'],
                        'low_unit_of_mesure_price'=>$item['LOUM PRICE'],
                        'item_source'=>$item['ITEM SOURCE'],
                        'price_source'=>$item['PRICING SOURCE'],
                        'packing_soucre'=>$item['PACKAGING SOURCE'],//
                        'vendor_item_no'=>$item['VENDOR ITEM NO'],
                        'price_match'=>$item['PRICE MATCH'],
                        'pi_align'=>$item['PI ALIGN'],
                        'original_hospital_mgf_id_no_dash'=>$item['ORIGINALHOSPITALMFGIDNODASH'],
                        'original_hospital_vendor_item_no_dash'=>$item['ORIGINALHOSPITALVENDORITEMNODASH'],
                        'packing'=>$item['PACKAGING'],
                        'purchase_unit_of_measure'=>$item['PURCHASE UNIT OF MEASURE'],
                        'effective_uom_to_luom_factor'=>$item['EFFECTIVE UOM TO LUOM FACTOR'],
                        'inventory_id'=>$inventory_id
                    ];

                    $result = $this->Items_model->getByMft($items['manufacture_item_number']);
                    if(count($result)>0)
                    {
                        $this->Items_model->updateMft($items);
                        $item_id = $result[0]['id'];
                    }
                    else
                    {
                        $item_id = $this->Items_model->insertGetID($items);
                    }

                }

                unlink('./files/'.$name);
            }
            else
            {
                $response['status'] = "fail";
                $response['message'] = "Please select inventory";
            }

        }
        echo json_encode($response);
    }


    /****************************Department********************************/

    public function getInventory()
    {
        $data = $_POST;
        $this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Get Inventory','message'=>'Retrieve successfully'];
        $data = $this->Curd->getByID('inventory',$data['id']);
        if(count($data)>0)
        {
            $response['data'] = $data[0];
        }
        else
        {
            $response['status'] = "fail";
            $response['message'] = "Inventory not found";
        }

        echo json_encode($response);
    }


    /****************************Department********************************/

    public function getDepartments()
    {
        $data = $_POST;
        $response = ['status'=>'ok','title'=>'Get Department','message'=>'Retrieve successfully'];
        $response = $this->getDepartmentByHospital('departments',$data['hospital_id']);
        echo json_encode($response);
    }

    public function addDepartments()
    {
        $data = $_POST;
        $department = $data['department'];

        //Insert new department in Database
        $response = $this->insert('departments',$department);

        //Get Department by Hospital ID
        $hospital_department = $this->getDepartmentByHospital('departments',$department['hospital_id']);

        $response['location_number'] = $this->getLocationNumber($response['data']['id']);

        $response['data']['departments'] = $hospital_department['data'];

        echo json_encode($response);
    }

    private function getDepartmentByHospital($table,$hospital_id)
    {
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];

        if($hospital_id>0)
        {
            $result = $this->Curd->getByWhere($table,['hospital_id'=>$hospital_id]);
            $response['data'] = $result;
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    /************************************Area**********************************/

    public function getAreas()
    {
        $data = $_POST;
        $response = ['status'=>'ok','title'=>'Get Area','message'=>'Retrieve successfully'];
        $response = $this->getAreaByDepartment('general_area',$data['department_id']);
        $response['location_number'] = $this->getLocationNumber($data['department_id']);
        echo json_encode($response);
    }

    //Generate Location Number
    private function getLocationNumber($dept_id)
    {
        $this->load->model('Curd');
        $department = $this->Curd->getByID('departments',$dept_id);
        $hospital = $this->Curd->getByID('hospital',$department[0]['hospital_id']);
        $max_location_id = $this->Curd->max('location');
        $hospital_prefix = strtoupper(substr($hospital[0]['name'],0,3));
        $department_prefix = strtoupper(substr($department[0]['name'],0,3));
        if($max_location_id[0]['id'])
        {
            $num = $max_location_id[0]['id']+1;
        }
        else
        {
            $num = 1;
        }

        $location_number = $hospital_prefix."-".$department_prefix."-".$num;

        return $location_number;
    }

    public function addArea()
    {
        $data = $_POST;
        $area = $data['area'];

        //Insert new department in Database
        $response = $this->insert('general_area',$area);

        //Get Department by Hospital ID
        $department_areas = $this->getAreaByDepartment('general_area',$area['dept_id']);

        $response['data']['areas'] = $department_areas['data'];

        echo json_encode($response);
    }

    private function getAreaByDepartment($table,$department_id)
    {
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];

        if($department_id>0)
        {
            $result = $this->Curd->getByWhere($table,
                [
                    'dept_id'=>$department_id
                ]);

            $response['data'] = $result;
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    /************************************Pre Physical Inventory*************************************/

    public function pre_pl()
    {
        $data = $_POST;

        $location = $data['location'];

        //Insert new Location in Database
        $response = $this->insert('location',$location);

        $response['location_number'] = $this->getLocationNumber($data['department']['id']);

        echo json_encode($response);
    }

    //Print Pre PI Module
    public function printPre_pi()
    {
        $dept_id = "all";
        $area_id = "all";
        $hospital_id = "all";
        if(isset($_GET['dept_id']))
        {
            $dept_id = $_GET['dept_id'];
        }

        if(isset($_GET['area_id']))
        {
            $area_id = $_GET['area_id'];
        }

        $where = "";

        if($hospital_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "hpt.id = ".$hospital_id;
        }

        if($dept_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "dept.id = ".$dept_id;
        }

        if($area_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "area.id = ".$area_id;
        }

        $fileName = 'exportData.csv';
        $header = [
            'Physical Inventory Date',
            'Audit Percent',
            'Status',
            'Hospital',
            'Address',
            'City',
            'State',
            'Zip',
            'Contact Name',
            'Email',
            'Phone',
            'Department',
            'General Area',
            'Location',
            'Location Number',
            'POU/Main',
            'Consigned',
            'Estimated Bins',
            'Estimated Bins Per Hours',
            'Estimated Count Time (Hours)',
            'Estimated Count Time (Minutes)'
        ];

        $data[] = $header;

        $this->load->model("Curd");

        $sql_query = "select
                    inv.pi_date,
                    inv.audit_percent,
                    inv.status,
                    hpt.name as 'hospital',
                    hpt.address,
                    hpt.city,
                    hpt.state,
                    hpt.zip,
                    hpt.contact_name,
                    hpt.email,
                    hpt.phone,
                    dept.name as 'department',
                    area.name as 'general_area',
                    loc.name as 'location',
                    loc.location_number,
                    loc.consume,
                    loc.consigned,
                    loc.estimated_bins,
                    loc.estimated_bins_per_hours,
                    Round((loc.estimated_bins/loc.estimated_bins_per_hours),2) as 'estimate_count_time_hours',
                    Round(((loc.estimated_bins/loc.estimated_bins_per_hours)*60),2) as 'estimate_count_time_min'
                    from
                    inventory as inv left join location as loc on inv.id = loc.inventory_id
                    left join general_area as area on area.id = loc.area_id
                    left join departments as dept on dept.id = area.dept_id
                    left join hospital as hpt on dept.hospital_id = hpt.id $where";

        $result = $this->Curd->query($sql_query);

        $this->data["result"] = $result;
        $this->load->view('admin/inventory/print', $this->data);

    }

    //Export Pre PI Module
    public function exportPre_pi()
    {
        $dept_id = "all";
        $area_id = "all";
        $hospital_id = "all";
        if(isset($_GET['dept_id']))
        {
            $dept_id = $_GET['dept_id'];
        }

        if(isset($_GET['area_id']))
        {
            $area_id = $_GET['area_id'];
        }

        $where = "";

        if($hospital_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "hpt.id = ".$hospital_id;
        }

        if($dept_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "dept.id = ".$dept_id;
        }

        if($area_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "area.id = ".$area_id;
        }

        $fileName = 'exportData.csv';
        $header = [
            'Physical Inventory Date',
            'Audit Percent',
            'Status',
            'Hospital',
            'Address',
            'City',
            'State',
            'Zip',
            'Contact Name',
            'Email',
            'Phone',
            'Department',
            'General Area',
            'Location',
            'Location Number',
            'POU/Main',
            'Consigned',
            'Estimated Bins',
            'Estimated Bins Per Hours',
            'Estimated Count Time (Hours)',
            'Estimated Count Time (Minutes)'
        ];

        $data[] = $header;

        $this->load->model("Curd");

        $sql_query = "select
                    inv.pi_date,
                    inv.audit_percent,
                    inv.status,
                    hpt.name as 'hospital',
                    hpt.address,
                    hpt.city,
                    hpt.state,
                    hpt.zip,
                    hpt.contact_name,
                    hpt.email,
                    hpt.phone,
                    dept.name as 'department',
                    area.name as 'general_area',
                    loc.name as 'location',
                    loc.location_number,
                    loc.consume,
                    loc.consigned,
                    loc.estimated_bins,
                    loc.estimated_bins_per_hours,
                    Round((loc.estimated_bins/loc.estimated_bins_per_hours),2) as 'estimate_count_time_hours',
                    Round(((loc.estimated_bins/loc.estimated_bins_per_hours)*60),2) as 'estimate_count_time_min'
                    from
                    inventory as inv left join location as loc on inv.id = loc.inventory_id
                    left join general_area as area on area.id = loc.area_id
                    left join departments as dept on dept.id = area.dept_id
                    left join hospital as hpt on dept.hospital_id = hpt.id $where";

        $result = $this->Curd->query($sql_query);

        foreach($result as $row)
        {
            $data[] = $row;
        }

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fp = @fopen( 'php://output', 'w' );

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }


    //Get Location
    public function getLocation()
    {
        $data = $_POST;
        $this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];
        $response['data'] = $this->Curd->getByPlainWhere('location','inventory_id = '.$data['inventory_id'].' AND '.$data['status']);
            //$this->getLocationByInventory('location',$data['inventory_id']);
        echo json_encode($response);
    }

    //Get Location with summary
    public function getLocationSummary()
    {
        $data = $_POST;
        $this->load->model('Curd');

        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];

        if($data['status']!="status = 'all'")
        {
            $response['data']['location'] = $this->Curd->getByPlainWhere('location','inventory_id = '.$data['inventory_id'].' AND '.$data['status']);
        }else
        {
            $response['data']['location'] = $this->Curd->getByPlainWhere('location','inventory_id = '.$data['inventory_id']);
        }

        $inventory_id = $data['inventory_id'];

        //Count All
        $total = $this->Curd->query("SELECT count(id) as total FROM `location` WHERE inventory_id = $inventory_id");
        $response['data']['total'] = $total[0]['total'];

        //Count Completed
        $complete = $this->Curd->query("SELECT count(id) as completed FROM `location` WHERE status = 'Completed' AND inventory_id = $inventory_id");
        $response['data']['completed'] = $complete[0]['completed'];

        //Count Remaining
        $remaining = $this->Curd->query("SELECT count(id) as remain FROM `location` WHERE status <> 'Completed' AND inventory_id = $inventory_id");
        $response['data']['remain'] = $remaining[0]['remain'];

        //Count Total Bins
        $total_bins = $this->Curd->query("SELECT sum(estimated_bins) as total_bins FROM `location` WHERE inventory_id = $inventory_id");
        $response['data']['total_bins'] = $total_bins[0]['total_bins'];

        //Count Complete Bins
        $complete_bins = $this->Curd->query("SELECT sum(estimated_bins) as completed_bins FROM `location` WHERE status = 'Completed' AND inventory_id = $inventory_id");
        $response['data']['complete_bins'] = $complete_bins[0]['completed_bins'];

        //Count Remain Bins
        $complete_bins = $this->Curd->query("SELECT sum(estimated_bins) as remain_bins FROM `location` WHERE status <> 'Completed' AND inventory_id = $inventory_id");
        $response['data']['remain_bins'] = $complete_bins[0]['remain_bins'];

        //Complete Percent
        if($response['data']['total_bins']!=0)
            $response['data']['completed_percent'] = round(($response['data']['complete_bins']*100)/$response['data']['total_bins'],2);
        else
            $response['data']['completed_percent'] = 0;

        //Estimate time completion
        $complete_bins = $this->Curd->query("SELECT sum(estimated_bins_per_hours) as bins_time FROM `location` WHERE inventory_id = $inventory_id");
        $response['data']['bins_time'] = $complete_bins[0]['bins_time']*60;

        echo json_encode($response);
    }

    //Update Location
    public function updateLocation()
    {
        $data = $_POST;

        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Update Location','message'=>'Team added to location successfully'];

        $result = $this->Curd->updateByWhere("location",$data,['location_number'=>$data['location_number']]);

        $response['data'] = $result;

        echo json_encode($response);
    }

    //Update Location
    public function updateLocationStatus()
    {
        $data = $_POST;

        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Update Location','message'=>'Location status completed'];

        $result = $this->Curd->updateByWhere("location",$data,['location_number'=>$data['location_number']]);

        $response['data'] = $result;

        echo json_encode($response);
    }

    //Update Items
    public function updateItem()
    {
        $data = $_POST;

        //Load Curd Model
        $this->load->model("Curd");

        if(isset($data['status']))
        {
            $status = $data['status'];
            $result = $this->Curd->updateByWhere("location",['status'=>$status],['location_number'=>$data['location_number']]);
            unset($data['status']);
        }

        //Set Response
        $response = ['status'=>'ok','title'=>'Update Item','message'=>'Team added to location successfully'];

        $result = $this->Curd->updateByWhere("items",$data,['id'=>$data['id']]);

        $response['data'] = $result;

        echo json_encode($response);
    }

    private function getLocationByInventory($table,$inventory_id)
    {
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];

        if($inventory_id>0)
        {
            $result = $this->Curd->getByWhere($table,
                [
                    'inventory_id'=>$inventory_id
                ]);

            $response['data'] = $result;
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    //Get Location details
    function getLocationDetail()
    {
        $this->load->model("Curd");
        $data = $_POST;
        //$data["location_number"] = "CEN-DEP-1";
        $location_number = $data["location_number"];
        $response = ['status'=>'ok','title'=>'Retrieve Detail','message'=>'Detail retrieve successfully'];
        $query = "select
                    inv.audit_percent,
                    loc.location_number,
                    loc.name as location,
                    area.name as area,
                    dept.name as dept,
                    loc.consume,
                    loc.consigned,
                    loc.estimated_bins,
                    loc.estimated_bins_per_hours,
                    loc.team
                    from
                    location as loc
                    left join inventory as inv on loc.inventory_id = inv.id
                    left join general_area as area on loc.area_id = area.id
                    left join departments as dept on area.dept_id = dept.id where loc.location_number = '$location_number'";

        $location = $this->Curd->query($query);
        if(count($location)>0)
        {
            $response['data'] = $location[0];
        }else
        {
            $response['status'] = 'fail';
            $response['message'] = 'No location found';
        }

        echo json_encode($response);
    }

    private function get($table)
    {
        $this->load->model("Curd");
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];
        if(!isset($data['id']))
        {
            $result = $this->Curd->getAll($table);
            if(count($result)==0)
            {
                $response['status'] = 'fail';
                $response['message'] = 'Record not found';
            }
            else
            {
                $response['data'] = $result;
            }
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    private function insert($table,$data)
    {
        $response = ['status'=>'ok','title'=>'Add','message'=>'Record added successfully'];
        $this->load->model(["Curd"]);
        $id = $this->Curd->insert($table,$data);
        $response['data'] = ['id'=>$id];
        return $response;
    }

    private function update($table,$data)
    {
        $this->load->model("Curd");
        $response = ['status'=>'ok','title'=>'Update','message'=>'Record update successfully'];
        if(isset($data['id']))
        {
            $response['result'] = $this->Curd->update($table,$data);
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
            $response['result'] = false;
        }
        return $response;
    }

    private function delete($table,$id)
    {
        $response = ['status'=>'ok','title'=>'Remove','message'=>'Record deleted successfully'];
        $this->load->model(["Curd"]);
        $id = $this->Curd->delete($table,$id);
        return $response;
    }

    public function countSheet()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Manufacture Item No','field' => 'manufacture_item_number','attributes'=>['required'=>'required']],
            ['label'=>'Manufacture Name','field' => 'manufacture_name','attributes'=>['required'=>'required']],
            ['label'=>'Manufacture Item Description','field' => 'manufacture_item_description','attributes'=>['required'=>'required']],
            ['label'=>'Unit of Measurement','field' => 'low_unit_of_mesure','attributes'=>['required'=>'required']],
            ['label'=>'Packaging(e.g 20ES/CS)','field' => 'packing','attributes'=>['required'=>'required']],
            ['label'=>'ON Need Quantity','field' => 'on_hand_quantity','attributes'=>['required'=>'required']],
            ['label'=>'Expired Quantity','field' => 'expired_quantity','attributes'=>['required'=>'required']],
            ['label'=>'Location Number','field' => 'location_number','attributes'=>['required'=>'required']]
        ];

        $load_data = [
            'type' => 'plainQuery',
            'primaryKey' => 'id',
            'col' => [
                'manufacture_item_number',
                'manufacture_name',
                'manufacture_item_description',
                'low_unit_of_mesure',
                'packing',
                'on_hand_quantity',
                'expired_quantity'
            ],
            'table' => 'items',
            'where' =>'id = 0'
        ];

        if(isset($data['condition']))
        {
            $load_data['where'] = "";
            if(isset($data['condition']['mfg_item_number']))
            {
                $id = preg_replace('/[^A-Za-z0-9]/', '', $data['condition']['mfg_item_number']);
                $load_data['where'] = "manufacture_item_number_nodash ='$id'";
            }else
                if(isset($data['condition']['multi_item_num'])&&$data['condition']['multi_item_num']!="")
            {
            $data['condition']['multi_item_num'] = preg_replace('/[^A-Za-z0-9]/', '', $data['condition']['multi_item_num']);
            $load_data['where'] .= "manufacture_item_number like '".$data['condition']['multi_item_num']."%' ";
            }

            if(isset($data['condition']['description'])&&$data['condition']['description']!="")
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";
                $load_data['where'] .= "manufacture_item_description like '%".$data['condition']['description']."%' ";
            }

            if(isset($data['condition']['vendor'])&&$data['condition']['vendor']!="")
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";

                $load_data['where'] .= "vendor_item_no like '".$data['condition']['vendor']."%' ";
            }
             if(isset($data['condition']['all']))
             {

             }
             else if(isset($data['condition']['location_number']))
             {
                 $num = $data['condition']['location_number'];
                 if($load_data['where']!="")
                     $load_data['where'] .= " AND ";
                 $load_data['where'] .= " location_number ='$num'";
             }
             else{
                 if($load_data['where']!="")
                     $load_data['where'] .= " AND ";
                 $load_data['where'] .= " (location_number is NULL or location_number = '')";
             }



        }


        $this->load->library('easy_curd',['table'=>'items','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }

    public function action()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Name','field' => 'name'],
            ['label'=>'Hospital',
                'field' => 'location_id',
                'relation'=>
                    [
                        'table'=>'location',
                        'key'=>'id',
                        'label'=>'name'
                    ]
            ]
        ];


        /*
        SELECT
        h.id,
        h.name,
        h.email,
        h.phone,
        (select username from users where userlvl = 2 and h.client_id = users_id) as client_id,
        (select username from users where userlvl = 3 and h.counter_id = users_id) as counter_id
        FROM `hospital` h
         *//*
        $load_data = [
            'type' => 'subQuery',
            'primaryKey' => 'h.id',
            'col' => [
                ['db'=>'h.id','name'=>'id'],
                ['db'=>'h.name','name'=>'name'],
                ['db'=>'h.email','name'=>'email'],
                ['db'=>'h.phone','name'=>'phone'],
                ['db'=>'(select username from users where userlvl = 2 and h.client_id = users_id) as client_id','name'=>'client_id'],
                ['db'=>'(select username from users where userlvl = 3 and h.counter_id = users_id) as counter_id','name'=>'counter_id']
            ],
            'table' => 'hospital as h',
            'where' =>''
        ];*/

        $load_data = [
            'type' => 'joinQuery',
            'primaryKey' => 'i.id',
            'col' => [
                'i.id',
                'i.pi_date',
                'h.name',
                'i.status'
            ],
            'table' => 'inventory i left join hospital h on i.hospital_id = h.id',
            'where' =>''
        ];

        if(isset($_POST['hospital_id'])&&$_POST['hospital_id']!=0)
        {
            $load_data['where'] = "i.hospital_id = ".$_POST['hospital_id'];
        }

        if( $this->data['user']['userlvl']==2 )
        {

        }
        else if( $this->data['user']['userlvl']==3 )
        {
            if(isset($_POST['action']))
            {
                $_POST['action']['edit'] = "false";
                $_POST['action']['delete'] = "false";
            }
        }

        if(isset($_POST['status']))
        {
            $status = $_POST['status'];
            if($status != "")
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";
                $load_data['where'] .= " i.status = '$status'";
            }
        }

        $this->load->library('easy_curd',['table'=>'inventory','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }

    public function location()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Team','field' => 'team'],
            ['label'=>'Name','field' => 'name'],
            ['label'=>'General Area',
                'field' => 'area_id',
                'relation'=>
                    [
                        'table'=>'general_area',
                        'key'=>'id',
                        'label'=>'name'
                    ]
            ],
            ['label'=>'Consigned','field' => 'consigned'],
            ['label'=>'POU/Main','field' => 'consume'],
            ['label'=>'Estimated Bins','field' => 'estimated_bins'],
            ['label'=>'Estimated Bins Per Hours','field' => 'estimated_bins_per_hours'],
            ['label'=>'Status','field' => 'status']
        ];

/*select
loc.name,
loc.location_number,
loc.consigned,
loc.consume,
loc.estimated_bins,
loc.estimated_bins_per_hours,
loc.team,
loc.status,
dept.name as department,
area.name as area
from location as loc left join general_area as area on loc.area_id = area.id left join departments as dept on dept.id = area.dept_id*/

        $load_data = [
            'type' => 'joinQuery',
            'primaryKey' => 'loc.id',
            'col' => [
                'loc.name',
                'loc.location_number',
                'loc.consigned',
                'loc.consume',
                'loc.estimated_bins',
                'loc.estimated_bins_per_hours',
                'loc.team',
                'loc.status',
                'dept.name',
                'area.name'
            ],
            'table' => 'location as loc left join general_area as area on loc.area_id = area.id left join departments as dept on dept.id = area.dept_id',
            'where' =>''
        ];

        if(isset($_POST['hospital_id'])&&$_POST['hospital_id']!=0)
        {
            $load_data['where'] = "i.hospital_id = ".$_POST['hospital_id'];
        }

        if( $this->data['user']['userlvl']==2 )
        {

        }
        else if( $this->data['user']['userlvl']==3 )
        {
            if(isset($_POST['action']))
            {
                $_POST['action']['edit'] = "false";
                $_POST['action']['delete'] = "false";
            }
        }

        if(isset($_POST['condition']['status'])&&$_POST['condition']['status']!="all")
        {
            $status = $_POST['condition']['status'];
            if($status != "")
            {
                $load_data['where'] .= " loc.status = '$status'";
            }
        }

        if(isset($_POST['condition']['inventory_id']))
        {
            $inventory_id = $_POST['condition']['inventory_id'];
            if($inventory_id != "")
            {
                if($load_data['where']!="")
                {
                    $load_data['where'] .= ' AND ';
                }
                $load_data['where'] .= " loc.inventory_id = '$inventory_id'";
            }
        }

        $this->load->library('easy_curd',['table'=>'location','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }
}