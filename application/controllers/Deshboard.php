<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deshboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $data;
    private $js = 0;
    private $css = 0;

    public function __construct()
    {
        parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }

        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        $user = $this->data['user'];
        $this->data['fileuploadstatus']="";
       
        if($user['userlvl']==3)
        {
            redirect("/counter");
        }
         $this->load->model('Gernal_model');    
         if($user['userlvl']==2)
        {
            $this->data['hospitals_list']=$this->Gernal_model->get_client_hospital($this->Um_users_model->get_id_by_username($username));
        }
        else
        {
           $this->data['hospitals_list']=$this->Gernal_model->get_all_data('hospital');  
        }
            
       /* else if($user['userlvl']==2)
        {
            redirect("/client");
        }
*/
        
        $this->data['css'][$this->css++] = base_url("/assets/css/deshboard/sb-admin-2.css");
        
        $this->data['js'][$this->js++] =  base_url("assets/js/easy_curd.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/custom/deshboard.js");
        $this->data['js'][$this->js++] =  "//code.highcharts.com/highcharts.js";
        $this->data['js'][$this->js++] =  "//code.highcharts.com/modules/data.js";
        $this->data['js'][$this->js++] =  "//code.highcharts.com/modules/drilldown.js";
    }


    public function index()
    {
       $this->load->view('admin/deshboard/index', $this->data);
    }
 public function onhand()
    {
       $this->load->view('admin/deshboard/onhand', $this->data);
    }
     public function expired()
    {
       $this->load->view('admin/deshboard/expired', $this->data);
    }
    
    public function supplier()
    {
       $this->load->view('admin/deshboard/supplier', $this->data);
    }
     public function data()
    {
       $data_req=$this->input->post('data_req');
        $item_id=$this->input->post('item_id');
         if($data_req!=NULL)
         {
             if(strcmp($data_req,'department')==0)
             {
             $this->load->model('Forign_key');    
             $hospital_department=$this->Forign_key->get_where_data('departments','hospital_id',$item_id,'id,name');
             foreach($hospital_department as $value)
             {
                ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                <?php
             }
            }
             
             if(strcmp($data_req,'department_data_full')==0)
             {
                 $deshboard_detail=array();
                 if($item_id!=null)
                 {
                 $this->load->model('Deshboard_model');
                 $deshboard_detail['total_value']=round($this->Deshboard_model->total_value($item_id),2);
                 $deshboard_detail['total_main']=round($this->Deshboard_model->total_main($item_id),2);
                 $deshboard_detail['total_pou']=round($this->Deshboard_model->total_pou($item_id),2);
                 $deshboard_detail['total_owned']=round($this->Deshboard_model->total_owned($item_id),2);
                 $deshboard_detail['total_consigned']=round($this->Deshboard_model->total_consigned($item_id),2);
                 $deshboard_detail['total_match']=round($this->Deshboard_model->total_match($item_id),2);
                 $deshboard_detail['total_unmatch']=round($this->Deshboard_model->total_unmatch($item_id),2);
                 $deshboard_detail['on_hand']=round($this->Deshboard_model->on_hand($item_id),2);
                 $deshboard_detail['on_hand_main']=round($this->Deshboard_model->on_hand_main($item_id),2);
                 $deshboard_detail['on_hand_pou']=round($this->Deshboard_model->on_hand_pou($item_id),2);
                 $deshboard_detail['on_hand_owned']=round($this->Deshboard_model->on_hand_owned($item_id),2);
                 $deshboard_detail['on_hand_consigned']=round($this->Deshboard_model->on_hand_consigned($item_id),2);
                 $deshboard_detail['on_hand_match']=round($this->Deshboard_model->on_hand_match($item_id),2);
                 $deshboard_detail['on_hand_unmatch']=round($this->Deshboard_model->on_hand_unmatch($item_id),2);
                 $deshboard_detail['expired']=round($this->Deshboard_model->expired($item_id),2);
                 $deshboard_detail['expired_main']=round($this->Deshboard_model->expired_main($item_id),2);
                 $deshboard_detail['expired_pou']=round($this->Deshboard_model->expired_pou($item_id),2);
                 $deshboard_detail['expired_owned']=round($this->Deshboard_model->expired_owned($item_id),2);
                 $deshboard_detail['expired_consigned']=round($this->Deshboard_model->expired_consigned($item_id),2);
                 $deshboard_detail['expired_match']=round($this->Deshboard_model->expired_match($item_id),2);
                 $deshboard_detail['expired_unmatch']=round($this->Deshboard_model->all_supplier($item_id),2);
                 
                 $deshboard_detail['supplier_detail']=$this->Deshboard_model->all_supplier($item_id);
                     }
                 else
                 {
                 $deshboard_detail['total_value']=0;
                 $deshboard_detail['total_main']=0;
                 $deshboard_detail['total_pou']=0;
                 $deshboard_detail['total_owned']=0;
                 $deshboard_detail['total_consigned']=0;
                 $deshboard_detail['total_match']=0;
                 $deshboard_detail['total_unmatch']=0;
                 $deshboard_detail['on_hand']=0;
                 $deshboard_detail['on_hand_main']=0;
                 $deshboard_detail['on_hand_pou']=0;
                 $deshboard_detail['on_hand_owned']=0;
                 $deshboard_detail['on_hand_consigned']=0;
                 $deshboard_detail['on_hand_match']=0;
                 $deshboard_detail['on_hand_unmatch']=0;
                 $deshboard_detail['expired']=0;
                 $deshboard_detail['expired_main']=0;
                 $deshboard_detail['expired_pou']=0;
                 $deshboard_detail['expired_owned']=0;
                 $deshboard_detail['expired_consigned']=0;
                 $deshboard_detail['expired_match']=0;
                 $deshboard_detail['expired_unmatch']=0;
                 }
                 echo json_encode($deshboard_detail);
                 die;
             }
        }
        
     }
    
        public function supplier_data()
    {
           $data_req=$this->input->post('data_req');
           $item_id=$this->input->post('item_id');
           $supplier_detail=array();
             if($item_id!=null)
                 {
           $this->load->model('Deshboard_model');      
           $supplier_detail=$this->Deshboard_model->all_supplier($item_id);
             }
           echo json_encode($supplier_detail);
           die;
    }
     public function supplier_data_top()
    {
           $data_req=$this->input->post('number');
           $item_id=$this->input->post('item_id');
           $supplier_detail=array();
          if($item_id!=null)
                 {
           $this->load->model('Deshboard_model');      
           $supplier_detail=$this->Deshboard_model->top_supplier($item_id,$data_req);
          }
           echo json_encode($supplier_detail);
           die;
    }


}
