<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CounterInventory extends CI_Controller
{

    private $data;
    private $js = 0;
    private $css = 0;

    public function __construct()
    {
        parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }

        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];

        $this->data['css'][$this->css++] = base_url("assets/css/jquery.steps.css");
        $this->data['css'][$this->css++] = base_url("assets/plugins/datepicker/css/bootstrap-datepicker.css");

        $this->data['js'][$this->js++] =  base_url("assets/plugins/datepicker/js/bootstrap-datepicker.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/jquery.steps.min.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/jquery.csv-0.71.min.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/easy_curd.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/custom/counter_inventory.js");
    }


    public function inventoryList($id = 0)
    {
        if($id == 0)
            redirect("/Counter_hospitals");
        $this->load->helper('form');
        $this->data['hospital_id'] = $id;
        $this->load->model("Hospital_model");
        $this->load->model("Department_model");
        $this->load->model("Area_model");
        $this->load->model("Location_model");
        $this->data['departments'] = $this->Department_model->getByHospital($id);
        $this->data['hospital'] = $this->Hospital_model->get_all_hospital();
        $this->data['selected_hospital'] = "";
        $data = $this->Hospital_model->getByID($id);
        $this->data['selected_hospital'] = $data[0];
        $this->data['area'] = $this->Area_model->getAll();
        $this->data['location'] = $this->Location_model->getAll();
        $this->load->view('admin/counter_inventory/index', $this->data);
    }


    public function setup($operation = "new")
    {
        $data = $_POST;

        $inventory = $data['inventory'];
        $hospital = $data['hospital'];
        $response = ['status'=>'ok','title'=>'Setup','message'=>'Record added successfully'];
        $hospital_response = $this->update('hospital',$hospital);
        if(isset($inventory['id']))
        {
            $inventory_response = $this->update('inventory',$inventory);
            $response['id'] = $inventory['id'];
            if($inventory_response['result'])
            {
                $response['message'] = "Inventory updated successfully";
            }
            else
            {
                $response['status'] = 'warn';
                $response['message'] = "Inventory not updated";
            }

        }
        else
        {
            $inventory_response = $this->insert('inventory',$inventory);
            $response['id'] = $inventory_response['data']['id'];
        }

        echo json_encode($response);
    }


    /****************************Department********************************/

    public function getInventory()
    {
        $data = $_POST;
        $this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Get Inventory','message'=>'Retrieve successfully'];
        $data = $this->Curd->getByID('inventory',$data['id']);
        if(count($data)>0)
        {
            $response['data'] = $data[0];
        }
        else
        {
            $response['status'] = "fail";
            $response['message'] = "Inventory not found";
        }

        echo json_encode($response);
    }


    /****************************Department********************************/

    public function getDepartments()
    {
        $data = $_POST;
        $response = ['status'=>'ok','title'=>'Get Department','message'=>'Retrieve successfully'];
        $response = $this->getDepartmentByHospital('departments',$data['hospital_id']);
        echo json_encode($response);
    }


    private function getDepartmentByHospital($table,$hospital_id)
    {
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];

        if($hospital_id>0)
        {
            $result = $this->Curd->getByWhere($table,['hospital_id'=>$hospital_id]);
            $response['data'] = $result;
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    /************************************Area**********************************/

    public function getAreas()
    {
        $data = $_POST;
        $response = ['status'=>'ok','title'=>'Get Area','message'=>'Retrieve successfully'];
        $response = $this->getAreaByDepartment('general_area',$data['department_id']);
        $response['location_number'] = $this->getLocationNumber($data['department_id']);
        echo json_encode($response);
    }

    //Generate Location Number
    private function getLocationNumber($dept_id)
    {
        $this->load->model('Curd');
        $department = $this->Curd->getByID('departments',$dept_id);
        $hospital = $this->Curd->getByID('hospital',$department[0]['hospital_id']);
        $max_location_id = $this->Curd->max('location');
        $hospital_prefix = strtoupper(substr($hospital[0]['name'],0,3));
        $department_prefix = strtoupper(substr($department[0]['name'],0,3));
        if($max_location_id[0]['id'])
        {
            $num = $max_location_id[0]['id']+1;
        }
        else
        {
            $num = 1;
        }

        $location_number = $hospital_prefix."-".$department_prefix."-".$num;

        return $location_number;
    }

    /************************************Pre Physical Inventory*************************************/

    //Get Location
    public function getLocation()
    {
        $data = $_POST;
        $this->load->model(['Curd','Work_status']);
        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];
//        $condition = 'inventory_id = '.$data['inventory_id'].' AND '.$data['status'].' and (select count(location_number) from count_work_status where location.location_number = location_number and counter_id <> '.$this->data['user']['users_id'].') = 0';
//        $response['data'] = $this->Curd->getByPlainWhere('location',$condition);

        $sql = 'select loc.name, 
                        loc.team, 
                        loc.location_number, 
                        iol.estimated_bins_per_hours, 
                        iol.estimated_bins,
                        iol.consigned,
                        iol.consume 
                        from 
                        inventory_in_location as iol 
                        left join location as loc on iol.location_id = loc.id 
                        where iol.inventory_id = '.$data['inventory_id'].' AND iol.'.$data['status'].' AND 
                        (select count(location_number) from count_work_status where loc.location_number = location_number and counter_id <> '.$this->data['user']['users_id'].') = 0';
        $response['data'] = $this->query($sql);
            //$this->getLocationByInventory('location',$data['inventory_id']);
        echo json_encode($response);
    }

    public function getLocationCount()
    {
        $data = $_POST;
        $this->load->model(['Curd','Work_status']);
        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];
//        $condition = 'inventory_id = '.$data['inventory_id'].' AND '.$data['status'].' and (select count(location_number) from count_work_status where location.location_number = location_number and counter_id <> '.$this->data['user']['users_id'].') = 0';
//        $response['data'] = $this->Curd->getByPlainWhere('location',$condition);

        $sql = 'select * from 
                        inventory_in_location as iol 
                        left join location as loc on iol.location_id = loc.id 
                        where iol.inventory_id = '.$data['inventory_id'].' AND iol.'.$data['status'].' AND 
                        (select count(location_number) from count_work_status where loc.location_number = location_number and counter_id <> '.$this->data['user']['users_id'].') = 0';
        $response['data'] = $this->query($sql);
        //$this->getLocationByInventory('location',$data['inventory_id']);
        echo json_encode($response);
    }

    private function query($sql)
    {
        $this->load->model("Curd");

        $data = $this->Curd->query($sql);

        return $data;
    }


    //Update Location
    public function updateLocation()
    {
        $data = $_POST;

        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Update Location','message'=>'Team added to location successfully'];

        $result = $this->Curd->updateByWhere("location",$data,['location_number'=>$data['location_number']]);

        $response['data'] = $result;

        echo json_encode($response);
    }

    //Update Location
    public function updateLocationStatus()
    {
        $data = $_POST;
        //Load Curd Model
        $this->load->model(["Curd",'Items_model','Inventory_model']);
        
        //Set Response
        $response = ['status'=>'ok','title'=>'Update Location','message'=>'Location status completed'];
//        $location = $this->Curd->getByWhere("location",['location_number'=>$data['location_number']]);

        $location = $this->query('select * from inventory_in_location as iol left join location as loc on iol.location_id = loc.id where loc.location_number = "'.$data['location_number'].'"');

        $estimated_bins = $location[0]['estimated_bins'];

        $inventory_id = $location[0]['inventory_id'];

        $inventory = $this->Inventory_model->getByID($inventory_id);
        $audit = $inventory[0]['audit_percent'];

        $resultRow = $this->Items_model->countItem($inventory_id,$data['location_number']);

        $actual_bins = $resultRow[0]['actual_bins'];
        $count_percent = ($actual_bins*100)/$estimated_bins;
        $status_id = 0;
        if(isset($data['status'])&&$data['status']=='Completed')
        {
            $user_id = $this->data['user']['users_id'];
            $this->load->model('Work_status');
            $status = $this->Work_status->getByCondition(['counter_id'=>$user_id,'location_number'=>$data['location_number'],'inventory_id'=>$location[0]['inventory_id']]);
            $datetime1 = strtotime($status[0]['start_time']);
            $datetime2 = strtotime(date('Y-m-d h:i:s'));
            $interval  = abs($datetime2 - $datetime1);
            $minutes   = round($interval / 60);
            $data['auto_count_time_min'] = $minutes;
            $status_id = $status[0]['id'];
        }

        if($audit!=$count_percent)
        {
            if(isset($data['status']))
            {
                $data['status'] = "Audit";
            }
        }
        unset($data['location_number']);

        $data['location_id'] = $location[0]['id'];

        $result = $this->Curd->updateByWhere("inventory_in_location",$data,['location_id'=>$location[0]['id'],'inventory_id'=>$location[0]['inventory_id']]);

        $this->Work_status->delete($status_id);

        $result = true;

        $response['data'] = $result;

        echo json_encode($response);
    }

    //Update Items
    public function updateItem()
    {
        $data = $_POST;

        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Update Item','message'=>'Team added to location successfully'];
        $item = $data['item'];
        $result = $this->Curd->updateByWhere("items",$item,['id'=>$item['id']]);

        $response['data'] = $result;

        echo json_encode($response);
    }

    private function getLocationByInventory($table,$inventory_id)
    {
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];

        if($inventory_id>0)
        {
            $result = $this->Curd->getByWhere($table,
                [
                    'inventory_id'=>$inventory_id
                ]);

            $response['data'] = $result;
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    //Get Location details
    function getLocationDetail()
    {
        $this->load->model("Curd");
        $data = $_POST;
        //$data["location_number"] = "CEN-DEP-1";
        $location_number = $data["location_number"];
        $response = ['status'=>'ok','title'=>'Retrieve Detail','message'=>'Detail retrieve successfully'];
        $query = "select
                    inv.audit_percent,
                    loc.location_number,
                    loc.name as location,
                    area.name as area,
                    dept.name as dept,
                    loc.consume,
                    loc.consigned,
                    loc.estimated_bins,
                    loc.estimated_bins_per_hours,
                    loc.team
                    from
                    location as loc
                    left join inventory as inv on loc.inventory_id = inv.id
                    left join general_area as area on loc.area_id = area.id
                    left join departments as dept on area.dept_id = dept.id where loc.location_number = '$location_number'";

        $location = $this->Curd->query($query);
        if(count($location)>0)
        {
            $response['data'] = $location[0];
        }else
        {
            $response['status'] = 'fail';
            $response['message'] = 'No location found';
        }

        echo json_encode($response);
    }

    private function get($table)
    {
        $this->load->model("Curd");
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];
        if(!isset($data['id']))
        {
            $result = $this->Curd->getAll($table);
            if(count($result)==0)
            {
                $response['status'] = 'fail';
                $response['message'] = 'Record not found';
            }
            else
            {
                $response['data'] = $result;
            }
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    private function insert($table,$data)
    {
        $response = ['status'=>'ok','title'=>'Add','message'=>'Record added successfully'];
        $this->load->model(["Curd"]);
        $id = $this->Curd->insert($table,$data);
        $response['data'] = ['id'=>$id];
        return $response;
    }

    private function update($table,$data)
    {
        $this->load->model("Curd");
        $response = ['status'=>'ok','title'=>'Update','message'=>'Record update successfully'];
        if(isset($data['id']))
        {
            $response['result'] = $this->Curd->update($table,$data);
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
            $response['result'] = false;
        }
        return $response;
    }

    private function delete($table,$id)
    {
        $response = ['status'=>'ok','title'=>'Remove','message'=>'Record deleted successfully'];
        $this->load->model(["Curd"]);
        $id = $this->Curd->delete($table,$id);
        return $response;
    }

    public function workStatus()
    {
        $data = $_POST;
        $user_id = $this->data['user']['users_id'];
        $data['counter_id'] = $user_id;
        $this->load->model('Work_status');
        $status = $this->Work_status->getByUserID($user_id);

        $response = ['status'=>'ok','title'=>'Save Status','message'=>'Status save successfully'];
        if(count($status)==1)
        {
            $this->Work_status->update($status[0]['id'],$data);
        }
        else
        {
            $data['start_time'] = date('Y-m-d h:i:s');
            $this->Work_status->insert($data);
        }

        echo json_encode($response);
    }

    public function getStatus()
    {
        $data = $_POST;
        $this->load->model('Work_status');
        $user_id = $this->data['user']['users_id'];
        $status = $this->Work_status->getByUserID($user_id);

        $response = ['status'=>'ok','title'=>'Save Status','message'=>'Status save successfully'];
        if( count($status) == 1 )
        {
            $response['countStatus'] = $status[0]['status'];
            $response['data'] = $status[0];
        }

        echo json_encode($response);
    }

    public function countSheet()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Manufacture Item No','field' => 'manufacture_item_number','attributes'=>['required'=>'required']],
            ['label'=>'Manufacture Name','field' => 'manufacture_name','attributes'=>['required'=>'required']],
            ['label'=>'Manufacture Item Description','field' => 'manufacture_item_description','attributes'=>['required'=>'required']],
            ['label'=>'Unit of Measurement','field' => 'low_unit_of_mesure','attributes'=>['required'=>'required']],
            ['label'=>'Packaging(e.g 20ES/CS)','field' => 'packing','attributes'=>['required'=>'required']],
            ['label'=>'ON Hand Quantity','field' => 'on_hand_quantity','attributes'=>['required'=>'required']],
            ['label'=>'Expired Quantity','field' => 'expired_quantity','attributes'=>['required'=>'required']],
            ['label'=>'Location Number','field' => 'location_number','attributes'=>['required'=>'required']]
        ];

        $load_data = [
            'type' => 'plainQuery',
            'primaryKey' => 'id',
            'col' => [
                'manufacture_item_number',
                'manufacture_name',
                'manufacture_item_description',
                'low_unit_of_mesure',
                'packing',
//                'on_hand_quantity',
//                'expired_quantity'
            ],
            'table' => 'items',
            'where' =>'id = 0'
        ];

        if(isset($data['condition']))
        {
            $load_data['where'] = "";
            if(isset($data['condition']['mfg_item_number']))
            {
                $id = preg_replace('/[^A-Za-z0-9]/', '', $data['condition']['mfg_item_number']);
                $load_data['where'] = "manufacture_item_number_nodash ='$id'";
            }else
                if(isset($data['condition']['multi_item_num'])&&$data['condition']['multi_item_num']!="")
            {
            $data['condition']['multi_item_num'] = preg_replace('/[^A-Za-z0-9]/', '', $data['condition']['multi_item_num']);
            $load_data['where'] .= "manufacture_item_number like '".$data['condition']['multi_item_num']."%' ";
            }

            if(isset($data['condition']['description'])&&$data['condition']['description']!="")
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";
                $load_data['where'] .= "manufacture_item_description like '%".$data['condition']['description']."%' ";
            }

            if(isset($data['condition']['vendor'])&&$data['condition']['vendor']!="")
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";

                $load_data['where'] .= "vendor_item_no like '".$data['condition']['vendor']."%' ";
            }
             if(isset($data['condition']['all']))
             {

             }
             else if(isset($data['condition']['location_number']))
             {
                 $num = $data['condition']['location_number'];
                 if($load_data['where']!="")
                     $load_data['where'] .= " AND ";
                 $load_data['where'] .= " location_number ='$num'";
             }
             else{
                 if($load_data['where']!="")
                     $load_data['where'] .= " AND ";
                 $load_data['where'] .= " (location_number is NULL or location_number = '')";
             }



        }


        $this->load->library('easy_curd',['table'=>'items','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }


    public function action()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Name','field' => 'name'],
            /*['label'=>'Hospital',
                'field' => 'location_id',
                'relation'=>
                    [
                        'table'=>'location',
                        'key'=>'id',
                        'label'=>'name'
                    ]
            ]*/
        ];

        $load_data = [
            'type' => 'joinQuery',
            'primaryKey' => 'i.id',
            'col' => [
                'i.id',
                'i.pi_date',
                'h.name',
                'i.status'
            ],
            'table' => 'inventory i left join hospital h on i.hospital_id = h.id',
            'where' =>''
        ];

        if(isset($_POST['hospital_id'])&&$_POST['hospital_id']!=0)
        {
            $load_data['where'] = "i.hospital_id = ".$_POST['hospital_id'];
        }

        if( $this->data['user']['userlvl']==2 )
        {

        }
        else if( $this->data['user']['userlvl']==3 )
        {
            if(isset($_POST['action']))
            {
                $_POST['action']['edit'] = "false";
                $_POST['action']['delete'] = "false";
            }
        }

        if(isset($_POST['status']))
        {
            $status = $_POST['status'];
            if($status != "")
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";
                $load_data['where'] .= " i.status = '$status'";
            }
        }

        $this->load->library('easy_curd',['table'=>'inventory','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }



}