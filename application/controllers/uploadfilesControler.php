<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class uploadfilesControler extends CI_Controller
{

    private $data;
    private $js = 0;
    private $css = 0;

    public function __construct()
    {
        parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }

        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        $user = $this->data['user'];
        $this->data['fileuploadstatus']="";
       
        if($user['userlvl']==3)
        {
            redirect("/counter");
        }
         $this->load->model('Gernal_model');    
         if($user['userlvl']==2)
        {
            $this->data['hospitals_list']=$this->Gernal_model->get_client_hospital($this->Um_users_model->get_id_by_username($username));
        }
        else
        {
           $this->data['hospitals_list']=$this->Gernal_model->get_all_data('hospital');  
        }
            
       /* else if($user['userlvl']==2)
        {
            redirect("/client");
        }
*/
        //$this->data['css'][$this->css++] = "assets/css/custom/internal_user.css";
        $this->data['js'][$this->js++] =  base_url("assets/js/easy_curd.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/custom/files.js");
    }

    public function index()
    {
       $this->load->view('admin/filesupload/index', $this->data);
    }


    public function action()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];
             $form_field = [
            ['label'=>'File Name','field' => 'filename'],
            ['label'=>'Uploaded By','field' => 'uploaded_by'],
            ];
        $load_data = [
            'type' => 'plainQuery',
            'primaryKey' => 'id',
            'col' => [
                'id',
                'filename',
                'uploaded_by',
                'fileurl',
            ],
            'table' => 'files',
            'where' =>''
        ];
        if( $this->data['user']['userlvl']==2 )
        {
            $load_data['where'] = "hospital IN (select id from hospital where client_id='".$this->data['user']['users_id']."')";
        }
          $this->load->library('easy_curd',['table'=>'files','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }
    
    function do_upload()
	{
		$config['upload_path'] = './uploadfiles/';
		$config['max_size']	= '1000';
        $config['allowed_types'] = 'csv|gif|jpg|png';
        $username=$this->user_manager->this_user_name();
        $level=$this->Um_users_model->get_level_by_username($username)[0];
        if($level['userlvl']=='1')
        {
            $uploadby=$username. '(QI)';
        }
        else
        {
            $uploadby=$username. '(Client)';
        }
   //  $filename=($this->um_users_model->get_username_by_email($email)).($this->um_users_model->get_id_by_email($email));
  //   $config['file_name']  =$filename ;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
            $this->data['fileuploadstatus']=$error['error'];
            $this->load->view('admin/filesupload/index', $this->data);
        //    var_dump($error);
		//	$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
           // base_url().'dps/'.$data['upload_data']['file_name']	
            
                $dbdata=array(
						'filename'=>$data['upload_data']['file_name'],
						'fileurl'=>'<a href="'.base_url().'uploadfiles/'.$data['upload_data']['file_name'].'" />Click Here</a>',
						'uploaded_by'=>$uploadby,
						'hospital'=>$this->input->post('hospital'),
					);
            $this->load->model('Gernal_model');
            $this->Gernal_model->add_table_data('files',$dbdata);
            $this->data['fileuploadstatus']='File Uploded';
            $this->load->view('admin/filesupload/index', $this->data);
		}
	}


}