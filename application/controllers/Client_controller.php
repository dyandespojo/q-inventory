<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client_controller extends CI_Controller 
{
    private $data;
	public function __construct()
	{
		parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }
        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        $user = $this->data['user'];
    //   echo $this->Um_users_model->get_user_licensed_status($username);
        if($user['userlvl']==3)
        {
            redirect("/counter");
        }
        else if($user['userlvl']==1)
        {
            redirect("/admin");
        }
        
            if($this->Um_users_model->get_user_licensed_status($username)!='yes')
        {
            $this->load->view('client/unlicensed',$this->data);
        }
    }
            
	public function index()
	{
	 $this->load->view('client/main',$this->data);
	}             
}