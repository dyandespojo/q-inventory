<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller
{

    private $data;
    private $js = 0;
    private $css = 0;

    public function __construct()
    {
        parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if (!$logged_in) {
            redirect("/");
        }

        $username = $this->session->userdata('logged_in')['username'];

        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        $user = $this->data['user'];
        if ($user['userlvl'] == 3) {
            redirect("/counter");
        }

        $this->data['css'][$this->css++] = base_url("assets/css/jquery.steps.css");
        $this->data['css'][$this->css++] = base_url("assets/plugins/datepicker/css/bootstrap-datepicker.css");
        $this->data['css'][$this->css++] = base_url("assets/css/custom/map.css");

        $this->data['js'][$this->js++] = base_url("assets/js/jquery-ui.js");
        $this->data['js'][$this->js++] = base_url("assets/js/custom/map.js");
        $this->data['js'][$this->js++] = base_url("assets/plugins/datepicker/js/bootstrap-datepicker.js");
        $this->data['js'][$this->js++] = base_url("assets/js/jquery.steps.min.js");
        $this->data['js'][$this->js++] = base_url("assets/js/jquery.csv-0.71.min.js");
        $this->data['js'][$this->js++] = base_url("assets/js/easy_curd.js");
        $this->data['js'][$this->js++] = base_url("assets/js/custom/inventory.js");
    }

    public function index()
    {
        $this->load->helper('form');
        $this->load->view('hospitals/index', $this->data);
    }

    public function inventoryList($id = 0)
    {
        if ($id == 0)
            redirect("/hospitals");
        $this->load->helper('form');
        $this->data['hospital_id'] = $id;
        $this->load->model("Hospital_model");
        $this->load->model("Department_model");
        $this->load->model("Area_model");
        $this->load->model("Location_model");
        $this->data['departments'] = $this->Department_model->getByHospital($id);
        $this->data['hospital'] = $this->Hospital_model->get_all_hospital();
        $this->data['selected_hospital'] = "";
        $data = $this->Hospital_model->getByID($id);
        $this->data['selected_hospital'] = $data[0];
        $this->data['area'] = $this->Area_model->getAll();
        $this->data['location'] = $this->Location_model->getAll();
        $this->load->view('admin/inventory/index', $this->data);
    }


    public function setup($operation = "new")
    {
        $data = $_POST;

        $inventory = $data['inventory'];
        $hospital = $data['hospital'];
        $response = ['status' => 'ok', 'title' => 'Setup', 'message' => 'Record added successfully'];
        $hospital_response = $this->update('hospital', $hospital);
        $this->load->model('Curd');
        $date = $this->Curd->getByWhere('inventory', ['pi_date' => $inventory['pi_date'], 'hospital_id' => $hospital['id']]);
        if (count($date) == 0) {
            if (isset($inventory['id'])) {
                $inventory_response = $this->update('inventory', $inventory);
                $response['id'] = $inventory['id'];
                if ($inventory_response['result']) {
                    $response['message'] = "Inventory updated successfully";
                } else {
                    $response['status'] = 'warn';
                    $response['message'] = "Inventory not updated";
                }

            } else {
                $inventory_response = $this->insert('inventory', $inventory);
                $response['id'] = $inventory_response['data']['id'];
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = "Inventory Date already in record.";
        }

        echo json_encode($response);
    }

    /***************************Delete Inventory*****************************************/
    function deleteInventory()
    {
        $data = $_POST;
        $id = $data['id'];
        $this->load->model('Curd');
        $response = ['status' => 'ok', 'title' => 'Delete Inventory', 'message' => 'Inventory remove successfully'];

        //<---------Note--------->
        //Delete maps related to location
        //Maps delete functionality need to be implemented

        //Delete location related to inventory
        $this->Curd->deleteByCondition('location', "inventory_id = $id");
        //Delete All Items
        $this->Curd->deleteByCondition('items', "inventory_id = $id");
        //Delete all location
        $this->Curd->deleteByCondition('inventory', "id = $id");

        echo json_encode($response);
    }


    /****************************Upload CSV File***********************************/

    public function CheckItems()
    {
        $data = $_POST;
        $this->load->model(['Items_model']);
        $response = ['status'=>'ok','title'=>'Check Items','message'=>'Items for inventory'];
        $items = $this->Items_model->getByCondition(['inventory_id'=>$data['inventory_id']]);
        if(count($items)>0)
        {
            $response['status'] = "warn";
            $response['message'] = 'Items for inventory already exist,<br>To remove old and import new click yes,<br>To skip importing item click no.';
        }

        echo json_encode($response);
    }

    public function uploadCSVfile()
    {
        $response = ['status'=>'ok','title'=>'Insert Items','message'=>'Items added successfully'];
        $this->load->helper(array('form', 'url'));
        $config['upload_path'] = './files/';
        $config['allowed_types'] = 'gif|jpg|png|csv';

        $this->load->library('upload', $config);

        if( !$this->upload->do_upload("file") )
        {
            $data = array('error' => $this->upload->display_errors());
            $response['status'] = "fail";
            $response['message'] = "Please select valid file";
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $name = $data['upload_data']['file_name'];
            $this->load->library('csvimport');
            $result = $this->csvimport->get_array('./files/'.$name);
            if(isset($_POST['inventory_id']))
            {
                $this->load->library('csvimport');
                $this->load->model(['temp_items_model','Inventory_items_model']);
                
                if(isset($_POST['remove']))
                {
                    $this->temp_items_model->deleteItem($_POST['inventory_id']);
                }

                $inventory_id = $_POST['inventory_id'];
                $result = $this->csvimport->get_array('./files/'.$name);

                foreach($result as $item)
                {

                    $items = [];
                    $items = [
                        'countsheet_control_number'=>$item['COUNTSHEET CONTROL NUMBER'],
                        'location_number'=>$item['LOCATION NUMBER'],
                        'bin'=>$item['BIN (SHELF)'],
                        'manufacture_item_number'=>$item['MANUFACTURER ITEM NUMBER'],
                        'manufacture_item_number_nodash'=>$item['MANUFACTURER ITEM NODASH'],
                        'manufacture_name'=>$item['MANUFACTURER NAME'],
                        'manufacture_item_description'=>$item['MANUFACTURE ITEM DESCRIPTION'],
                        'on_hand_qty'=>$item['ON HAND QTY'],
                        'on_hand_unit_of_measure'=>$item['ON HAND UNIT OF MEASURE (CS,BX,EA)'],
                        'expired_on_hand_qty'=>$item['EXPIRED ON HAND QTY'],
                        'hospital_item_number'=>$item['HOSPITAL ITEM NUMBER'],
                        'effective_uom_to_luom_factor'=>$item['EFFECTIVE UOM TO LUOM FACTOR'],
                        'on_hand_uom_to_luom_factor'=>$item['ON HAND UOM TO LUOM FACTOR'],
                        'stock_yestno'=>$item['STOCK_YESTNO'],
                        'effective_unit_of_measure_price'=>$item['EFFECTIVE UNIT OF MEASURE PRICE'],
                        'consignment_flag'=>$item['CONSIGNMENT FLAG'],
                        'dioh_flag'=>$item['90 DIOH FLAG'],
                        'low_unit_of_mesure_price'=>$item['LOUM PRICE'],
                        'item_source'=>$item['ITEM SOURCE'],
                        'price_source'=>$item['PRICING SOURCE'],
                        'packing_soucre'=>$item['PACKAGING SOURCE'],//
                        'vendor_item_no'=>$item['VENDOR ITEM NO'],
                        'price_match'=>$item['PRICE MATCH'],
                        'pi_align'=>$item['PI ALIGN'],
                        'original_hospital_mgf_id_no_dash'=>$item['ORIGINALHOSPITALMFGIDNODASH'],
                        'original_hospital_vendor_item_no_dash'=>$item['ORIGINALHOSPITALVENDORITEMNODASH'],
                        'packing'=>$item['PACKAGING'],
                        'purchase_unit_of_measure'=>$item['PURCHASE UNIT OF MEASURE'],
                        'effective_uom_to_luom_factor'=>$item['EFFECTIVE UOM TO LUOM FACTOR'],
                        'inventory_id'=>$inventory_id
                    ];

                    $result = $this->temp_items_model->getByMft($items['manufacture_item_number']);
                    if(count($result)>0)
                    {
                        $this->temp_items_model->updateMft($items);
                        $item_id = $result[0]['id'];
                    }
                    else
                    {
                        $item_id = $this->temp_items_model->insertGetID($items);
                    }

                }

                unlink('./files/'.$name);
            }
            else
            {
                $response['status'] = "fail";
                $response['message'] = "Please select inventory";
            }

        }
        echo json_encode($response);
    }


    /****************************Department********************************/

    public function getInventory()
    {
        $data = $_POST;
        $this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Get Inventory','message'=>'Retrieve successfully'];
        $data = $this->Curd->getByID('inventory',$data['id']);
        if(count($data)>0)
        {
            $response['data'] = $data[0];
        }
        else
        {
            $response['status'] = "fail";
            $response['message'] = "Inventory not found";
        }

        echo json_encode($response);
    }


    /****************************Department********************************/

    public function getDepartments()
    {
        $data = $_POST;
        $response = ['status'=>'ok','title'=>'Get Department','message'=>'Retrieve successfully'];
        $response = $this->getDepartmentByHospital('departments',$data['hospital_id']);
        echo json_encode($response);
    }

    public function addDepartments()
    {
        $data = $_POST;
        $department = $data['department'];

        //Insert new department in Database
        $response = $this->insert('departments',$department);

        //Get Department by Hospital ID
        $hospital_department = $this->getDepartmentByHospital('departments',$department['hospital_id']);

        $response['location_number'] = $this->getLocationNumber($response['data']['id']);

        $response['data']['departments'] = $hospital_department['data'];

        echo json_encode($response);
    }

    private function getDepartmentByHospital($table,$hospital_id)
    {
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];

        if($hospital_id>0)
        {
            $result = $this->Curd->getByWhere($table,['hospital_id'=>$hospital_id]);
            $response['data'] = $result;
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    /************************************Area**********************************/

    public function getAreas()
    {
        $data = $_POST;
        $response = ['status'=>'ok','title'=>'Get Area','message'=>'Retrieve successfully'];
        $response = $this->getAreaByDepartment('general_area',$data['department_id']);
        $response['location_number'] = $this->getLocationNumber($data['department_id']);
        echo json_encode($response);
    }


    public function getPostAreas()
    {
        $data = $_POST;
        $response = ['status'=>'ok','title'=>'Get Area','message'=>'Retrieve successfully'];
        $this->load->model('Curd');
        $ids = "(0)";
        if($data['dept_id']!='all')
        {
            $ids = '('.$data['dept_id'].')';
        }else
        {
            $query = 'select id from departments
            where
            hospital_id = '.$data['hospital_id'];

            $dept = $this->query($query);
            
            $dept_ids = [];
            foreach($dept as $row)
            {
                array_push($dept_ids,$row['id']);
            }
            $ids = '('.join(',',$dept_ids).')';
        }


        $sql = "select * from general_area where dept_id in $ids";
        $response['data'] = $this->Curd->query($sql);
        //$response['location_number'] = $this->getLocationNumber($data['department_id']);
        echo json_encode($response);
    }

    //Generate Location Number
    private function getLocationNumber($dept_id)
    {
        $this->load->model('Curd');
        $department = $this->Curd->getByID('departments',$dept_id);
        $hospital = $this->Curd->getByID('hospital',$department[0]['hospital_id']);
        $max_location_id = $this->Curd->max('location');
        $hospital_prefix = strtoupper(substr($hospital[0]['name'],0,3));
        $department_prefix = strtoupper(substr($department[0]['name'],0,3));
        if($max_location_id[0]['id'])
        {
            $num = $max_location_id[0]['id']+1;
        }
        else
        {
            $num = 1;
        }

        $location_number = $hospital_prefix."-".$department_prefix."-".$num;

        return $location_number;
    }

    public function addArea()
    {
        $data = $_POST;
        $area = $data['area'];

        //Insert new department in Database
        $response = $this->insert('general_area',$area);

        //Get Department by Hospital ID
        $department_areas = $this->getAreaByDepartment('general_area',$area['dept_id']);

        $response['data']['areas'] = $department_areas['data'];

        echo json_encode($response);
    }

    public function LocationNumber()
    {
        $data = $_POST;
        $response = ['status'=>'ok','title'=>'Get Area','message'=>'Retrieve successfully'];
        $response = $this->getAreaByDepartment('general_area',$data['department_id']);
        $response['location_number'] = $this->getLocationNumber($data['department_id']);
        echo json_encode($response);
    }

    public function addLocation()
    {
        $data = $_POST;

        $dept_id = $data['dept_id'];

        $inventory_id = $data['inventory_id'];


        unset($data['dept_id']);

        unset($data['inventory_id']);

        //Insert new department in Database
        $response = $this->insert('location',$data);


        $location = $this->query("select * from location where id = ".$response['data']['id']);
        $info = $location[0];

        //Get Location
        $query = 'select * from location 
                  where 
                  id not in 
                  (select DISTINCT location_id from inventory_in_location where inventory_id = '.$inventory_id.') 
                  and 
                  area_id = '.$data['area_id'];

        $locations = $this->query($query);

        $response['data']['location'] = $locations;
        $response['data']['info'] = $info;
        echo json_encode($response);
    }

    private function query($sql)
    {
        $this->load->model("Curd");

        $data = $this->Curd->query($sql);

        return $data;
    }


    private function getLocationByArea($table,$condition)
    {
        //Load Curd Model
        $this->load->model("Curd");

        $result = $this->Curd->getByWhere($table, $condition);

        return $result;
    }

    private function getAreaByDepartment($table,$department_id)
    {
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];

        if($department_id>0)
        {
            $result = $this->Curd->getByWhere($table,
                [
                    'dept_id'=>$department_id
                ]);

            $response['data'] = $result;
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    /************************************Pre Physical Inventory*************************************/

    public function pre_pl()
    {
        $data = $_POST;

        $location_inventory = $data['location_inventory'];

        $inventory_id = $location_inventory['inventory_id'];
        $area_id = $data['location']['area_id'];
        //Insert new Location in Database
        $response = $this->insert('inventory_in_location',$location_inventory);

        //Get Location
        $query = 'select * from location 
                  where 
                  id not in 
                  (select DISTINCT location_id from inventory_in_location where inventory_id = '.$inventory_id.') 
                  and 
                  area_id = '.$area_id;

        $locations = $this->query($query);

        $response['data']['location'] = $locations;

        //$response['location_number'] = $this->getLocationNumber($data['department']['id']);

        echo json_encode($response);
    }

    //Print Pre PI Module
    public function printPre_pi()
    {
        $dept_id = "all";
        $area_id = "all";
        $hospital_id = "all";
        if(isset($_GET['dept_id']))
        {
            $dept_id = $_GET['dept_id'];
        }

        if(isset($_GET['area_id']))
        {
            $area_id = $_GET['area_id'];
        }

        $where = "";

        if($hospital_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "hpt.id = ".$hospital_id;
        }

        if($dept_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "dept.id = ".$dept_id;
        }

        if($area_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "area.id = ".$area_id;
        }

        $fileName = 'exportData.csv';
        $header = [
            'Physical Inventory Date',
            'Audit Percent',
            'Status',
            'Hospital',
            'Address',
            'City',
            'State',
            'Zip',
            'Contact Name',
            'Email',
            'Phone',
            'Department',
            'General Area',
            'Location',
            'Location Number',
            'POU/Main',
            'Consigned',
            'Estimated Bins',
            'Estimated Bins Per Hours',
            'Estimated Count Time (Hours)',
            'Estimated Count Time (Minutes)'
        ];

        $data[] = $header;

        $this->load->model("Curd");



        $sql_query = "select
                    *
                    from
                    hospital";

        if($hospital_id != "all")
        {
            $sql_query .= ' where id = '.$hospital_id;
        }

        $hospital = $this->Curd->query($sql_query);

        $sql_query = "select
                    inv.pi_date,
                    inv.audit_percent,
                    inv.status,
                    hpt.name as 'hospital',
                    hpt.address,
                    hpt.city,
                    hpt.state,
                    hpt.zip,
                    hpt.contact_name,
                    hpt.email,
                    hpt.phone,
                    dept.name as 'department',
                    area.name as 'general_area',
                    loc.name as 'location',
                    loc.location_number,
                    iol.consume,
                    iol.consigned,
                    iol.estimated_bins,
                    iol.estimated_bins_per_hours,
                    Round((iol.estimated_bins/iol.estimated_bins_per_hours),2) as 'estimate_count_time_hours',
                    Round(((iol.estimated_bins/iol.estimated_bins_per_hours)*60),2) as 'estimate_count_time_min'
                    from
                    inventory_in_location as iol
                    left join
                    inventory as inv
                    on inv.id = iol.inventory_id 
                    left join location as loc on loc.id = iol.location_id
                    left join general_area as area on area.id = loc.area_id
                    left join departments as dept on dept.id = area.dept_id
                    left join hospital as hpt on dept.hospital_id = hpt.id $where";

        $result = $this->Curd->query($sql_query);

        if(isset($hospital[0]))
        {
            $this->data["hospital"] = $hospital[0];
        }

        $this->data["result"] = $result;
        $this->load->view('admin/inventory/print', $this->data);

    }

    public function mapPrintView()
    {
        $data = $_GET;
		$this->load->model("Curd");
		if(isset($data['dept'])&&isset($data['area']))
		{
			$sql = 'select 
			dept.name as dept, 
			area.name as area, 
			location.name as specific_name,
			location.location_number,
			location.position_top,
			location.position_left from departments as dept 
			right join general_area as area on dept.id = area.dept_id 
			right join location on location.area_id = area.id where dept.id = '.$data['dept'].' and area.id = '.$data['area'];
					
				$dept = $this->Curd->query($sql);
				$this->data['map_data'] = $dept; 
		}
		else if(isset($data['dept']))
		{
				$sql = 'select 
					dept.name as dept, 
					area.name as specific_name,
					area.position_top,
					area.position_left 
					from 
					departments as dept 
					right join 
					general_area as area 
					on 
					dept.id = area.dept_id 
					where 
					dept.id = '.$data['dept'];
					
				$dept = $this->Curd->query($sql);
				$this->data['map_data'] = $dept;
		}
		else
		{
				echo "<h1>200 Page not found</h1>";
				exit;
		}
		
        $this->load->view('admin/inventory/mapPrint', $this->data);
    }

    //Print Pre PI Module
    public function printPost_pi()
    {
        $hospital_id = "all";
        $status = "all";
        
        $where = "";

        if($hospital_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "hpt.id = ".$hospital_id;
        }

        if($status != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "status = ".$status;
        }


        $fileName = 'exportData.csv';
        $header = [
            'Location',
            'Location Number',
            'Status',
            'POU/Main',
            'Consigned',
            'Estimated Bins',
            'Estimated Bins Per Hours',
            'Estimated Count Time (Hours)',
            'Estimated Count Time (Minutes)',
            'Status',
            'Hospital',
            'Address',
            'City',
            'State',
            'Zip',
            'Contact Name',
            'Email',
            'Phone',
            'Department',
            'General Area'
        ];

        $data[] = $header;

        $this->load->model("Curd");

        $sql_query = "select
                        loc.name as 'location',
                        loc.location_number,
                        loc.status,
                        loc.consume,
                        loc.consigned,
                        loc.estimated_bins,
                        loc.estimated_bins_per_hours,
                        Round((loc.estimated_bins/loc.estimated_bins_per_hours),2) as 'estimate_count_time_hours',
                        Round(((loc.estimated_bins/loc.estimated_bins_per_hours)*60),2) as 'estimate_count_time_min',
                        hpt.name as 'hospital',
                        hpt.address,
                        hpt.city,
                        hpt.state,
                        hpt.zip,
                        hpt.contact_name,
                        hpt.email,
                        hpt.phone,
                        dept.name as 'department',
                        area.name as 'general_area'
                        from
                        location as loc
                        left join general_area as area on area.id = loc.area_id
                        left join departments as dept on dept.id = area.dept_id
                        left join hospital as hpt on dept.hospital_id = hpt.id $where";

        $result = $this->Curd->query($sql_query);

        $this->data["result"] = $result;
        $this->load->view('admin/inventory/postPrint', $this->data);

    }

    //Export Pre PI Module
    public function exportPre_pi()
    {
        $dept_id = "all";
        $area_id = "all";
        $hospital_id = "all";
        if(isset($_GET['dept_id']))
        {
            $dept_id = $_GET['dept_id'];
        }

        if(isset($_GET['area_id']))
        {
            $area_id = $_GET['area_id'];
        }

        if(isset($_GET['hospital_id']))
        {
            $hospital_id = $_GET['hospital_id'];
        }

        $where = "";

        if($hospital_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "hpt.id = ".$hospital_id;
        }

        if($dept_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "dept.id = ".$dept_id;
        }

        if($area_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "area.id = ".$area_id;
        }

        $fileName = 'exportData.csv';
        $header = [
            'Physical Inventory Date',
            'Audit Percent',
            'Status',
            'Hospital',
            'Address',
            'City',
            'State',
            'Zip',
            'Contact Name',
            'Email',
            'Phone',
            'Department',
            'General Area',
            'Location',
            'Location Number',
            'POU/Main',
            'Consigned',
            'Estimated Bins',
            'Estimated Bins Per Hours',
            'Estimated Count Time (Hours)',
            'Estimated Count Time (Minutes)'
        ];

        $data[] = $header;

        $this->load->model("Curd");

        $sql_query = "select 
                      inv.pi_date, 
                      inv.audit_percent, 
                      inv.status, 
                      hpt.name as 'hospital', 
                      hpt.address, 
                      hpt.city, 
                      hpt.state, 
                      hpt.zip, 
                      hpt.contact_name, 
                      hpt.email, 
                      hpt.phone, 
                      dept.name as 'department', 
                      area.name as 'general_area', 
                      loc.name as 'location', 
                      loc.location_number, 
                      iol.consume, 
                      iol.consigned, 
                      iol.estimated_bins, 
                      iol.estimated_bins_per_hours, 
                      Round((iol.estimated_bins/iol.estimated_bins_per_hours),2) as 'estimate_count_time_hours', 
                      Round(((iol.estimated_bins/iol.estimated_bins_per_hours)*60),2) as 'estimate_count_time_min' 
                      from 
                      inventory_in_location as iol 
                      left join inventory as inv on inv.id = iol.inventory_id 
                      left join location as loc on iol.location_id = loc.id 
                      left join general_area as area on area.id = loc.area_id 
                      left join departments as dept on dept.id = area.dept_id 
                      left join hospital as hpt on dept.hospital_id = hpt.id $where";

        $result = $this->Curd->query($sql_query);

        foreach($result as $row)
        {
            $data[] = $row;
        }

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fp = @fopen( 'php://output', 'w' );

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }


    //Export Pre PI Module
    public function exportPost_pi()
    {
        $dept_id = "all";
        $area_id = "all";
        $location_id = "all";
        $hospital_id = "all";

        if(isset($_GET['hospital_id']))
        {
            $hospital_id = $_GET['hospital_id'];
        }

        if(isset($_GET['dept_id']))
        {
            $dept_id = $_GET['dept_id'];
        }

        if(isset($_GET['area_id']))
        {
            $area_id = $_GET['area_id'];
        }

        if(isset($_GET['location_id']))
        {
            $location_id = $_GET['location_id'];
        }
        $where = '';
        //$where = " where iol.status = 'Completed' ";

        if($hospital_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "h.id = ".$hospital_id;
        }

        if($dept_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "dept.id = ".$dept_id;
        }

        if($area_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "area.id = ".$area_id;
        }

        if($location_id != "all")
        {
            if($where== "")
            {
                $where = " where ";
            }else
            {
                $where .= " AND ";
            }
            $where .= "loc.id = ".$location_id;
        }


        $fileName = 'exportData.csv';
        $header = [
            'Control Number',
            'Location Number',
            'Bin',
            'Manufacture Item Number',
            'Manufacture Name',
            'Manufacture Item Description',
            'On Hand Quantity',
            'On Hand Unit Of Measure',
            'Expired On Hand Quantity',
            'Hospital Item Number',
            'Low Unit Of Measure',
            'Effective uom to luom factor',
            ' On Hand uom to luom factor',
            'Stock yestno',
            'Effective Unit Of Measure Price',
            'Consignment Flag',
            'Dioh Flag',
            'Low Unit Of Measure Price',
            'Item Source',
            'Price Source',
            'Packing Soucre',
            'Vendor Name',
            'Vendor Item_no',
            'Price Match',
            'PI Align',
            'Original Hospital MGF ID No Dash',
            'Original Hospital Vendor Item No Dash',
            'Packing',
            'Purchase Unit Of Measure',
            'On Hand Quantity',
            'Expired Quantity',
            'Hospital',
            'Department',
            'General Area',
            'Location'
        ];

        $data[] = $header;

        $this->load->model("Curd");

        $sql_query = "SELECT 
                            i.countsheet_control_number,
                            loc.location_number,
                            i.bin,
                            i.manufacture_item_number,
                            i.manufacture_name,
                            i.manufacture_item_description,
                            i.on_hand_qty,
                            i.on_hand_unit_of_measure,
                            i.expired_on_hand_qty,
                            i.hospital_item_number,
                            i.low_unit_of_mesure,
                            i.effective_uom_to_luom_factor,
                            i.on_hand_uom_to_luom_factor,
                            i.stock_yestno,
                            i.effective_unit_of_measure_price,
                            i.consignment_flag,
                            i.dioh_flag,
                            i.low_unit_of_mesure_price,
                            i.item_source,
                            i.price_source,
                            i.packing_soucre,
                            i.vendor_name,
                            i.vendor_item_no,
                            i.price_match,
                            i.pi_align,
                            i.original_hospital_mgf_id_no_dash,
                            i.original_hospital_vendor_item_no_dash,
                            i.packing,
                            i.purchase_unit_of_measure,
                            i.on_hand_quantity,
                            i.expired_quantity,
                            h.name as 'hospital',
                            dept.name as 'department',
                            area.name as 'general_area', 
                            loc.name as 'location'
                            FROM 
                            hospital h 
                            right join 
                            departments dept 
                            on 
                            dept.hospital_id = h.id
                            right join 
                            general_area area
                            on area.dept_id = dept.id
                            right JOIN
                            location loc
                            on loc.area_id = area.id
                            right join
                            inventory_in_location as iol
                            on iol.location_id = loc.id
                            inner JOIN
                            location_items i
                            on i.location_id = loc.id $where";


/*
        echo $sql_query.'<br><br>';
        exit;*/


        $result = $this->Curd->query($sql_query);

/*
        echo json_encode($result);
        exit;*/

        foreach($result as $row)
        {
            $data[] = $row;
        }

        /*
        echo json_encode($data);
        exit;*/

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fp = @fopen( 'php://output', 'w' );

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }

    //Get Location
    public function getLocation()
    {
        $data = $_POST;
        //$this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];
        //$response['data'] = $this->Curd->getByPlainWhere('location','inventory_id = '.$data['inventory_id'].' AND '.$data['status']);
        $sql = 'select loc.name, 
                        loc.team, 
                        loc.location_number, 
                        iol.estimated_bins_per_hours, 
                        iol.estimated_bins,
                        iol.consigned,
                        iol.consume 
                        from 
                        inventory_in_location as iol 
                        left join location as loc on iol.location_id = loc.id 
                        where iol.inventory_id = '.$data['inventory_id'].' AND iol.'.$data['status'];
        $response['data'] = $this->query($sql);
            //$this->getLocationByInventory('location',$data['inventory_id']);
        echo json_encode($response);
    }


    //Get Location
    public function getLocationEdit()
    {
        $data = $_POST;

        //$this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];

        //$response['data'] = $this->Curd->getByPlainWhere('location','inventory_id = '.$data['inventory_id'].' AND '.$data['status']);
        $sql = 'select loc.name, 
                       loc.team, 
                       loc.location_number, 
                       iol.estimated_bins_per_hours, 
                       iol.estimated_bins,
                       iol.consigned,
                       iol.consume 
                       from 
                       inventory_in_location as iol 
                       left join location as loc on iol.location_id = loc.id 
                       where loc.id = '.$data['id'].' AND iol.inventory_id = '.$data['inventory_id'];

        $response['data'] = $this->query($sql);

        //$this->getLocationByInventory('location',$data['inventory_id']);
        echo json_encode($response);
    }

    public function updatelocationEdit()
    {
        $data = $_POST;
        $this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Update Location','message'=>'Updated successfully'];
        $inventory = $data['inventory'];
        $location = $data['location'];

        if(!isset($location['id'])||$location['id']==""||!isset($inventory['inventory_id'])||$inventory['inventory_id']=="")
        {
            $response['status'] = 'fail';
            $response['message'] = "Location or inventory not specify";
        }
        else
        {
            $result = $this->Curd->updateByWhere("location",$location,['id'=>$location['id']]);
            $result = $this->Curd->updateByWhere("inventory_in_location",$inventory,['location_id'=>$location['id'],'inventory_id'=>$inventory['inventory_id']]);
        }
        echo json_encode($response);
    }

    //Get Location
    public function getLocationPostPI()
    {
        $data = $_POST;
        $this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];
//        $response['data'] = $this->Curd->getByPlainWhere('location','inventory_id = '.$data['inventory_id'].' AND area_id = '.$data['area_id']);
        //$this->getLocationByInventory('location',$data['inventory_id']);

        //Get Location
        $query = 'select * from location 
                  where  
                  area_id = '.$data['area_id'];

        $locations = $this->query($query);

        $response['data'] = $locations;

        echo json_encode($response);
    }


    //Get Location
    public function getGAPostPI()
    {
        $data = $_POST;
        $this->load->model('Curd');
        $response = ['status'=>'ok','title'=>'Get General Area','message'=>'Retrieve successfully'];
        //Get Location
        $query = 'select * from general_area
                  where
                  dept_id = '.$data['dept_id'];

        $locations = $this->query($query);

        $response['data'] = $locations;

        echo json_encode($response);
    }

    //Get Location
    public function getMapBylocation()
    {
        $data = $_POST;
 
        $this->load->model('Curd');
//        $location = $this->Curd->getByPlainWhere('location','inventory_id = '.$data['inventory_id'].' AND location_number = "'.$data['location_number'].'"');
        $location = $this->query('select * from inventory_in_location as iol left join location as loc on loc.id = iol.location_id where iol.inventory_id = '.$data['inventory_id'].' AND loc.location_number ="'.$data['location_number'].'"');
//        echo json_encode($location[0]);
        $area_id = $location[0]['area_id'];
        $inventory_id = $data['inventory_id'];
        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];

        $response['data'] = $this->Curd->getByPlainWhere('location',' area_id = '.$area_id);
        //$this->getLocationByInventory('location',$data['inventory_id']);
        echo json_encode($response);
    }

    //Get Location with summary
    public function getLocationSummary()
    {
        $data = $_POST;
        $this->load->model('Curd');

        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];

        $response['data']['dept'] = $this->query('select * from departments where hospital_id = '.$data['hospital_id']);
        /*
        if($data['status']!="status = 'all'")
        {
            $response['data']['location'] = $this->query('select * from inventory_in_location as iol left join location as loc on loc.id = iol.location_id');
        }else
        {
            $response['data']['location'] = $this->query('select * from inventory_in_location as iol left join location as loc on loc.id = iol.location_id where iol.inventory_id = '.$data['inventory_id']);
        }*/

        $inventory_id = $data['inventory_id'];

        //Count All
        $total = $this->Curd->query("SELECT count(id) as total FROM `inventory_in_location` WHERE inventory_id = $inventory_id");
        $response['data']['total'] = $total[0]['total']?$total[0]['total']:0;

        //Count Completed
        $complete = $this->Curd->query("SELECT count(id) as completed FROM `inventory_in_location` WHERE status = 'Completed' AND inventory_id = $inventory_id");
        $response['data']['completed'] = $complete[0]['completed']?$complete[0]['completed']:0;

        //Count Remaining
        $remaining = $this->Curd->query("SELECT count(id) as remain FROM `inventory_in_location` WHERE status <> 'Completed' AND inventory_id = $inventory_id");
        $response['data']['remain'] = $remaining[0]['remain']?$remaining[0]['remain']:0;

        //Count Total Bins
        $total_bins = $this->Curd->query("SELECT sum(estimated_bins) as total_bins FROM `inventory_in_location` WHERE inventory_id = $inventory_id");
        $response['data']['total_bins'] = $total_bins[0]['total_bins']?$total_bins[0]['total_bins']:0;

        //Count Complete Bins
        $complete_bins = $this->Curd->query("SELECT sum(estimated_bins) as completed_bins FROM `inventory_in_location` WHERE status = 'Completed' AND inventory_id = $inventory_id");
        $response['data']['complete_bins'] = $complete_bins[0]['completed_bins']?$complete_bins[0]['completed_bins']:0;

        //Count Remain Bins
        $complete_bins = $this->Curd->query("SELECT sum(estimated_bins) as remain_bins FROM `inventory_in_location` WHERE status <> 'Completed' AND inventory_id = $inventory_id");
        $response['data']['remain_bins'] = $complete_bins[0]['remain_bins']?$complete_bins[0]['remain_bins']:0;

        //Complete Percent
        if($response['data']['total_bins']!=0)
            $response['data']['completed_percent'] = round(($response['data']['complete_bins']*100)/$response['data']['total_bins'],2);
        else
            $response['data']['completed_percent'] = 0;

        //Estimate time completion
        $complete_bins = $this->Curd->query("SELECT sum(estimated_bins_per_hours) as bins_time FROM `inventory_in_location` WHERE inventory_id = $inventory_id");
        $response['data']['bins_time'] = $complete_bins[0]['bins_time']*60;
/*
        //Count Audit Bins
        $audit_bins = $this->Curd->query("SELECT sum(estimated_bins) as completed_bins FROM `location` WHERE status = 'Audit' AND inventory_id = $inventory_id");
        $response['data']['complete_bins'] = $complete_bins[0]['completed_bins']?$complete_bins[0]['completed_bins']:0;
*/

        echo json_encode($response);
    }

        //Get Location with summary
    public function getPostLocationSummary()
    {
        $this->load->model('Curd');

        $data = $_POST;        

        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];
        
        $dept = "all";

        $area = "all";

        $location = "all";

        $where = "";

        if(isset($data['dept_id']))
        {
            if($data['dept_id']!="all")
            {
                $dept = $data['dept_id'];
                $where .= "where dept.id = ".$dept." ";
            }
        }

        if(isset($data['area_id']))
        {
            if($data['area_id']!="all")
            {
                $area = $data['area_id'];

                if($where == "")
                {
                    $where .= "where ";
                }
                else
                {
                    $where .= " AND ";
                }

                $where .= " area.id = ".$area." ";
            }
        }

        if(isset($data['location_id']))
        {
            if($data['location_id']!="all")
            {
                $location_id = [$data['location_id']];

                //$where = "where loc.id = ".$location." ";
            }
        }else
        {
                $sql = 'select 
                dept.id as dept_id,
                area.id as area_id,
                loc.id as location_id
                from departments as dept 
                right join general_area as area on area.dept_id = dept.id
                right join location as loc on loc.area_id = area.id '.$where;

                $locations = $this->Curd->query($sql);
                $location_id = [];
                foreach($locations as $row)
                {
                    array_push($location_id, $row['location_id']);
                }
        }

        //echo join(",",$location_id);
        //exit;
        $ids = join(",",$location_id);
        if(count($location_id)==0)
        {
            $ids = "(0)";
        }


        $inventory_id = $data['inventory_id'];

        //Count All
        
        $total = $this->Curd->query("SELECT count(id) as total FROM `inventory_in_location` WHERE inventory_id = $inventory_id and location_id in ($ids)");
        $response['data']['total'] = $total[0]['total']?$total[0]['total']:0;

        //Count Completed
        $complete = $this->Curd->query("SELECT count(id) as completed FROM `inventory_in_location` WHERE status = 'Completed' AND inventory_id = $inventory_id and location_id in ($ids)");
        $response['data']['completed'] = $complete[0]['completed']?$complete[0]['completed']:0;

        //Count Remaining
        $remaining = $this->Curd->query("SELECT count(id) as remain FROM `inventory_in_location` WHERE status <> 'Completed' AND inventory_id = $inventory_id and location_id in ($ids)");
        $response['data']['remain'] = $remaining[0]['remain']?$remaining[0]['remain']:0;

        //Count Total Bins
        $total_bins = $this->Curd->query("SELECT sum(estimated_bins) as total_bins FROM `inventory_in_location` WHERE inventory_id = $inventory_id and location_id in ($ids)");
        $response['data']['total_bins'] = $total_bins[0]['total_bins']?$total_bins[0]['total_bins']:0;

        //Count Complete Bins
        $complete_bins = $this->Curd->query("SELECT sum(estimated_bins) as completed_bins FROM `inventory_in_location` WHERE status = 'Completed' AND inventory_id = $inventory_id and location_id in ($ids)");
        $response['data']['complete_bins'] = $complete_bins[0]['completed_bins']?$complete_bins[0]['completed_bins']:0;

        //Count Remain Bins
        $complete_bins = $this->Curd->query("SELECT sum(estimated_bins) as remain_bins FROM `inventory_in_location` WHERE status <> 'Completed' AND inventory_id = $inventory_id and location_id in ($ids)");
        $response['data']['remain_bins'] = $complete_bins[0]['remain_bins']?$complete_bins[0]['remain_bins']:0;

        //Complete Percent
        if($response['data']['total_bins']!=0)
            $response['data']['completed_percent'] = round(($response['data']['complete_bins']*100)/$response['data']['total_bins'],2);
        else
            $response['data']['completed_percent'] = 0;

        //Estimate time completion
        $complete_bins = $this->Curd->query("SELECT sum(estimated_bins_per_hours) as bins_time FROM `inventory_in_location` WHERE inventory_id = $inventory_id and location_id in ($ids)");
        $response['data']['bins_time'] = $complete_bins[0]['bins_time']*60;
/*
        //Count Audit Bins
        $audit_bins = $this->Curd->query("SELECT sum(estimated_bins) as completed_bins FROM `location` WHERE status = 'Audit' AND inventory_id = $inventory_id");
        $response['data']['complete_bins'] = $complete_bins[0]['completed_bins']?$complete_bins[0]['completed_bins']:0;
*/

        echo json_encode($response);
    }


    //Get Location with summary PrePI
    public function getLocationSummaryPrePI()
    {
        $data = $_POST;
        $this->load->model('Curd');

        $response = ['status'=>'ok','title'=>'Get Location','message'=>'Retrieve successfully'];

        if($data['area_id']!=""&&$data['area_id']!="none")
        {
            if(isset($data['status'])&&$data['status']!="all")
                $data['status'] = ' AND iol.status = "'.$data['status'].'"';
            else
                $data['status'] = '';

            if($data['area_id']=="all")
            {
                $response['data']['location'] = $this->query('select * from location as loc where loc.id in (select location_id from inventory_in_location where inventory_id = '.$data['inventory_id'].') '.$data['status']);
            }
            if(isset($data['module'])&&$data['module']=="pre"){
                $response['data']['location'] = $this->query('select * from location as loc where loc.area_id = '.$data['area_id'].' AND loc.id not in (select location_id from inventory_in_location where inventory_id = '.$data['inventory_id'].') '.$data['status']);
            }
            else{
                $response['data']['location'] = $this->query('select * from location as loc where loc.area_id = '.$data['area_id'].' AND loc.id in (select location_id from inventory_in_location where inventory_id = '.$data['inventory_id'].') '.$data['status']);
            }
            //echo json_encode($response['data']['location']);
            //exit;
        }else {
            if (isset($data['status']) && $data['status'] != "All")
                $data['status'] = ' AND iol.status = "' . $data['status'] . '"';
            else
                $data['status'] = '';
//            $response['data']['location'] = $this->Curd->getByPlainWhere('location','inventory_id = '.$data['inventory_id']);
            if ($data['area_id'] == "none")
            {
                $response['data']['location'] = $this->query('select * from inventory_in_location as iol left join location as loc on loc.id = iol.location_id where iol.inventory_id = '.$data['inventory_id']);
            }
            else
            {
                $response['data']['location'] = $this->query('select * from inventory_in_location as iol left join location as loc on loc.id = iol.location_id where iol.inventory_id = '.$data['inventory_id'].' AND loc.area_id = "'.$data['area_id'].'"');
            }

        }

        $inventory_id = $data['inventory_id'];

        //Count All
        $total = $this->Curd->query("SELECT count(loc.id) as total FROM inventory_in_location as iol left join`location` as loc on loc.id = iol.location_id WHERE iol.inventory_id = $inventory_id");
        $response['data']['total'] = $total[0]['total']?$total[0]['total']:0;

        //Count Completed
        $complete = $this->Curd->query("SELECT count(loc.id) as completed FROM  inventory_in_location as iol left join`location` as loc on loc.id = iol.location_id WHERE iol.status = 'Completed' AND iol.inventory_id = $inventory_id");
        $response['data']['completed'] = $complete[0]['completed']?$complete[0]['completed']:0;

        //Count Remaining
        $remaining = $this->Curd->query("SELECT count(loc.id) as remain FROM inventory_in_location as iol left join`location` as loc on loc.id = iol.location_id WHERE iol.status <> 'Completed' AND iol.inventory_id = $inventory_id");
        $response['data']['remain'] = $remaining[0]['remain']?$remaining[0]['remain']:0;

        //Count Total Bins
        $total_bins = $this->Curd->query("SELECT sum(iol.estimated_bins) as total_bins FROM inventory_in_location as iol left join`location` as loc on loc.id = iol.location_id WHERE iol.inventory_id = $inventory_id");
        $response['data']['total_bins'] = $total_bins[0]['total_bins']?$total_bins[0]['total_bins']:0;

        //Count Complete Bins
        $complete_bins = $this->Curd->query("SELECT sum(iol.estimated_bins) as completed_bins FROM inventory_in_location as iol left join`location` as loc on loc.id = iol.location_id WHERE iol.status = 'Completed' AND iol.inventory_id = $inventory_id");
        $response['data']['complete_bins'] = $complete_bins[0]['completed_bins']?$complete_bins[0]['completed_bins']:0;

        //Count Remain Bins
        $complete_bins = $this->Curd->query("SELECT sum(iol.estimated_bins) as remain_bins FROM inventory_in_location as iol left join`location` as loc on loc.id = iol.location_id WHERE iol.status <> 'Completed' AND iol.inventory_id = $inventory_id");
        $response['data']['remain_bins'] = $complete_bins[0]['remain_bins']?$complete_bins[0]['remain_bins']:0;

        //Complete Percent
        if($response['data']['total_bins']!=0)
            $response['data']['completed_percent'] = round(($response['data']['complete_bins']*100)/$response['data']['total_bins'],2);
        else
            $response['data']['completed_percent'] = 0;

        //Estimate time completion
        $complete_bins = $this->Curd->query("SELECT sum(iol.estimated_bins_per_hours) as bins_time FROM inventory_in_location as iol left join`location` as loc on loc.id = iol.location_id WHERE iol.inventory_id = $inventory_id");
        $response['data']['bins_time'] = $complete_bins[0]['bins_time']*60;
        
        

        
        
        /*
                //Count Audit Bins
                $audit_bins = $this->Curd->query("SELECT sum(estimated_bins) as completed_bins FROM `location` WHERE status = 'Audit' AND inventory_id = $inventory_id");
                $response['data']['complete_bins'] = $complete_bins[0]['completed_bins']?$complete_bins[0]['completed_bins']:0;
        */

        //Get Location
        $query = 'select * from location 
                  where 
                  id not in 
                  (select DISTINCT location_id from inventory_in_location where inventory_id = '.$data['inventory_id'].')';

        if($data['area_id']!="none"&&$data['area_id']!="all")
        {
            $query .= 'and 
                  area_id = '.$data["area_id"];
        }
        $locations = $this->query($query);

//        $response['data']['location'] = $locations;

        echo json_encode($response);
    }

    //Update Location
    public function updateLocation()
    {
        $data = $_POST;

        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Update Location','message'=>'Team added to location successfully'];

        $result = $this->Curd->updateByWhere("location",$data,['location_number'=>$data['location_number']]);

        $response['data'] = $result;

        echo json_encode($response);
    }

    //Update Location
    public function updateLocationPosition()
    {
        $data = $_POST;
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Update Location Position','message'=>'Location position updated successfully'];

        foreach ($data['data'] as $location)
        {
            $this->Curd->updateByWhere("location",$location,['id'=>$location['id']]);
        }

        echo json_encode($response);
    }
	
	//Update Location
    public function updateGAPosition()
    {
        $data = $_POST;
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Update General Area Position','message'=>'General Areas position updated successfully'];

        foreach ($data['data'] as $location)
        {
            $this->Curd->updateByWhere("general_area",$location,['id'=>$location['id']]);
        }

        echo json_encode($response);
    }

    //Update Location
    public function updateLocationStatus()
    {
        $data = $_POST;
        //Load Curd Model
        $this->load->model(["Curd",'Items_model','Inventory_model']);

        //Set Response
        $response = ['status'=>'ok','title'=>'Update Location','message'=>'Location status completed'];

        $loc = $this->Curd->getByWhere("location",['location_number'=>$data['location_number']]);

        $location = $this->Curd->getByWhere("inventory_in_location",['location_id'=>$loc[0]['id']]);

        $estimated_bins = $location[0]['estimated_bins'];

        $inventory_id = $location[0]['inventory_id'];

        $inventory = $this->Inventory_model->getByID($inventory_id);
        $audit = $inventory[0]['audit_percent'];

        $resultRow = $this->Items_model->countItem($inventory_id,$data['location_number']);

        $actual_bins = $resultRow[0]['actual_bins'];
        $count_percent = ($actual_bins*100)/$estimated_bins;

        if(isset($data['status'])&&$data['status']=='Completed')
        {
            $user_id = $this->data['user']['users_id'];
            $this->load->model('Work_status');
            $status = $this->Work_status->getByCondition(['counter_id'=>$user_id,'location_number'=>$data['location_number']]);
            if(count($status)>0) {
                $datetime1 = strtotime($status[0]['start_time']);
                $datetime2 = strtotime(date('Y-m-d h:i:s'));
                $interval = abs($datetime2 - $datetime1);
                $minutes = round($interval / 60);
                $data['auto_count_time_min'] = $minutes;
            }
        }

        if($audit!=$count_percent)
        {
            if(isset($data['status']))
            {
                $data['status'] = "Audit";
            }
        }
        
        unset($data['location_number']);
        $result = $this->Curd->updateByWhere("inventory_in_location",$data,['location_id'=>$loc[0]['id']]);
        $result = true;
        $response['data'] = $result;

        echo json_encode($response);
    }

    //Update Items
    public function updateItem()
    {
        $data = $_POST;
        //echo json_encode($data);
        //exit;
        //Load Curd Model
        $this->load->model("Curd");

        $condition = "none";

        if(isset($data['condition']))
        {
            $condition = $data['condition'];
        }

        $item = $data['item'];

        if($condition=="temp")
        {
            $row = $this->Curd->getByWhere('temp_items',['manufacture_item_number'=>$item['manufacture_item_number'],'inventory_id'=>$item['inventory_id']]);
        }
        else
        {
            $row = $this->Curd->getByWhere('items',['manufacture_item_number'=>$item['manufacture_item_number']]);
        }

            $row = $row[0];
            $location = $this->Curd->getByWhere('location',['location_number'=>$item['location_number']]);
            $location_id = $location[0]['id'];
            unset($row['id']);
            unset($row['location_number']);
            $row['location_id'] = $location_id;
            $row['purchase_unit_of_measure'] = $item['purchase_unit_of_measure'];
            $row['on_hand_quantity'] = $item['on_hand_quantity'];
            $row['expired_quantity'] = $item['expired_quantity'];


            $result = $this->Curd->insert('location_items',$row);

        //$item
        //Set Response
        $response = ['status'=>'ok','title'=>'Update Item','message'=>'Team added to location successfully'];
        

        //$result = $this->Curd->updateByWhere("items",$item,['id'=>$item['id']]);

        $response['data'] = $result;

        echo json_encode($response);
    }

    private function getLocationByInventory($table,$inventory_id)
    {
        //Load Curd Model
        $this->load->model("Curd");

        //Set Response
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];

        if($inventory_id>0)
        {
            $result = $this->Curd->getByWhere($table,
                [
                    'inventory_id'=>$inventory_id
                ]);

            $response['data'] = $result;
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    //Get Location details
    function getLocationDetail()
    {
        $this->load->model("Curd");
        $data = $_POST;
        //$data["location_number"] = "CEN-DEP-1";
        $location_number = $data["location_number"];
        $response = ['status'=>'ok','title'=>'Retrieve Detail','message'=>'Detail retrieve successfully'];
        $query = "select
                    inv.audit_percent,
                    loc.location_number,
                    loc.name as location,
                    area.name as area,
                    dept.name as dept,
                    iol.auto_count_time_min as min,
                    iol.consume,
                    iol.consigned,
                    iol.estimated_bins,
                    iol.estimated_bins_per_hours,
                    loc.team
                    from
                    location as loc
                    left join
                    inventory_in_location as iol
                    on loc.id = iol.location_id
                    left join inventory as inv on iol.inventory_id = inv.id
                    left join general_area as area on loc.area_id = area.id
                    left join departments as dept on area.dept_id = dept.id where loc.location_number = '$location_number'";

        $location = $this->Curd->query($query);
        if(count($location)>0)
        {
            $response['data'] = $location[0];
        }else
        {
            $response['status'] = 'fail';
            $response['message'] = 'No location found';
        }

        echo json_encode($response);
    }



    private function get($table)
    {
        $this->load->model("Curd");
        $response = ['status'=>'ok','title'=>'Retrieve','message'=>'Record retrieve successfully'];
        if(!isset($data['id']))
        {
            $result = $this->Curd->getAll($table);
            if(count($result)==0)
            {
                $response['status'] = 'fail';
                $response['message'] = 'Record not found';
            }
            else
            {
                $response['data'] = $result;
            }
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
        }
        return $response;
    }

    private function insert($table,$data)
    {
        $response = ['status'=>'ok','title'=>'Add','message'=>'Record added successfully'];
        $this->load->model(["Curd"]);
        $id = $this->Curd->insert($table,$data);
        $response['data'] = ['id'=>$id];
        return $response;
    }

    private function update($table,$data)
    {
        $this->load->model("Curd");
        $response = ['status'=>'ok','title'=>'Update','message'=>'Record update successfully'];
        if(isset($data['id']))
        {
            $response['result'] = $this->Curd->update($table,$data);
        }
        else
        {
            $response['status'] = 'fail';
            $response['message'] = 'Please select record';
            $response['result'] = false;
        }
        return $response;
    }

    private function delete($table,$id)
    {
        $response = ['status'=>'ok','title'=>'Remove','message'=>'Record deleted successfully'];
        $this->load->model(["Curd"]);
        $id = $this->Curd->delete($table,$id);
        return $response;
    }

    public function deleteLocationEdit()
    {
        $data = $_POST;
        if(!isset($data['inventory_id'])||$data['inventory_id']==""||!isset($data['location_id'])||$data['location_id']=="")
        {
            $response = ['status'=>'fail','title'=>'Error','message'=>'Please specify inventory and location'];
        }
        else
        {
            $response = $this->deleteByCondition('inventory_in_location',['location_id'=>$data['location_id'],'inventory_id'=>$data['inventory_id']]);
        }

        echo json_encode($response);
    }

    private function deleteByCondition($table,$condition)
    {
        $response = ['status'=>'ok','title'=>'Remove','message'=>'Record deleted successfully'];
        $this->load->model(["Curd"]);
        $id = $this->Curd->deleteByCondition($table,$condition);
        return $response;
    }

    public function countSheet()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];
        $temp = "none";

        if(isset($_POST['condition']['target']))
        {
            $temp = "temp";
        }

        $form_field = [
            ['label'=>'Manufacture Item No','field' => 'manufacture_item_number','attributes'=>['required'=>'required']],
            ['label'=>'Manufacture Name','field' => 'manufacture_name','attributes'=>['required'=>'required']],
            ['label'=>'Manufacture Item Description','field' => 'manufacture_item_description','attributes'=>['required'=>'required']],
            ['label'=>'Unit of Measurement','field' => 'low_unit_of_mesure','attributes'=>['required'=>'required']],
            ['label'=>'Packaging(e.g 20ES/CS)','field' => 'packing','attributes'=>['required'=>'required']],
            ['label'=>'ON Hand Quantity','field' => 'on_hand_quantity','attributes'=>['required'=>'required']],
            ['label'=>'Expired Quantity','field' => 'expired_quantity','attributes'=>['required'=>'required']],
            ['label'=>'Location Number','field' => 'location_number','attributes'=>['required'=>'required']]
        ];

        $load_data = [
            'type' => 'plainQuery',
            'primaryKey' => 'manufacture_item_number',
            'col' => [
                'manufacture_item_number',
                'manufacture_name',
                'manufacture_item_description',
                'low_unit_of_mesure',
                'packing',
                'on_hand_quantity',
                'expired_quantity'
            ],
            'table' => 'temp_items',
            'where' =>'id = 0'
        ];

        if($temp=="temp")
        {
            $load_data['table'] = "temp_items";
        }

        if(isset($data['condition']))
        {
            $load_data['where'] = "";
            if(isset($data['condition']['mfg_item_number']))
            {
                $id = preg_replace('/[^A-Za-z0-9]/', '', $data['condition']['mfg_item_number']);
                $load_data['where'] = "manufacture_item_number_nodash ='$id'";
            }else
                if(isset($data['condition']['multi_item_num'])&&$data['condition']['multi_item_num']!="")
            {
            $data['condition']['multi_item_num'] = preg_replace('/[^A-Za-z0-9]/', '', $data['condition']['multi_item_num']);
            $load_data['where'] .= "manufacture_item_number like '".$data['condition']['multi_item_num']."%' ";
            }

            if(isset($data['condition']['description'])&&$data['condition']['description']!="")
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";
                $load_data['where'] .= "manufacture_item_description like '%".$data['condition']['description']."%' ";
            }

            if(isset($data['condition']['vendor'])&&$data['condition']['vendor']!="")
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";

                $load_data['where'] .= "vendor_item_no like '".$data['condition']['vendor']."%' ";
            }
            
			if(isset($data['inventory_id']))
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";

                $load_data['where'] .= " inventory_id =".$data['inventory_id']." ";
            }
			
             if(isset($data['condition']['all']))
             {

             }
             else if(isset($data['condition']['location_number']))
             {
                 $num = $data['condition']['location_number'];
                 if($load_data['where']!="")
                     $load_data['where'] .= " AND ";
                 $load_data['where'] .= " location_number ='$num'";
                 if(isset($data['condition']['inventory_id']))
                 {
                     if($load_data['where']!="")
                         $load_data['where'] .= " AND ";

                    $inventory_id = $data['condition']['inventory_id'];
                     $load_data['where'] .= " inventory_id = ".$inventory_id;
                 }
             }
             else{
                 if($load_data['where']!="")
                     $load_data['where'] .= " AND ";
                 $load_data['where'] .= " (location_number is NULL or location_number = '')";
             }
        }

        $table = "items";
        if($temp=="temp")
        {
            $table = "temp_items";
        }
        


        if($temp=="temp") {
			$this->load->library('easy_curd',['table'=>$table,'load_data'=>$load_data,'columns'=>$form_field,'return'=>'temp']);        
			$response = $this->easy_curd->index($data);
		    foreach($response['data'] as $index=>$row)
                {
                    $var = $row[0];
                    $response['data'][$index][0] = $var.'<input type="hidden" value="temp" class="condition"/>';

                }
			echo json_encode($response);
			
        }
        else{
			$table = "temp_items";
			$this->load->library('easy_curd',['table'=>$table,'load_data'=>$load_data,'columns'=>$form_field,'return'=>'temp']);        
			$response = $this->easy_curd->index($data);
			if($response['recordsFiltered']=="0")
			{
				$response = $this->easy_curd->index(['return'=>'temp','table'=>'items']);
				echo json_encode($response);
			}
			else
			{
                //$response['return_condition'] = "temp";
                foreach($response['data'] as $index=>$row)
                {
                    $var = $row[0];
                    $response['data'][$index][0] = $var.'<input type="hidden" value="temp" class="condition"/>';

                }
				echo json_encode($response);
			}
			
			/*
			$this->load->library('easy_curd',['table'=>$table,'load_data'=>$load_data,'columns'=>$form_field]);
            $this->easy_curd->index($data);*/
        }
    }

    public function locationItemsTable()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $this->load->model('Curd');
        $location_id = '0';
        $inventory_id = '0';
        if(isset($data['condition']['location_number']))
        {
            $location = $this->Curd->getByWhere('location',['location_number'=>$data['condition']['location_number']]);
            $location_id = $location[0]['id'];
        }

        if(isset($data['condition']['inventory_id']))
        {
            $inventory_id = $data['condition']['inventory_id'];
        }

        $form_field = [
            ['label'=>'Manufacture Item No','field' => 'manufacture_item_number','attributes'=>['required'=>'required']],
            ['label'=>'Manufacture Name','field' => 'manufacture_name','attributes'=>['required'=>'required']],
            ['label'=>'Manufacture Item Description','field' => 'manufacture_item_description','attributes'=>['required'=>'required']],
            ['label'=>'Unit of Measurement','field' => 'low_unit_of_mesure','attributes'=>['required'=>'required']],
            ['label'=>'Packaging(e.g 20ES/CS)','field' => 'packing','attributes'=>['required'=>'required']],
            ['label'=>'ON Hand Quantity','field' => 'on_hand_quantity','attributes'=>['required'=>'required']],
            ['label'=>'Expired Quantity','field' => 'expired_quantity','attributes'=>['required'=>'required']]
        ];

        $load_data = [
            'type' => 'plainQuery',
            'primaryKey' => 'id',
            'col' => [
                'manufacture_item_number',
                'manufacture_name',
                'manufacture_item_description',
                'low_unit_of_mesure',
                'packing',
                'on_hand_quantity',
                'expired_quantity'
            ],
            'table' => 'location_items',
            'where' =>''
        ];

        $where = "";
        if($location_id!='0')
        {
            $where .= "location_id = ".$location_id;
        }

        if($inventory_id!='0')
        {
            if($where!="")
            {
                $where .= " AND ";
            }
            $where .= " inventory_id = ".$inventory_id;
        }
        $load_data['where'] = $where;

        $this->load->library('easy_curd',['table'=>'location_items','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }

    public function action()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Name','field' => 'name'],
            ['label'=>'Hospital',
                'field' => 'location_id',
                'relation'=>
                    [
                        'table'=>'location',
                        'key'=>'id',
                        'label'=>'name'
                    ]
            ]
        ];


        /*
        SELECT
        h.id,
        h.name,
        h.email,
        h.phone,
        (select username from users where userlvl = 2 and h.client_id = users_id) as client_id,
        (select username from users where userlvl = 3 and h.counter_id = users_id) as counter_id
        FROM `hospital` h
         *//*
        $load_data = [
            'type' => 'subQuery',
            'primaryKey' => 'h.id',
            'col' => [
                ['db'=>'h.id','name'=>'id'],
                ['db'=>'h.name','name'=>'name'],
                ['db'=>'h.email','name'=>'email'],
                ['db'=>'h.phone','name'=>'phone'],
                ['db'=>'(select username from users where userlvl = 2 and h.client_id = users_id) as client_id','name'=>'client_id'],
                ['db'=>'(select username from users where userlvl = 3 and h.counter_id = users_id) as counter_id','name'=>'counter_id']
            ],
            'table' => 'hospital as h',
            'where' =>''
        ];*/

        $load_data = [
            'type' => 'joinQuery',
            'primaryKey' => 'i.id',
            'col' => [
                'i.id',
                'i.pi_date',
                'h.name',
                'i.status'
            ],
            'table' => 'inventory i left join hospital h on i.hospital_id = h.id',
            'where' =>''
        ];

        if(isset($_POST['hospital_id'])&&$_POST['hospital_id']!=0)
        {
            $load_data['where'] = "i.hospital_id = ".$_POST['hospital_id'];
        }

        if( $this->data['user']['userlvl']==2 )
        {

        }
        else if( $this->data['user']['userlvl']==3 )
        {
            if(isset($_POST['action']))
            {
                $_POST['action']['edit'] = "false";
                $_POST['action']['delete'] = "false";
            }
        }

        if(isset($_POST['status']))
        {
            $status = $_POST['status'];
            if($status != "")
            {
                if($load_data['where']!="")
                    $load_data['where'] .= " AND ";
                $load_data['where'] .= " i.status = '$status'";
            }
        }

        $this->load->library('easy_curd',['table'=>'inventory','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }

    public function location()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Team','field' => 'team'],
            ['label'=>'Name','field' => 'name'],
            ['label'=>'General Area',
                'field' => 'area_id',
                'relation'=>
                    [
                        'table'=>'general_area',
                        'key'=>'id',
                        'label'=>'name'
                    ]
            ],
        ];

/*
select
loc.name,
loc.location_number,
loc.consigned,
loc.consume,
loc.estimated_bins,
loc.estimated_bins_per_hours,
loc.team,
loc.status,
dept.name as department,
area.name as area
from location as loc left join general_area as area on loc.area_id = area.id left join departments as dept on dept.id = area.dept_id
*/

        $load_data = [
            'type' => 'joinQuery',
            'primaryKey' => 'loc.id',
            'col' => [
                'loc.name',
                'loc.location_number',
                'iol.consigned',
                'iol.consume',
                'iol.estimated_bins',
                'iol.estimated_bins_per_hours',
                'loc.team',
                'iol.status'
            ],
            'table' => 'inventory_in_location as iol left join location as loc on iol.location_id=loc.id left join general_area as area on loc.area_id = area.id left join departments as dept on dept.id = area.dept_id',
            'where' =>''
        ];

        if(isset($_POST['hospital_id'])&&$_POST['hospital_id']!=0)
        {
            $load_data['where'] = "i.hospital_id = ".$_POST['hospital_id'];
        }

        if( $this->data['user']['userlvl']==2 )
        {

        }
        else if( $this->data['user']['userlvl']==3 )
        {
            if(isset($_POST['action']))
            {
                $_POST['action']['edit'] = "false";
                $_POST['action']['delete'] = "false";
            }
        }

        if(isset($_POST['condition']['status'])&&$_POST['condition']['status']!="all")
        {
            $status = $_POST['condition']['status'];
            if($status != "")
            {
                $load_data['where'] .= " iol.status = '$status'";
            }
        }

        if(isset($_POST['condition']['inventory_id']))
        {
            $inventory_id = $_POST['condition']['inventory_id'];
            if($inventory_id != "")
            {
                if($load_data['where']!="")
                {
                    $load_data['where'] .= ' AND ';
                }
                $load_data['where'] .= " iol.inventory_id = '$inventory_id'";
            }
        }

        $dept_id = "";
        
        if(isset($_POST['condition']['dept_id']))
        {
            $dept_id = $_POST['condition']['dept_id'];
            if($dept_id != "")
            {
                if($load_data['where']!="")
                {
                    $load_data['where'] .= ' AND ';
                }
                $load_data['where'] .= " dept.id = '$dept_id'";
            }
        }

        $area_id = "";
        
        if(isset($_POST['condition']['area_id'])&&$_POST['condition']['area_id']!="all")
        {
            $area_id = $_POST['condition']['area_id'];
            if($area_id != "")
            {
                if($load_data['where']!="")
                {
                    $load_data['where'] .= ' AND ';
                }
                $load_data['where'] .= " area.id = '$area_id'";
            }
        }

        if(isset($_POST['condition']['hospital_id']))
        {
            $hospital_id = $_POST['condition']['hospital_id'];
            if($hospital_id != "")
            {
                if($load_data['where']!="")
                {
                    $load_data['where'] .= ' AND ';
                }
                $load_data['where'] .= " dept.hospital_id = '$hospital_id'";
            }
        }

        $this->load->library('easy_curd',['table'=>'location','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }
	
	
	    //Export Pre PI Module
    public function exportTemplate()
    {
        $fileName = 'template.csv';
        $header = [
            'COUNTSHEET CONTROL NUMBER',
            'LOCATION NUMBER',
            'BIN (SHELF)',
            'MANUFACTURER ITEM NUMBER',
            'MANUFACTURER ITEM NODASH',
            'MANUFACTURER NAME',
            'MANUFACTURE ITEM DESCRIPTION',
            'PACKAGING',
            'ON HAND QTY',
            'ON HAND UNIT OF MEASURE (CS,BX,EA)',
            'EXPIRED ON HAND QTY',
            'HOSPITAL ITEM NUMBER',
            'PURCHASE UNIT OF MEASURE',
            'LOW UNIT OF MEASURE',
            'EFFECTIVE UOM TO LUOM FACTOR',
            'ON HAND UOM TO LUOM FACTOR',
            'STOCK_YESTNO',
            'EFFECTIVE UNIT OF MEASURE PRICE',
            'LOUM PRICE',
            'CONSIGNMENT FLAG',
            '90 DIOH FLAG',
			'VENDOR NAME',
			'VENDOR ITEM NO',
			'ITEM SOURCE',
			'PRICING SOURCE',
			'PACKAGING SOURCE',
			'PRICE MATCH',
			'PI ALIGN',
			'ORIGINALHOSPITALMFGIDNODASH',
			'ORIGINALHOSPITALVENDORITEMNODASH'
        ];

        $data[] = $header;

        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header('Content-Description: File Transfer');
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$fileName}");
        header("Expires: 0");
        header("Pragma: public");

        $fp = @fopen( 'php://output', 'w' );

        foreach ($data as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }
}