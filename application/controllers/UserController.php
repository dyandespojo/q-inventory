<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller
{

    private $data;
    private $js = 0;
    private $css = 0;

    public function __construct()
    {
        parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }

        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        $user = $this->data['user'];

        if($user['userlvl']==3)
        {
            redirect("/counter");
        }
        else if($user['userlvl']==2)
        {
            redirect("/client");
        }

        //$this->data['css'][$this->css++] = "assets/css/custom/internal_user.css";
        $this->data['js'][$this->js++] =  base_url("assets/js/easy_curd.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/custom/user.js");
    }

    public function index()
    {
        $this->load->view('admin/user/index', $this->data);
    }


    public function action()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];

        $form_field = [
            ['label'=>'Username','field' => 'username'],
            ['label'=>'First Name','field' => 'firstname'],
            ['label'=>'Last Name','field' => 'secondname'],
            ['label'=>'Email','field' => 'email'],
            ['label'=>'User Level','field' => 'userlvl'],
            ['label'=>'Blocked','field' => 'accountblocked'],
            ['label'=>'Licensed','field' => 'licensed'],
            /*
            userlvl
            ['label'=>'Internal Project ID',
                'field' => 'internal_project_id',
                'relation'=>
                    [
                        'table'=>'projects_table',
                        'key'=>'internal_project_id',
                        'label'=>'internal_project_id'
                    ]
            ],
            ['label'=>'Customer Name Prefix',
                'field' => 'customer_name_prefix',
                'relation'=>
                    [
                        'table'=>'customer_table',
                        'key'=>'name_prefix',
                        'label'=>'name_full'
                    ]
            ],
            ['label'=>'Cust Site Class','field' => 'cust_site_class'],
            ['label'=>'Audit Status','field' => 'audit_status']*/
        ];

//accountblocked
        $load_data = [
            'type' => 'plainQuery',
            'primaryKey' => 'users_id',
            'col' => [
                'users_id',
                'username',
                'firstname',
                'secondname',
                'email',
                'userlvl',
                'accountblocked',
                'licensed'
            ],
            'table' => 'users',
            'where' =>''
        ];

        /*
        $load_data = [
            'type' => 'joinQuery',
            'primaryKey' => 'cs.id',
            'col' => [
                'cs.site_id',
//                'cs.id',
                'cs.site_region',
                'cs.site_sub_region',
                'cs.site_cluster_id',
                'cs.site_sub_cluster_id',
                'at.audit_team_id'
            ],
            'table' => 'customer_site_table as cs left join audit_team_table as at on cs.audit_team_id = at.id',
            'where' =>'cs.internal_project_id = "ISAT-MPFS001"'/*
                        AND (cs.audit_status = "Not Checked" OR cs.audit_status = "Completed")
                        AND approver_1 = "Not Checked"
                        AND approver_2 = "Not Checked"
                        AND approver_3 = "Not Checked"
                        AND approver_4 = "Not Checked"'*/
        /*];*/

        $this->load->library('easy_curd',['table'=>'users','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }


}