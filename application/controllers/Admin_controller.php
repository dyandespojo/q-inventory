<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller 
{
    private $data;
	public function __construct()
	{
		parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }
        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        $user = $this->data['user'];
        if($user['userlvl']==3)
        {
            redirect("/counter");
        }
        else if($user['userlvl']==2)
        {
            redirect("/client");
        }
    }
            
	public function index()
	{
	 $this->load->view('admin/main',$this->data);
	}             
}