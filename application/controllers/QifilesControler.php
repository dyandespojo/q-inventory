<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QifilesControler extends CI_Controller
{

    private $data;
    private $js = 0;
    private $css = 0;

    public function __construct()
    {
        parent::__construct();
        $logged_in = $this->session->has_userdata('logged_in');
        if(!$logged_in)
        {
            redirect("/");
        }

        $username = $this->session->userdata('logged_in')['username'];
        $this->load->model('Um_users_model');
        $this->data['user'] = $this->Um_users_model->get_level_by_username($username)[0];
        $user = $this->data['user'];
        $this->data['fileuploadstatus']="";
       
        if($user['userlvl']==3)
        {
            redirect("/counter");
        }
         $this->load->model('Gernal_model');    
         if($user['userlvl']==2)
        {
            $this->data['hospitals_list']=$this->Gernal_model->get_client_hospital($this->Um_users_model->get_id_by_username($username));
        }
        else
        {
           $this->data['hospitals_list']=$this->Gernal_model->get_all_data('hospital');  
        }
            
       /* else if($user['userlvl']==2)
        {
            redirect("/client");
        }
*/
        //$this->data['css'][$this->css++] = "assets/css/custom/internal_user.css";
        $this->data['js'][$this->js++] =  base_url("assets/js/easy_curd.js");
        $this->data['js'][$this->js++] =  base_url("assets/js/custom/templatefiles.js");
    }

    public function index()
    {
       $this->load->view('admin/qiupload/index', $this->data);
    }


    public function action()
    {
        if(isset($_POST))
            $data = $_POST;
        else
            $data = [];
             $form_field = [
            ['label'=>'File Name','field' => 'filename'],
            ];
        $load_data = [
            'type' => 'plainQuery',
            'primaryKey' => 'id',
            'col' => [
                'id',
                'filename',
                'fileurl',
            ],
            'table' => 'qitemplatefiles',
            'where' =>''
        ];
          $this->load->library('easy_curd',['table'=>'qitemplatefiles','load_data'=>$load_data,'columns'=>$form_field]);
        $this->easy_curd->index($data);
    }
    
    function do_upload()
	{
		$config['upload_path'] = './templatefiles/';
		$config['max_size']	= '1000';
        $config['allowed_types'] = 'csv|gif|jpg|png';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
            $this->data['fileuploadstatus']=$error['error'];
            $this->load->view('admin/filesupload/index', $this->data);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
            $this->load->library('csvimport');
            $result = $this->csvimport->get_array('./templatefiles/'.$data['upload_data']['file_name']);
                foreach($result as $item)
                {

                    $items = [];
                    $items = [
                        'countsheet_control_number'=>$item['COUNTSHEET CONTROL NUMBER'],
                        'location_number'=>$item['LOCATION NUMBER'],
                        'bin'=>$item['BIN (SHELF)'],
                        'manufacture_item_number'=>$item['MANUFACTURER ITEM NUMBER'],
                        'manufacture_item_number_nodash'=>$item['MANUFACTURER ITEM NODASH'],
                        'manufacture_name'=>$item['MANUFACTURER NAME'],
                        'manufacture_item_description'=>$item['MANUFACTURE ITEM DESCRIPTION'],
                        'on_hand_qty'=>$item['ON HAND QTY'],
                        'on_hand_unit_of_measure'=>$item['ON HAND UNIT OF MEASURE (CS,BX,EA)'],
                        'expired_quantity'=>$item['EXPIRED ON HAND QTY'],
                        'hospital_item_number'=>$item['HOSPITAL ITEM NUMBER'],
                        'effective_uom_to_luom_factor'=>$item['EFFECTIVE UOM TO LUOM FACTOR'],
                        'on_hand_uom_to_luom_factor'=>$item['ON HAND UOM TO LUOM FACTOR'],
                        'stock_yestno'=>$item['STOCK_YESTNO'],
                        'effective_unit_of_measure_price'=>$item['EFFECTIVE UNIT OF MEASURE PRICE'],
                        'consigned'=>$item['CONSIGNMENT FLAG'],
                        'dioh_flag'=>$item['90 DIOH FLAG'],
                        'low_unit_of_mesure_price'=>$item['LOUM PRICE'],
                        'item_source'=>$item['ITEM SOURCE'],
                        'price_source'=>$item['PRICING SOURCE'],
                        'packing_soucre'=>$item['PACKAGING SOURCE'],//
                        'vendor_item_no'=>$item['VENDOR ITEM NO'],
                        'price_match'=>$item['PRICE MATCH'],
                        'pi_align'=>$item['PI ALIGN'],
                        'original_hospital_mgf_id_no_dash'=>$item['ORIGINALHOSPITALMFGIDNODASH'],
                        'original_hospital_vendor_item_no_dash'=>$item['ORIGINALHOSPITALVENDORITEMNODASH'],
                        'packing'=>$item['PACKAGING'],
                        'purchase_unit_of_measure'=>$item['PURCHASE UNIT OF MEASURE'],
                        'effective_uom_to_luom_factor'=>$item['EFFECTIVE UOM TO LUOM FACTOR'],
                        'consume'=>$item['CONSUME'],
                        
                    ];

                    
                      $this->load->model('Gernal_model');
                    if($this->Gernal_model->item_exist('templateitems','countsheet_control_number',$items['countsheet_control_number']))
                    {
                        
                             $this->Gernal_model->update_data('templateitems','countsheet_control_number',$items['countsheet_control_number'],$items);
                    }
                    else
                    {
                        $this->Gernal_model->add_table_data('templateitems',$items);
                    }
                }
            $this->data['fileuploadstatus']='File Uploded';
           $this->load->view('admin/qiupload/index', $this->data);
		}
	}


}