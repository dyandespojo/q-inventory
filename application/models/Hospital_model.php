<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hospital_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    
    public function get_all_hospital()
    {
		$this->db->select('*');
		$query= $this->db->get('hospital');
		return $query->result_array();
	}
       public function get_client_hospital($cientID)
    {
		$this->db->select('*');
		$this->db->where('client_id = "'.$cientID.'"');
        $query= $this->db->get('hospital');
		return $query->result_array();
	}
        public function get_counter_hospital($cientID)
    {
		$this->db->select('*');
		$this->db->where('counter_id = "'.$cientID.'"');
        $query= $this->db->get('hospital');
		return $query->result_array();
	}

    public function getByID($id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        $query= $this->db->get('hospital');
        return $query->result_array();
    }

    public function update($id,$data)
    {
        $this->db->where("id",$id);
        $query = $this->db->update('hospital',$data);
        return $query;
    }
 
}