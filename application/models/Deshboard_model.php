<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deshboard_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }
    
    public function total_value($dept_id)
    {
		 $sql_query="SELECT sum(effective_unit_of_measure_price) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id."))";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(effective_unit_of_measure_price)'];
	}
    
    public function total_main($dept_id)
    {
		 $sql_query="SELECT sum(effective_unit_of_measure_price) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consume='main'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(effective_unit_of_measure_price)'];
	}
    public function total_pou($dept_id)
    {
		 $sql_query="SELECT sum(effective_unit_of_measure_price) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.") ) AND consume='POU'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(effective_unit_of_measure_price)'];
	}
     public function total_owned($dept_id)
    {
		 $sql_query="SELECT sum(effective_unit_of_measure_price) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consigned='owned'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(effective_unit_of_measure_price)'];
	}
        public function total_consigned($dept_id)
    {
		 $sql_query="SELECT sum(effective_unit_of_measure_price) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consigned='consigned'";
         $query = $this->db->query($sql_query);
        $query_result=$query->result_array();
         return $query_result[0]['sum(effective_unit_of_measure_price)'];
	}
        public function total_match($dept_id)
    {
         $sql_query="SELECT count(effective_unit_of_measure_price) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND effective_unit_of_measure_price>0";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['count(effective_unit_of_measure_price)'];
	}
        public function total_unmatch($dept_id)
    {
            $sql_query="SELECT count(effective_unit_of_measure_price) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND effective_unit_of_measure_price=0";
            $query = $this->db->query($sql_query);
        $query_result=$query->result_array();
         return $query_result[0]['count(effective_unit_of_measure_price)'];
	}
     public function on_hand($dept_id)
    {
		 $sql_query="SELECT sum(on_hand_qty) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id."))";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(on_hand_qty)'];
	}
        public function on_hand_main($dept_id)
    {
		 $sql_query="SELECT sum(on_hand_qty) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consume='main'";
         $query = $this->db->query($sql_query);
        $query_result=$query->result_array();
         return $query_result[0]['sum(on_hand_qty)'];
	}
    public function on_hand_pou($dept_id)
    {
		 $sql_query="SELECT sum(on_hand_qty) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consume='POU'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(on_hand_qty)'];
	}
    public function on_hand_owned($dept_id)
    {
		 $sql_query="SELECT sum(on_hand_qty) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consigned='owned'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(on_hand_qty)'];
	}
        public function on_hand_consigned($dept_id)
    {
		 $sql_query="SELECT sum(on_hand_qty) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consigned='consigned'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(on_hand_qty)'];
	}
        public function on_hand_match($dept_id)
    {
         $sql_query="SELECT count(on_hand_qty) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND effective_unit_of_measure_price>0";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['count(on_hand_qty)'];
	}
        public function on_hand_unmatch($dept_id)
    {
           $sql_query="SELECT count(on_hand_qty) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND effective_unit_of_measure_price=0";
            $query = $this->db->query($sql_query);
        $query_result=$query->result_array();
         return $query_result[0]['count(on_hand_qty)'];
	}
         public function expired($dept_id)
    {
		 $sql_query="SELECT sum(expired_quantity) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id."))";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(expired_quantity)'];
	}
        public function expired_main($dept_id)
    {
		 $sql_query="SELECT sum(expired_quantity) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consume='main'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(expired_quantity)'];
	}
    public function expired_pou($dept_id)
    {
		 $sql_query="SELECT sum(expired_quantity) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consume='POU'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(expired_quantity)'];
	}
    public function expired_owned($dept_id)
    {
		 $sql_query="SELECT sum(expired_quantity) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.") ) AND consigned='owned'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(expired_quantity)'];
	}
        public function expired_consigned($dept_id)
    {
		 $sql_query="SELECT sum(expired_quantity) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND consigned='consigned'";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['sum(expired_quantity)'];
	}
              public function expired_match($dept_id)
    {
         $sql_query="SELECT count(expired_quantity) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND effective_unit_of_measure_price>0";
         $query = $this->db->query($sql_query);
         $query_result=$query->result_array();
         return $query_result[0]['count(expired_quantity)'];
	}
        public function expired_unmatch($dept_id)
    {
        $sql_query="SELECT count(expired_quantity) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND effective_unit_of_measure_price=0";
            $query = $this->db->query($sql_query);
        $query_result=$query->result_array();
         return $query_result[0]['count(expired_quantity)'];
	}
    
    public function all_supplier($dept_id)
    {
        $sql_query="SELECT `manufacture_name` as name ,sum(effective_unit_of_measure_price) as y FROM `templateitems`  where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) GROUP BY `manufacture_name`";
        $query = $this->db->query($sql_query);
        $query_result=$query->result_array();
         return $query_result;
	}
    
    public function top_supplier($dept_id,$numberOfSupplier)
    {
       $sql_query="SELECT `manufacture_name` as name ,sum(effective_unit_of_measure_price) as y FROM `templateitems`  where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) GROUP BY `manufacture_name` ASC LIMIT ".$numberOfSupplier;
        $query = $this->db->query($sql_query);
        $query_result=$query->result_array();
         return $query_result;
	}
    
    public function singal_supplier($dept_id,$singal_supplier)
    {
        $sql_query="SELECT count(expired_quantity) FROM `templateitems` where location_number IN (select location_number from location where area_id IN (select id from general_area where dept_id=".$dept_id.")) AND effective_unit_of_measure_price=0";
            $query = $this->db->query($sql_query);
        $query_result=$query->result_array();
         return $query_result[0]['count(expired_quantity)'];
	}
}