<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location_model extends CI_Model {
    public $table = "location";
    public $key = "id";
    function __construct(){
        parent::__construct();
    }
    
    public function getAll()
    {
		$this->db->select('*');
		$query= $this->db->get($this->table);
		return $query->result_array();
	}

    public function getByID($id)
    {
        $this->db->select('*');
        $this->db->where($this->key,$id);
        $query= $this->db->get($this->table);
        return $query->result_array();
    }


    public function getByCondition($data)
    {
        $this->db->select('*');
        foreach($data as $key => $value)
        {
            $this->db->where($key,$value);
        }

        $query= $this->db->get($this->table);
        return $query->result_array();
    }

    public function getByArea($id)
    {
        $this->db->select('*');
        $this->db->where("area_id",$id);
        $query= $this->db->get($this->table);
        return $query->result_array();
    }

    function getLastId()
    {
        $this->db->select_max('id');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function update($id,$data)
    {
        $this->db->where("id",$id);
        $query = $this->db->update('location',$data);
        return $query;
    }

    public function insert($data)
    {
        $query= $this->db->insert('location',$data);

        return $this->db->insert_id();
    }
}