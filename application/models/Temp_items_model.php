<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class temp_items_model extends CI_Model {

    private $table = "temp_items";
    private $key = "id";
    function __construct(){
        parent::__construct();
    }


    public function getAll()
    {
		$this->db->select('*');
		$query= $this->db->get($this->table);
		return $query->result_array();
	}

    public function getByID($id)
    {
		$this->db->select('*');
		$this->db->where($this->key,$id);
        $query = $this->db->get($this->table);
		return $query->result_array();
	}

    public function getByMft($id)
    {
        $this->db->select('*');
        $this->db->where("manufacture_item_number",$id);
        $query= $this->db->get($this->table);
        return $query->result_array();
    }

    public function updateMft($data)
    {
        $this->db->where("manufacture_item_number",$data['manufacture_item_number']);
        $query = $this->db->update($this->table,$data);
        return $query;
    }

    public function getByCondition($data)
    {
        $this->db->select('*');
        foreach($data as $key => $value)
        {
            $this->db->where($key,$value);
        }

        $query= $this->db->get($this->table);
        return $query->result_array();
    }

    public function getByLike($data)
    {
        $this->db->select('*');
        foreach($data as $key => $value)
        {
            if($key == "manufacture_item_description")
            {
                $this->db->where($key.' like "%'.$value.'%"');
            }
            else
            {
                $this->db->where($key.' like "'.$value.'%"');
            }
        }

        $query= $this->db->get($this->table);
        return $query->result_array();
    }

    public function insertGetID($data)
    {
        $query= $this->db->insert($this->table,$data);

		return $this->db->insert_id();
	}

    public function insertItem($data)
    {
        $query= $this->db->insert($this->table,$data);

        return $this->db->insert_id();
    }

    public function update($id,$data)
    {
        $this->db->where($this->key,$id);
        $query = $this->db->update($this->table,$data);
        return $query;
    }

    public function deleteItem($id)
    {
        $sql_query="DELETE from temp_items where inventory_id = $id";
        $query = $this->db->query($sql_query);
    }

    public function clear()
    {
        $sql_query="DELETE from temp_items";
        $query = $this->db->query($sql_query);
    }

    public function countItem($inventory_id,$loction_number)
    {
        $sql_query='SELECT count(*) as actual_bins from temp_items where inventory_id = '.$inventory_id.' and location_number = "'.$loction_number.'"';
        $query = $this->db->query($sql_query);
        return $query->result_array();
    }
 
}