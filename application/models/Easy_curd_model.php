<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Easy_curd_model extends CI_Model {

    private $key = "none";
    private $table = "none";

    function __construct(){
        parent::__construct();
    }

    function set($table)
    {
        $this->table = $table;

        $fields = $this->db->field_data($table);

        foreach ($fields as $field)
        {
            if($field->primary_key == 1)
            {
                $this->key = $field->name;
            }
        }
    }

    function getFieldDetail($fields)
    {
        $response = [];
        foreach($fields as $field)
        {
            $data = [];
            $data['label'] = $field['label'];
            $data['name'] = $field['field'];

            $type = $this->gettype($field['field']);

            if(isset($field['attributes']))
            {
                foreach($field['attributes'] as $key=>$value)
                {
                    $data['attributes'][] = ['key'=>$key,'value'=>$value];
                }
            }
            if(substr($type,0,4)=='enum')
            {
                $data['type'] = 'select';
                $option = $this->get_enum_values($field['field']);
                foreach($option as $item)
                {
                    $data['option'][] = ['label'=>$item,'value'=>$item];
                }
            }
            else if(isset($field['relation']))
            {
                $where = "";
                if(isset($field['relation']['where']))
                {
                    $where = $field['relation']['where'];
                }
                $values = $this->getValues($field['relation']['key'].' as key,'.$field['relation']['label'].' as label',$field['relation']['table'],$where);

                $data['type'] = 'select';

                foreach($values as $item)
                {
                    $data['option'][] = ['label'=>$item['label'],'value'=>$item['key']];
                }

            }
            else
            {
                $data['type'] = 'input';
            }

            array_push($response,$data);
        }
        return $response;
    }

    function getValues($field,$table,$where = "")
    {
        if($where!="")
        {
            $this->db->where($where);
        }
        $this->db->select($field);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    function getType( $field )
    {
    	//echo $this->table."  ".$field."<br>";
        $type = $this->db->query( "SHOW COLUMNS FROM {$this->table} WHERE Field = '{$field}'" )->row( 0 )->Type;
        return $type;
    }

    function get_enum_values( $field )
    {
        $type = $this->db->query( "SHOW COLUMNS FROM {$this->table} WHERE Field = '{$field}'" )->row( 0 )->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    function setTable($table)
    {
        $this->table = $table;
    }

    function setKey($key)
    {
        $this->key = $key;
    }

    function getTable($table)
    {
        return $this->table;
    }

    function getKey($key)
    {
        return $this->key;
    }

    function getById($id)
    {
        $this->db->where($this->key,$id);
        $this->db->select("*");
        $query = $this->db->get($this->table);
        $result = $query->result_array();
        if(count($result)>0)
        {
            $result = $result[0];
        }
        return $result;
    }

    function getAll()
    {
        $this->db->select("*");
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    function insert($data)
    {
        return $this->db->insert($this->table,$data);
    }

    function update($id,$data)
    {
        $this->db->where($this->key,$id);
        return $this->db->update($this->table,$data);
    }

    function getByWhere($select,$condition)
    {
        $this->db->select($select);
        $this->db->where($condition);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    function delete($id)
    {
        $this->db->where($this->key,$id);
        return $this->db->delete($this->table);
    }
}
?>