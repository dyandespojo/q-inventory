<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_model extends CI_Model {
    public $table = "departments";
    public $key = "id";
    function __construct(){
        parent::__construct();
    }
    
    public function getAll()
    {
		$this->db->select('*');
		$query= $this->db->get($this->table);
		return $query->result_array();
	}

    public function getByHospital($id)
    {
        $this->db->select('*');
        $this->db->where("hospital_id",$id);
        $query= $this->db->get($this->table);
        return $query->result_array();
    }


    public function getByID($id)
    {
        $this->db->select('*');
        $this->db->where($this->key,$id);
        $query= $this->db->get($this->table);
        return $query->result_array();
    }

    public function insert($data)
    {
        $query = $this->db->insert($this->table,$data);
        return $this->db->insert_id();
    }

    public function update($id,$data)
    {
        $this->db->where("id",$id);
        $query = $this->db->update($this->table,$data);
        return $query;
    }
 
}