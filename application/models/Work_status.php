<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Work_status extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    public function getAll()
    {
		$this->db->select('*');
		$query= $this->db->get('count_work_status');
		return $query->result_array();
	}

    public function getByUserID($user_id)
    {
        $this->db->select('*');
        $this->db->where('counter_id',$user_id);
        $query= $this->db->get('count_work_status');
        return $query->result_array();
    }

    public function getByCondition($condition)
    {
        $this->db->select('*');
        foreach($condition as $key => $value)
        {
            $this->db->where($key,$value);
        }
        $query= $this->db->get('count_work_status');
        return $query->result_array();
    }

    public function getByID($id)
    {
		$this->db->select('*');
		$this->db->where('id',$id);
        $query= $this->db->get('count_work_status');
		return $query->result_array();
	}

    public function insert($data)
    {
        $query= $this->db->insert('count_work_status',$data);

		return $this->db->insert_id();
	}

    public function delete($id)
    {
        $this->db->where("id",$id);
        $query = $this->db->delete('count_work_status');
    }

    public function update($id,$data)
    {
        $this->db->where("id",$id);
        $query = $this->db->update('count_work_status',$data);
        return $query;
    }
 
 
}