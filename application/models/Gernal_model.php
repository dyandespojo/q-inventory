<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gernal_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    
    function add_table_data($table,$dbdata)
    {
		return $this->db->insert($table, $dbdata);
	}
    
    
    public function get_all_data($table)
    {
		$this->db->select('*');
		$query= $this->db->get($table);
		return $query->result_array();
	}
       public function get_client_hospital($cientID)
    {
		$this->db->select('*');
		$this->db->where('client_id = "'.$cientID.'"');
        $query= $this->db->get('hospital');
		return $query->result_array();
	}
        public function get_counter_hospital($cientID)
    {
		$this->db->select('*');
		$this->db->where('counter_id = "'.$cientID.'"');
        $query= $this->db->get('hospital');
		return $query->result_array();
	}
    
    public function get_data_where($table,$field,$condition)
    {
		$this->db->select('*');
        $this->db->where($field.' = "'.$condition.'"');
		$query= $this->db->get($table);
        return $query->result_array();
	}
    public function delete_table_data($table,$field,$condition)
    {
		$this->db->select('*');
        $this->db->where($field.' = "'.$condition.'"');
        $this->db->delete($table);
	}
        public function item_exist($table,$item_field_name,$item)
    {
        	$this->db->select($item_field_name);
		$this->db->where($item_field_name.'="'.$item.'"');
		$query=$this->db->get($table);
		if($query -> num_rows()> 0){
			return true;
		}else{
			return false;
		}
    }
    public function update_data($table,$item_field_name,$item,$data)
    {
        $this->db->where($item_field_name.'="'.$item.'"');
		return $this->db->update($table,$data);
    }
    	

 
 
}