<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Map_model extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    
    public function getAll()
    {
		$this->db->select('*');
		$query= $this->db->get('map');
		return $query->result_array();
	}

    public function getByID($id)
    {
		$this->db->select('*');
		$this->db->where('id',$id);
        $query= $this->db->get('map');
		return $query->result_array();
	}

    public function getByLocationID($id)
    {
        $this->db->select('*');
        $this->db->where('location_id',$id);
        $query= $this->db->get('map');

        return $query->result_array();
    }

    public function insert($data)
    {
        $query= $this->db->insert('map',$data);

		return $this->db->insert_id();
	}

    public function update($id,$data)
    {
        $this->db->where("id",$id);
        $query = $this->db->update('map',$data);
        return $query;
    }

    public function delete($id)
    {
        $this->db->where("id",$id);
        $query = $this->db->delete("map");
    }
}