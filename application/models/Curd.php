<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curd extends CI_Model {

    private $table;
    private $key;

    function __construct(){
        parent::__construct();
    }

    public function getAll($table)
    {
        $this->db->select('*');
        $query= $this->db->get($table);
        return $query->result_array();
    }

    public function getByID($table,$id)
    {
        $this->db->select('*');
        $this->db->where('id',$id);
        $query= $this->db->get($table);
        return $query->result_array();
    }

    public function getByWhere($table,$condition)
    {
        $this->db->select('*');
        foreach($condition as $key => $value)
        {
            $this->db->where($key,$value);
        }
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function getByPlainWhere($table,$condition)
    {
        $this->db->select('*');
        $this->db->where($condition);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    function insert($table,$data)
    {
        $query = $this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    function update($table,$data)
    {
        $this->db->where("id",$data['id']);
        $query = $this->db->update($table,$data);
        return $query;
    }

    function updateByWhere($table,$data,$condition)
    {
        foreach($condition as $key=>$value)
        {
            $this->db->where($key,$value);
        }

        $query = $this->db->update($table,$data);
        return $query;
    }

    function delete($table,$id)
    {
        $this->db->where("id",$id);
        $query = $this->db->delete($table);
    }

    function deleteByCondition($table,$condition)
    {
        $this->db->where($condition);
        $query = $this->db->delete($table);
    }

    function max($table)
    {
        $this->db->select_max('id');
        $query = $this->db->get($table);
        return $query->result_array();
    }

    function query($query)
    {
        $query = $this->db->query($query);
        return $query->result_array();
    }
}