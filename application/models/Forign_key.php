<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forign_key extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    
    public function get_all_data($table,$field="*")
    {
            	$this->db->select($field);
		$query= $this->db->get($table);
		return $query->result_array();
	}
    
    public function get_where_data($table,$check_field,$check_value,$field="*")
    {
            	$this->db->select($field);
        $this->db->where($check_field.'= "'.$check_value.'"');
		$query= $this->db->get($table);
		return $query->result_array();
	}
    
    public function get_where_condition_data($table,$condition,$field="*")
    {
            	$this->db->select($field);
        $this->db->where($condition);
		$query= $this->db->get($table);
		return $query->result_array();
	}
    public function get_where_like_data($table,$check_field,$check_value,$likefield,$likevalue,$field="*")
    {
            	$this->db->select($field);
        $this->db->where($check_field.'= "'.$check_value.'"');
        $this->db->like($likefield, $likevalue);
		$query= $this->db->get($table);
		return $query->result_array();
	}

 
    public function add_data($table,$dbdata){
		return $this->db->insert($table, $dbdata);
	}
     
    
    public function update_data($table,$field,$field_value,$dbdata){
		$this->db->where($field.'= "'.$field_value.'"');
		return $this->db->update($table, $dbdata);
	}
    function get_data_field($table,$field,$condition)
    {
         	$this->db->select($field);
        $this->db->where($condition);
		$query= $this->db->get($table);
		$query=$query->row_array(); 
		return $query[$field];
    }
    function get_group_by_data($table,$field,$condition,$group_by)
    {
	    $this->db->select($field);
        $this->db->where($condition);
        $this->db->group_by($group_by); 
		$query= $this->db->get($table);
		return $query->result_array();
        
    /*SELECT COUNT( internal_project_id ) , site_region
FROM  `customer_site_table` 
GROUP BY site_region*/
    }
    function get_count_data_field($table,$condition)
    {
        $this->db->where($condition);
        $this->db->from($table);
        return $this->db->count_all_results();

    }
    
    
   function project_team($id)
    {
      $sql_query="SELECT * FROM `internal_project_team_to_project_table` as ipt left join internal_project_team_table as ip on ipt.internal_project_team_id = ip.id where ipt.internal_project_id =".$id;
            $query = $this->db->query($sql_query);
       return $query->result_array();
    }
   function project_company($id)
    {
        $sql_query="SELECT * FROM `project_to_audit_companies_table` as ipt left join audit_company_table as ip on ipt.audit_company_name_prefix = ip.id where ipt.internal_project_id =".$id;
            $query = $this->db->query($sql_query);
       return $query->result_array();
    }
 
}