    <nav id="myNav" class="navbar navbar-default ">
                   <!-- Brand and toggle get grouped for better mobile display -->
                <div class="container">    
        <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<!--                <h3>YOU CARE ABOUT PATIENTS,WE CARE ABOUT YOU</h3>-->
                    <?php
                    if(isset($user)) {
                     ?>  
                        <img src="<?php echo base_url();?>/assets/img/qilogo.png"  class="logo-img" />
                    <?php
                    }
                    else
                    {
                        ?>
                        <h3>YOU CARE ABOUT PATIENTS, WE CARE ABOUT YOU</h3>
                    <?php }
                    ?>
                <ul class="nav navbar-nav navbar-right main-menu">
                <?php  if($this->session->userdata('logged_in')){?>
                    <?php if($user['userlvl']!=3){?>
                    <li>
                        <a class="page-scroll" href="<?=site_url('profile')?>">Home</a>
                    </li>
                    <?php }?>
                    <li>
                        <a class="page-scroll" href="<?=site_url('hospitals')?>">Hospitals</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="logout">Log Out</a>
                    </li>
                    <li>
                        <a class="page-scroll">| &nbsp;&nbsp;&nbsp;Login As
                            <?php
                                if($user['userlvl'] == 1)
                                {
                                    echo $user['name']." (QI Team)";
                                }
                                else if($user['userlvl'] == 2)
                                {
                                    echo $user['name']. " (Client)";
                                }
                                else if($user['userlvl'] == 3)
                                {
                                    echo $user['name']." (Counter) ";
                                }
                                ?>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

