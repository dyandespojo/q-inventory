<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>
    <div class="container-fulid my_header"> 
     <h3>Welcome <?php echo ucfirst($this->user_manager->this_user_name());?></h3>
    </div>
<header>
    <div class="header-content">
        <div class="header-content-inner">
            <section class="container">
                <div class="row">
                    <div class="col-md-2">
                        <a href="#">
                            <div class="larg-btn-box">
                                <div class="circle-border"><i class="fa fa-tachometer fa-3x" aria-hidden="true"></i></div>
                                <h3>Dashboard</h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2">
                        <a href="#">
                            <div class="larg-btn-box">
                                <div class="circle-border"><i class="fa fa-upload fa-3x" aria-hidden="true"></i></div>
                                <h3>Upload</h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2">
                        <a href="#">
                            <div class="larg-btn-box">
                                <div class="circle-border"><i class="fa fa-download fa-3x" aria-hidden="true"></i></div>
                                <h3>Download</h3>
                            </div>
                        </a>
                    </div>
                </div>
            </section>
        </div>
    </div>
</header>
<?php $this->load->view("footer.php");?>