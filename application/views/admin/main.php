<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>
    <div class="container-fulid my_header"> 
     <h3>Welcome <?php echo ucfirst($this->user_manager->this_user_name());?></h3>
    </div>
<header>

    <div class="header-content">
            <div class="header-content-inner">
                <section class="container">
        <div class="row">
    <div class="col-md-5ths">
        <a href="user">
<div class="larg-btn-box">
        <div class="circle-border"><i class="fa fa-user fa-3x"></i></div>
    <h3>Users</h3>
        </div>
            </a>
    </div>
    <div class="col-md-5ths">
            <a href="hospitals">
                <div class="larg-btn-box">
                    <div class="circle-border"><i class="fa fa-hospital-o fa-3x"></i></div>
                <h3>QI Software</h3>
                </div>
            </a>
        </div>
        <div class="col-md-5ths">
            <a href="deshboard">
                <div class="larg-btn-box">
                    <div class="circle-border"><i class="fa fa-tachometer fa-3x" aria-hidden="true"></i></div>
                <h3>Dashboard</h3>
                </div>
            </a>
        </div>
    <div class="col-md-5ths">
              <a href="datafiles">
                <div class="larg-btn-box">
                    <div class="circle-border"><i class="fa fa-file fa-3x" aria-hidden="true"></i></div>
                <h3>Client FTP</h3>
                </div>
            </a>
    </div>
        <div class="col-md-5ths">
                  <a href="qifiles">
                    <div class="larg-btn-box">
                       <div class="circle-border"><i class="fa fa-building-o fa-3x" aria-hidden="true"></i></div>
                        <h3>Q-Inventory FTP</h3>
                      </div>
                    </a>
            
            </div>
        </div>
</section>
            </div>
        </div>
</header>
<?php $this->load->view("footer.php");?>