<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>
    <div class="container-fulid my_header"> 
     <h3>Deshboard</h3>
    </div>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
 <?php $this->load->view("admin/deshboard/deshboard_header.php");?>
        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>All Supplier Detail
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                              <div id="container-supplier"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>           
                <div class="col-lg-12">
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Top Supplier Detail
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">      
                                    <div class="form-group">
                                        <select class="form-control" id="top-filter">
                                            <option value="5">TOP 5</option>
                                           <option value="10">TOP 10</option>
                                           <option value="15">TOP 15</option>
                                           <option value="20">TOP 20</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4"></div>
                            <div class="row">
                                <div class="col-lg-12">
                             
                              <div id="container-supplier-top"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>  
                </div>
        <!--        <div class="col-lg-12">
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>Supplier Comparison
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            <div class="col-md-3"></div>
                                <div class="col-md-2">  
                                    <div class="form-group">
                                        <select class="form-control" id="sel1">
                                            <option>A</option>
                                           <option>B</option>
                                           <option>C</option>
                                           <option>D</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2" style="text-align: center;color: #078bd0;font-size: 20px;font-weight: 900;">VS</div>
                            <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control" id="sel1">
                                            <option>A</option>
                                           <option>B</option>
                                           <option>C</option>
                                           <option>D</option>
                                        </select>
                                    </div>
    
                            
                            </div>
                            <div class="col-md-3"></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                              <div id="container-comparison"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    -->       
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
  
</body>

<?php $this->load->view("footer.php");?>

