<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>
    <div class="container-fulid my_header"> 
     <h3>Deshboard</h3>
    </div>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
 <?php $this->load->view("admin/deshboard/deshboard_header.php");?>
        
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i>On Hand Detail
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                  <th>On Hand Value</th>
                                                    <td class="on_hand"></td>
                                                </tr>
                                                <tr>
                                                    <th>Main</th>
                                                    <td class="on_hand_main"></td>
                                                </tr>
                                                <tr>
                                                    <th>POU</th>
                                                    <td class="on_hand_pou"></td>
                                                </tr>
                                                <tr>
                                                    <th>Match</th>
                                                    <td class="on_hand_match"></td>
                                                </tr>
                                                <tr>
                                                    <th>Unmatch</th>
                                                    <td class="on_hand_unmatch"></td>
                                                </tr>
                                                <tr>
                                                    <th>Owned</th>
                                                    <td class="on_hand_owned"></td>
                                                </tr>
                                                <tr>
                                                    <th>Consigned</th>
                                                    <td class="on_hand_consigned"></td>
                                                </tr>

                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                              <div id="container-on-hand"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Main VS POU
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                              <tr>
                                                    <th>Main</th>
                                                    <td class="on_hand_main"></td>
                                                </tr>
                                                <tr>
                                                    <th>POU</th>
                                                    <td class="on_hand_pou"></td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="main_vs_pou"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Match vs Unmatch
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                               <tr>
                                                    <th>Match</th>
                                                    <td class="on_hand_match"></td>
                                                </tr>
                                                <tr>
                                                    <th>Unmatch</th>
                                                    <td class="on_hand_unmatch"></td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="match_vs_unmatch"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            
                  <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Owned Vs. Consigned
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Owned</th>
                                                    <td class="on_hand_owned"></td>
                                                </tr>
                                                <tr>
                                                    <th>Consigned</th>
                                                    <td class="on_hand_consigned"></td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="owned_vs_consigned"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <div class="no-display" style="display:none">
   <div id="container-supplier-top"></div>
     <div id="container-supplier"></div>
        </div>
</body>

<?php $this->load->view("footer.php");?>
