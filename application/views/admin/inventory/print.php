<?php $this->load->view("header.php");?>

  <div class="container-fulid my_header"> 
     <h3>Q-Inventory</h3>
    </div>

<section class="contant admin-list-contant container-fulid">
    <span id="updation-message"></span>


    <div class="panel panel-default section-1">
        <div class="panel-heading">
            Record
        </div>
        <div class="panel-body">
            <h3>Hospital</h3>
            <table id="" class="print_table table table-striped table-bordered table-hover" width="100%">
                <tr>
                    <th>Name</th>
                    <td><?=$hospital['name']?></td>
                    <th>Email</th>
                    <td><?=$hospital['email']?></td>
                    <th>Phone</th>
                    <td><?=$hospital['phone']?></td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td><?=$hospital['address']?></td>
                    <th>City</th>
                    <td><?=$hospital['city']?></td>
                    <th>State</th>
                    <td><?=$hospital['state']?></td>
                </tr>

                <tr>
                    <th>Zip</th>
                    <td><?=$hospital['zip']?></td>
                    <th>Contact Name</th>
                    <td><?=$hospital['contact_name']?></td>
                </tr>
            </table>
            <h3>Inventories</h3>
            <table id="" class="print_table table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>PI Date</th>
                        <th>Audit %</th>
                        <th>Status</th>
                        <th>Department</th>
                        <th>General Area</th>
                        <th>Location</th>
                        <th>Location #</th>
                        <th>POU/Main</th>
                        <th>Consign</th>
                        <th>Est Bins</th>
                        <th>Est Bin/Hour</th>
                        <th>Est Count Time</th>
                        <th>Est Count Time (min)</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach($result as $row){ ?>
                    <tr>
                        <td><?=$row['pi_date']?></td>
                        <td><?=$row['audit_percent']?></td>
                        <td><?=$row['status']?></td>
                        <td><?=$row['department']?></td>
                        <td><?=$row['general_area']?></td>
                        <td><?=$row['location']?></td>
                        <td><?=$row['location_number']?></td>
                        <td><?=$row['consume']?></td>
                        <td><?=$row['consigned']?></td>
                        <td><?=$row['estimated_bins']?></td>
                        <td><?=$row['estimated_bins_per_hours']?></td>
                        <td><?=$row['estimate_count_time_hours']?></td>
                        <td><?=$row['estimate_count_time_min']?></td>

                    </tr>
                    <?php continue; ?>
                <tr>
                    <th width="50">Physical Inventory Date</th>
                    <td><?=$row['pi_date']?></td>
                </tr>
                <tr>
                    <th>Audit Percent</th>
                    <td><?=$row['audit_percent']?></td>
                    <th>Status</th>
                    <td><?=$row['status']?></td>
                </tr>

<!--                <tr>-->
<!--                    <th>Hospital</th>-->
<!--                    <td>--><?//=$row['hospital']?><!--</td>-->
<!--                    <th>Address</th>-->
<!--                    <td>--><?//=$row['address']?><!--</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>City</th>-->
<!--                    <td>--><?//=$row['city']?><!--</td>-->
<!--                    <th>State</th>-->
<!--                    <td>--><?//=$row['state']?><!--</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Zip</th>-->
<!--                    <td>--><?//=$row['zip']?><!--</td>-->
<!--                    <th>Contact Name</th>-->
<!--                    <td>--><?//=$row['contact_name']?><!--</td>-->
<!--                </tr>-->
<!--                <tr>-->
<!--                    <th>Email</th>-->
<!--                    <td>--><?//=$row['email']?><!--</td>-->
<!--                    <th>Phone</th>-->
<!--                    <td>--><?//=$row['phone']?><!--</td>-->
<!--                </tr>-->

                <tr>
                    <th>Department</th>
                    <td><?=$row['department']?></td>
                </tr>

                <tr>
                    <th>General Location</th>
                    <td><?=$row['general_area']?></td>
                    <th>Location</th>
                    <td><?=$row['location']?></td>
                </tr>

                <tr>
                    <th>Location Number</th>
                    <td><?=$row['location_number']?></td>
                    <th>POU/Main</th>
                    <td><?=$row['consume']?></td>
                </tr>

                <tr>
                    <th>Consign</th>
                    <td><?=$row['consigned']?></td>
                    <th>Estimated Bins</th>
                    <td><?=$row['estimated_bins']?></td>
                </tr>

                <tr>
                    <th>Estimated Bins Per Hours</th>
                    <td><?=$row['estimated_bins_per_hours']?></td>
                    <th>Estimated Count Time (Hours)</th>
                    <td><?=$row['estimate_count_time_hours']?></td>
                </tr>

                <tr><th>Estimated Count Time (Minutes)</th>
                    <td><?=$row['estimate_count_time_min']?></td>
                </tr>
                <tr><td colspan="4"><hr></td></tr>
                <?php } ?>
                </tbody>
            </table>

        </div>
    </div>



</section>
<?php $this->load->view("footer.php");?>
<script>
    $(".my-footer").hide();
    window.print();
</script>