<?php $this->load->view("header.php");?>

  <div class="container-fulid my_header"> 
     <h3>Q-Inventory</h3>
    </div>

<section class="contant admin-list-contant container-fulid">
    <span id="updation-message"></span>


    <div class="panel panel-default section-1">
        <div class="panel-heading">
            Record
        </div>
        <div class="panel-body">

            <table id="" class="table table-striped table-bordered table-hover" width="100%">
                <tbody>
                <?php
                foreach($result as $row){?>
                <tr>
                    <th width="50">Location</th>
                    <td><?=$row['location']?></td>
                </tr>
                <tr>
                    <th>Location Number</th>
                    <td><?=$row['location_number']?></td>
                    <th>Status</th>
                    <td><?=$row['status']?></td>
                </tr>
                <tr>
                    <th>Consume</th>
                    <td><?=$row['consume']?></td>
                    <th>Consigned</th>
                    <td><?=$row['consigned']?></td>
                </tr>
                <tr>
                    <th>Estimated Bins</th>
                    <td><?=$row['estimated_bins']?></td>
                    <th>Estimated Bins Per Hours</th>
                    <td><?=$row['estimated_bins_per_hours']?></td>
                </tr>
                <tr>
                    <th>Estimate Count Time (Hours)</th>
                    <td><?=$row['estimate_count_time_hours']?></td>
                    <th>Estimate Count Time (Min)</th>
                    <td><?=$row['estimate_count_time_min']?></td>
                </tr>
                <tr>
                    <th>Hospital</th>
                    <td><?=$row['hospital']?></td>
                    <th>Department</th>
                    <td><?=$row['department']?></td>
                </tr>

                <tr>
                    <th>General Location</th>
                    <td><?=$row['general_area']?></td>
                </tr>

                <tr><td colspan="4"><hr></td></tr>
                <?php } ?>
                </tbody>
            </table>

        </div>
    </div>



</section>
<?php $this->load->view("footer.php");?>
<script>
    $(".my-footer").hide();
    window.print();
</script>