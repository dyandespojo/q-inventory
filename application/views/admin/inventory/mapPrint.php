<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="<?=base_url()?>">
    <title>Q-Inventry</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-confirm.css">
    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/toastr.min.css" type="text/css">
    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css" type="text/css">
    <?php
    if(isset($css))
        foreach($css as $file) {?>
            <link rel="stylesheet" href="<?=$file;?>" type="text/css">
        <?php } ?>
</head>

<body style="background-color: transparent;">
<img src="<?=base_url('assets/img/printbg.png')?>" width="100%" height="100%" style="position:absolute;" />
<div id="mapPrintView" style="width:571px; height:461px; margin:auto;">
    <table class="info_box">
        <tr>
            <td>Department : </td>
            <td class="department_name"><?=$map_data[0]['dept']?></td>
        </tr>
		<?php if(isset($map_data[0]['area'])){?>
        <tr>
            <td>General Area : </td>
            <td class="area_name"><?=$map_data[0]['area']?></td>
        </tr>
		<?php } ?>
    </table>
	
	<?php foreach($map_data as $node){?>

	<table class="map-node ui-draggable ui-draggable-handle" style="left: <?=$node['position_left']?>px; top: <?=$node['position_top']?>px; min-width: 100px; position: relative;">
		<tbody>
			<tr>
				<td>
				<img src="<?=base_url('assets/img/btn.jpg')?>" width="150" height="40" />
				<span style="position:absolute;top:20%; left:20%;">
				<font color="white">
				<?=$node['specific_name']?>
				</font>
				<?php if(isset($node['location_number'])){?>
					<br><font color="white"><?=$node['location_number']?></font>
				<?php } ?>
				</span>
				</td>
			</tr>
		</tbody>
	</table>
	
	<?php }?>
</div>


<script src="<?php echo base_url();?>assets/js/backhand.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.js"></script>
<script src="<?php echo base_url();?>assets/js/toastr.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

<?php
if(isset($js))
    foreach($js as $file) {?>
        <script src="<?=$file?>"></script>
    <?php } ?>
	<script>
	window.print();
	</script>
</body>
</html>
