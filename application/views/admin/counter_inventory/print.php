<?php $this->load->view("header.php");?>

  <div class="container-fulid my_header"> 
     <h3>Q-Inventory</h3>
    </div>

<section class="contant admin-list-contant container-fulid">
    <span id="updation-message"></span>


    <div class="panel panel-default section-1">
        <div class="panel-heading">
            Record
        </div>
        <div class="panel-body">

            <table id="" class="table table-striped table-bordered table-hover" width="100%">
                <tbody>
                <?php
                foreach($result as $row){?>
                <tr>
                    <th width="50">Physical Inventory Date</th>
                    <td><?=$row['pi_date']?></td>
                </tr>
                <tr>
                    <th>Audit Percent</th>
                    <td><?=$row['audit_percent']?></td>
                    <th>Status</th>
                    <td><?=$row['status']?></td>
                </tr>
                <tr>
                    <th>Hospital</th>
                    <td><?=$row['hospital']?></td>
                    <th>Address</th>
                    <td><?=$row['address']?></td>
                </tr>
                <tr>
                    <th>City</th>
                    <td><?=$row['city']?></td>
                    <th>State</th>
                    <td><?=$row['state']?></td>
                </tr>
                <tr>
                    <th>Zip</th>
                    <td><?=$row['zip']?></td>
                    <th>Contact Name</th>
                    <td><?=$row['contact_name']?></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><?=$row['email']?></td>
                    <th>Phone</th>
                    <td><?=$row['phone']?></td>
                </tr>

                <tr>
                    <th>Department</th>
                    <td><?=$row['department']?></td>
                </tr>

                <tr>
                    <th>General Location</th>
                    <td><?=$row['general_area']?></td>
                    <th>Location</th>
                    <td><?=$row['location']?></td>
                </tr>

                <tr>
                    <th>Location Number</th>
                    <td><?=$row['location_number']?></td>
                    <th>POU/Main</th>
                    <td><?=$row['consume']?></td>
                </tr>

                <tr>
                    <th>Consign</th>
                    <td><?=$row['consigned']?></td>
                    <th>Estimated Bins</th>
                    <td><?=$row['estimated_bins']?></td>
                </tr>

                <tr>
                    <th>Estimated Bins Per Hours</th>
                    <td><?=$row['estimated_bins_per_hours']?></td>
                    <th>Estimated Count Time (Hours)</th>
                    <td><?=$row['estimate_count_time_hours']?></td>
                </tr>

                <tr><th>Estimated Count Time (Minutes)</th>
                    <td><?=$row['estimate_count_time_min']?></td>
                </tr>
                <tr><td colspan="4"><hr></td></tr>
                <?php } ?>
                </tbody>
            </table>

        </div>
    </div>



</section>
<?php $this->load->view("footer.php");?>
<script>
    $(".my-footer").hide();
    window.print();
</script>