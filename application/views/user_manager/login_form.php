<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>
<header>
        <div class="header-content">
            <div class="header-content-inner container">

                <?php
                if($msg!="")    
                {
                    ?>
<snap class="error-msg wow bounce"><?php echo $msg; ?></snap>
                <audio autoplay>
  <source src="<?php echo base_url();?>/assets/audio/error.mp3" type="audio/mpeg">
</audio>

                <?php
                }
                            ?>
    
<div class="row ">
    <div class="col-md-6">
        <div class="login-page-text <?php if($msg==""){ ?> wow zoomIn <?php } ?>">
    <ul>
        <li>Higher integrity of PHYSICAL INVENTORY with increased accuracy and efficiency</li>
        <li>Decreased Physician, Clinician and Patient disruption</li>
        <li>Increased Patient privacy</li>
        </ul>
        <h4>Physician Satisfaction and Clinical Optimization are paramount to 
providing a sterile and efficient envirionment to promote Patient Care</h4>
            </div>
</div> 
    <div class="col-md-6">                
                      <div class="home-page-login <?php if($msg==""){ ?> wow zoomIn <?php } ?>">
<form role="form" action="login" method="post">
  <div class="form-group ">
    <label for="username">Username:</label>
   <input name="username" type="text" value="<?=$username ?>" class="form-control" />
      <p><? echo form_error('username'); ?></p>
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input name="password" type="password" value="" class="form-control" minlength="6"/>
      <p><? echo form_error('password'); ?></p>
  </div>
 <div class="row ">
     <div class="col-md-6"> <input name="login" type="submit" value="login" class="btn login-btn"/></div><div class="col-md-6"><a href="<?=site_url('register')?>" class="btn login-btn">Sign Up</a></div>
    </div>
    <p id="forget-link">Forgot Password?</p>
</form>
      <form role="form" id="forgetform">
  <div class="form-group ">
    <label for="email">Email address:</label>
    <input type="email" class="form-control" id="email">
  </div>
 <div class="row">
     <div class="col-md-6"></div><div class="col-md-6"><button type="submit" class="btn login-btn">Send</button></div>
 </div>
</form>
</div> 
  

            </div>
        </div>
            </div>
    </div>
</header>
<?php $this->load->view("footer.php");?>
<script>
$("#forget-link").click(function () {
  if ( $( "#forgetform" ).is( ":hidden" ) ) {
    $( "#forgetform" ).slideDown( "slow" );
  } else {
    $( "#forgetform" ).hide("slow");
  }
});
</script>

   
