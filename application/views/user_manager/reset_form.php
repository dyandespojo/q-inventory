<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>
<header>
        <div class="header-content">
            <div class="header-content-inner container">
                <?php
                if($msg!="")    
                {
                    ?>
<snap class="error-msg wow bounce"><?php echo $msg; ?></snap>
                <audio autoplay>
  <source src="<?php echo base_url();?>/assets/audio/error.mp3" type="audio/mpeg">
</audio>

                <?php
                }
                            ?>
    
<div class="row ">
    <div class="col-md-3"></div> 
    <div class="col-md-6">                
                      <div class="home-page-login <?php if($msg==""){ ?> wow zoomIn <?php } ?>">

<form role="form" action="reset" method="post">
  <div class="form-group ">
    <label for="email">Your email:</label>
   <input type="text" name="email" value="<? echo $email ?>"  class="form-control" /> 
      <p><? echo form_error('email'); ?></p>
  </div>
 <div class="row ">
     <div class="col-md-6"> <input type="submit" value="reset" name="submit" class="btn login-btn"/></div><div class="col-md-6"><a href="<?=site_url('login')?>" class="btn login-btn">Log In</a></div>
    </div>
</form>

</div> 
  

            </div>
    <div class="col-md-3"></div>
        </div>
</header>
<?php $this->load->view("footer.php");?>

