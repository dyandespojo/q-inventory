<?php $this->load->view("header.php");?>
<?php $this->load->view("nav.php");?>
<header>
        <div class="header-content">
            <div class="header-content-inner container">
                <?php
                if($msg!="")    
                {
                    ?>
<snap class="error-msg wow bounce"><?php echo $msg; ?></snap>
                <audio autoplay>
  <source src="<?php echo base_url();?>/assets/audio/error.mp3" type="audio/mpeg">
</audio>

                <?php
                }
                            ?>
    
<div class="row ">
    <div class="col-md-3"></div> 
    <div class="col-md-6">                
                      <div class="home-page-login <?php if($msg==""){ ?> wow zoomIn <?php } ?>">
<form role="form" action="register" method="post">
  <div class="form-group ">
    <label for="username">Username:</label>
   <input type="text" name="username" value="<? echo $username; ?>" class="form-control"/> 
      <p><? echo form_error('username'); ?></p>
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" name="password" value="" class="form-control"/>
      <p><? echo form_error('password'); ?></p>
  </div>
  <div class="form-group">
    <label for="pwd">Re-enter Password</label>
    <input type="password" name="password2" value="" class="form-control"/>
      <p><? echo form_error('password2'); ?></p>
  </div>
  <div class="form-group">
    <label for="pwd">Email</label>
    <input type="text" name="email" value="<? echo $email; ?>" class="form-control"/>
      <p><? echo form_error('password2'); ?></p>
  </div>
  <div class="form-group">
    <label for="pwd">First name</label>
   <input type="text" name="firstname" value="<? echo $firstname; ?>" class="form-control"/> 
      <p><? echo form_error('firstname'); ?></p>
  </div>
  <div class="form-group">
    <label for="pwd">Last name</label>
   <input type="text" name="lastname" value="<? echo $lastname; ?>" class="form-control"/>
      <p><? echo form_error('lastname'); ?></p>
  </div>
<div class="form-group">
    <label for="pwd">Date of birth</label>
   <input type="date" name="dateofbirth" value="<? echo $dateofbirth; ?>" class="form-control"/>
      <p><? echo form_error('dateofbirth'); ?></p>
  </div>
 <div class="row ">
     <div class="col-md-6"><input type="submit" name="submit" value="Sign UP" class="btn login-btn"/></div><div class="col-md-6"><a href="<?=site_url('login')?>" class="btn login-btn">Already have an account</a></div>
    </div>

</form>

</div> 
  

            </div>
    <div class="col-md-3"></div>
        </div>
</header>
<?php $this->load->view("footer.php");?>


